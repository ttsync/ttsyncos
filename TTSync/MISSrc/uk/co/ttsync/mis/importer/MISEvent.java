package uk.co.ttsync.mis.importer;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Event;
import uk.co.ttsync.entities.User;

public class MISEvent extends Event {

	private static final long serialVersionUID = 1L;

	private static final DateTimeFormatter dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.S");
	private static final DateTimeFormatter timeFormat = DateTimeFormat.forPattern("HH:mm");

	public MISEvent(Calendar calendar, String date, String startTime, String finishTime, String location,
			User teacher) {
		super(calendar);
		
		DateTime d = DateTime.parse(date,dateFormat);
		DateTime s = DateTime.parse(startTime, timeFormat);
		DateTime f = DateTime.parse(finishTime, timeFormat);
			
		this.start = new DateTime (d.getYear(), d.getMonthOfYear(), d.getDayOfMonth(), s.getHourOfDay(), s.getMinuteOfHour());
		this.finish = new DateTime (d.getYear(), d.getMonthOfYear(), d.getDayOfMonth(), f.getHourOfDay(), f.getMinuteOfHour());

		setProperty ("location", location);
		this.teacher = teacher;
	}
	
}
