package uk.co.ttsync.mis.importer;

import java.util.Collection;
import java.util.HashMap;
import uk.co.ttsync.utils.Properties;

import uk.co.ttsync.entities.User;

public abstract class UserDao {

	protected final int logLevel;
	
	protected final Properties properties;

	private HashMap <String, User> users = new HashMap <String, User> ();

	protected UserDao (Properties properties) {
		this.properties = properties;

		logLevel = properties.getLogLevel();
	}
	
	public abstract Collection <User> readUsers () throws Exception;
	public abstract void readMembership(GroupDao groupDao) throws Exception;
	public abstract void readSubscriptions(CalendarDao calendarDao) throws Exception;
	
	protected void addUser (String id, User user) {
		users.put(id, user);
	}
	
	public Collection <User> getUsers () {
		return users.values();
	}
	
	public User getUser (String id) {
		return users.get(id);
	}

	
}

