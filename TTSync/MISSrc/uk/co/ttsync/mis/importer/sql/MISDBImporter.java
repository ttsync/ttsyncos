package uk.co.ttsync.mis.importer.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import uk.co.ttsync.entities.dtos.Dto;
import uk.co.ttsync.mis.importer.MISImporter;
import uk.co.ttsync.utils.Properties;

public class MISDBImporter extends MISImporter {

	private final Connection connection;

	public MISDBImporter(Properties properties)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, IOException {
		super(properties);

		Class.forName(properties.getProperty("mis.driver")).newInstance();
		connection = DriverManager.getConnection(properties.getProperty("mis.url"),
				properties.getProperty("mis.username"), properties.getProperty("mis.password"));
	}

	public void closeConnection() throws SQLException {
		connection.close();
	}

	/**
	 * Calls the DAOs to load entries from MIS. Override this if you want to
	 * customise how you load the entries, for example by updating the
	 * properties.
	 * 
	 * @param properties
	 * @throws SQLException
	 */
	public Dto readEntries() throws Exception {

		try {
			groupDao = new SQLGroupDao(connection, properties);
			groupDao.readGroups();
			studentDao = new SQLStudentDao(connection, properties);
			studentDao.readUsers();
			teacherDao = new SQLStaffDao(connection, properties);
			teacherDao.readUsers();

			studentDao.readMembership(groupDao);
			teacherDao.readMembership(groupDao);

			calendarDao = new SQLCalendarDao(connection, properties);
			calendarDao.readCalendars(groupDao);
			calendarDao.readCalendarEvents(teacherDao);

			studentDao.readSubscriptions(calendarDao);
			teacherDao.readSubscriptions(calendarDao);
		} catch (SQLException s) {
			s.printStackTrace();
		}

		return getDto();
	}
}
