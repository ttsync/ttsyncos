package uk.co.ttsync.mis.importer.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import uk.co.ttsync.utils.Properties;

import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Event;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.mis.importer.CalendarDao;
import uk.co.ttsync.mis.importer.GroupDao;
import uk.co.ttsync.mis.importer.MISEvent;
import uk.co.ttsync.mis.importer.UserDao;


public class SQLCalendarDao extends CalendarDao {

		private Connection connection;
	
	public SQLCalendarDao(Connection connection, Properties properties) throws SQLException {
		super(properties);
		this.connection = connection;
	}
	
	public Collection <Calendar> readCalendars (GroupDao groupDao) throws SQLException {
	
		if (logLevel > 0) 
 			System.out.println ("Starting calendar import "+new java.util.Date ());
		
		String query = properties.getProperty("mis.calendar.sql");
		ResultSet resultSet = connection.prepareStatement(query).executeQuery();
		int propCount = resultSet.getMetaData().getColumnCount();

		while (resultSet.next()) {
			String id = resultSet.getString("_calendarid");
			Calendar calendar = new Calendar ();
			addCalendar(id, calendar);

			Group group = groupDao.getGroup(id);
			if (group != null)
				group.setCalendar(calendar);

			for (int i = 1; i <= propCount; i++) {
				String param = resultSet.getMetaData().getColumnName(i);
				String value = resultSet.getString(i);
				calendar.setProperty(param, value);
			}
			
			if (logLevel > 1) 
				System.out.println (" ...imported MIS calendar: " + calendar);
		}
 		
		if (logLevel > 0) 
 			System.out.println (" Finished " + new java.util.Date () + "\n Created " + getCalendars().size() + " calendar entries.");
		
		return getCalendars();
	}
	
	
	
	public void readCalendarEvents (UserDao teacherDao) throws SQLException {
		if (logLevel > 0) 
 			System.out.println ("Starting event import "+new java.util.Date ());
		
		String query = properties.getProperty("mis.calendar.event.sql");
		ResultSet resultSet = connection.prepareStatement(query).executeQuery();
		
		while (resultSet.next()) {
			String date = resultSet.getString("date");
			String start = resultSet.getString("starttime");
			String finish = resultSet.getString("finishtime");
			String room = resultSet.getString("room");
			String teacher = resultSet.getString("_staffid");
			
			String groupName = resultSet.getString("_calendarid");


			Calendar calendar = getCalendar(groupName);
			Event event = new MISEvent (calendar, 
					date, start, finish, room, 
					teacherDao.getUser(teacher)
				);
			if (calendar != null)
				calendar.addEvent(event);
			else
				if (logLevel > 1)
					System.out.println ("Cannot find calendar " + groupName + " - skipping event " + event);
			
			if (logLevel > 1) 
				System.out.println (" ...imported MIS event: " + event);
		}
		
 		if (logLevel > 0) 
 			System.out.println (" Finished event entries " + new java.util.Date ());
	}
}