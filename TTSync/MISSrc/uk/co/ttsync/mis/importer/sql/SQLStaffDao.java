package uk.co.ttsync.mis.importer.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.mis.importer.CalendarDao;
import uk.co.ttsync.mis.importer.GroupDao;
import uk.co.ttsync.mis.importer.UserDao;

public class SQLStaffDao extends UserDao {

	private Connection connection;

	SQLStaffDao(Connection connection, Properties properties) throws SQLException {
		super(properties);
		this.connection = connection;
	}

	public Collection<User> readUsers() throws SQLException {

		if (logLevel > 0)
			System.out.println("Starting MIS staff import " + new Date());

		String query = properties.getProperty("mis.staff.sql");
		ResultSet resultSet = connection.prepareStatement(query).executeQuery();
		int propCount = resultSet.getMetaData().getColumnCount();

		while (resultSet.next()) {
			String id = resultSet.getString("_userid");
			User teacher = new SQLStaff();
			for (int i = 1; i <= propCount; i++) {
				String param = resultSet.getMetaData().getColumnName(i);
				String value = resultSet.getString(i);
				teacher.setProperty(param, value);
			}
			addUser(id, teacher);

			if (logLevel > 1) {
				System.out.print(" ...imported MIS staff: " + teacher);
				if (logLevel > 5)
					System.out.print ("\t" +teacher.getDetails());
				System.out.println();
			}
			
		}

		if (logLevel > 0)
			System.out.println(" Finished " + new Date() + "\n Created " + getUsers().size() + " staff entries.");

		return getUsers();
	}

	public void readMembership(GroupDao groupDao) throws SQLException {

		// Collection <Membership> memberships = new HashSet <Membership> ();
		if (logLevel > 0)
			System.out.println("Starting MIS staff group membership import " + new Date());

		String query = properties.getProperty("mis.staff.membership.sql");
		ResultSet resultSet = connection.prepareStatement(query).executeQuery();

		while (resultSet.next()) {
			String teacherId = resultSet.getString("_userid");
			String groupId = resultSet.getString("_groupid");
			User teacher = getUser(teacherId);
			Group group = groupDao.getGroup(groupId);
			if (teacher != null && group != null) {
				Membership m = new Membership(teacher, group, resultSet.getString("type"));
				if (logLevel > 1)
					System.out.println("...imported MIS member " + teacher + " to " + group + " as a " + m.getRole());
			}
		}

		if (logLevel > 0)
			System.out.println(" Finished staff membership iport." + new Date());
	}

	public void readSubscriptions(CalendarDao calendarDao) throws SQLException {

		Collection<Subscription> subscriptions = new HashSet<Subscription>();

		if (logLevel > 0)
			System.out.println("Starting MIS teacher calndar subscription import " + new Date());

		String query = properties.getProperty("mis.staff.membership.sql");
		ResultSet resultSet = connection.prepareStatement(query).executeQuery();

		while (resultSet.next()) {
			String teacherId = resultSet.getString("_userid");
			String calendarName = resultSet.getString("_groupid");
			User teacher = getUser(teacherId);
			Calendar calendar = calendarDao.getCalendar(calendarName);
			if (teacher != null && calendar != null) {
				Subscription s = new Subscription(teacher, calendar, resultSet.getString("type"));
				subscriptions.add(s);
				if (logLevel > 1)
					System.out.println("...imported MIS subscription " + teacher + " to " + calendar + " as a " + s.getRole());
			}
		}

		if (logLevel > 0)
			System.out.println(" Finished student membership iport." + new Date());
	}
}
