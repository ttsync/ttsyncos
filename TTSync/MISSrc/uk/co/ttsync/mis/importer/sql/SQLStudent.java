package uk.co.ttsync.mis.importer.sql;

import uk.co.ttsync.entities.User;

class SQLStudent extends User {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString () {
		return this.getProperty("forename") + " " + this.getProperty("surname") + " (" + this.getProperty("enrolmentnumber") + ")";
	}
}