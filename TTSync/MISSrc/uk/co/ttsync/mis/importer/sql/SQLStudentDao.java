package uk.co.ttsync.mis.importer.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.mis.importer.CalendarDao;
import uk.co.ttsync.mis.importer.GroupDao;
import uk.co.ttsync.mis.importer.UserDao;

/**
 * this class reads student User objects out of the input data source.
 * 
 * @author jimster
 *
 */
public class SQLStudentDao extends UserDao {

	private Connection connection;
	
	// variables for deriving enrolmentyear...
	private int oldestMonth = 9;
	private int youngestAge = 11;
	private DateTimeFormatter fmt; 
	
	SQLStudentDao(Connection connection, Properties properties) throws SQLException {
		super(properties);
		this.connection = connection;
		
		// for enrolment year...
		if (properties.containsKey("mis.oldestmonth"))
			oldestMonth = Integer.parseInt(properties.getProperty("mis.oldestmonth"));
		if (properties.containsKey("mis.youngestage"))
			oldestMonth = Integer.parseInt(properties.getProperty("mis.youngestage"));
		
		String dateFormat = "dd/MM/yyyy";
		if (properties.containsKey("mis.dateformat"))
			dateFormat = properties.getProperty("mis.dateformat");
		fmt = DateTimeFormat.forPattern(dateFormat);
	}
	
	public Collection <User> readUsers () throws SQLException {
		
		if (logLevel > 0) 
 			System.out.println ("Starting MIS student import "+new Date ());
		
		String query = properties.getProperty("mis.student.sql");
		ResultSet resultSet = connection.prepareStatement(query).executeQuery();
		int propCount = resultSet.getMetaData().getColumnCount();
				
		while (resultSet.next()) {
			String id = resultSet.getString("_userid");
			User student = new SQLStudent ();
			for (int i = 1; i <= propCount; i++) {
				String param = resultSet.getMetaData().getColumnName(i);
				String value = resultSet.getString(i);
				student.setProperty(param, value);
			}
// 			set derived data, eg enrolment year here...		
			setDerivedData (student);
			addUser(id, student);
		
			if (logLevel > 1) {
				System.out.print (" ...imported MIS student: " + student); 
				if (logLevel > 5)
					System.out.print ("\t" +student.getDetails());
				System.out.println();
			}
		}
 		
		if (logLevel > 0) 
 			System.out.println (" Finished " + new Date () + "\n Created " + getUsers().size() + " student entries.");
		
		return getUsers();
	}

	/**
	 * Override this to do something other stuff...???
	 * @param student
	 */
	protected void setDerivedData (User student) {
		
		DateTime dob = fmt.parseDateTime(student.getProperty("dob"));
		int enrolmentYear = dob.plusMonths(1).minusMonths(oldestMonth) // add a month, take away oldest month
				.getYear()				// take the year
				+ youngestAge + 1;		// add some fudge
		
		student.setProperty("enrolmentyear", enrolmentYear);
	}
	
	/**
	 * read all groups from source quey of all 
	 *  
	 * @return
	 * @throws SQLException
	 */

	public void readMembership (GroupDao groupDao) throws SQLException {
		
//		Collection <Membership> memberships = new HashSet <Membership> ();
		if (logLevel > 0) 
 			System.out.println ("Starting MIS student group membership import "+new Date ());
	
		String query = properties.getProperty("mis.student.membership.sql");
		ResultSet resultSet = connection.prepareStatement(query).executeQuery();

		while (resultSet.next ()) {
			String studentId = resultSet.getString("_userid");
			String groupId = resultSet.getString("_groupid");
			User student = getUser(studentId);
			Group group = groupDao.getGroup(groupId);
			if (student != null && group != null) {
				Membership m = new Membership (student, group, resultSet.getString("type"));
				if (logLevel > 1)
					System.out.println ("...imported MIS member " + student + " to " + group + " as a " + m.getRole());
			}				
		}
		
 		if (logLevel > 0) 
 			System.out.println (" Finished student membership iport." + new Date ());
	}
	
	public void readSubscriptions (CalendarDao calendarDao) throws SQLException {
	
		Collection <Subscription> subscriptions = new HashSet <Subscription> ();
		
		if (logLevel > 0) 
 			System.out.println ("Starting MIS student calendar subscription import "+new Date ());
	
		String query = properties.getProperty("mis.student.subscription.sql");
		ResultSet resultSet = connection.prepareStatement(query).executeQuery();

		while (resultSet.next ()) {
			String studentId = resultSet.getString("_userid");
			String calendarName = resultSet.getString("_groupid");
			User student = getUser(studentId);
			Calendar calendar = calendarDao.getCalendar(calendarName);
			if (student != null && calendar != null) {
				Subscription s = new Subscription (student, calendar, resultSet.getString("type"));
				subscriptions.add(s);
				if (logLevel > 1)
					System.out.println ("...imported MIS subscription " + student + " to " + calendar + " as a " + s.getRole());
			}				
		}
		
 		if (logLevel > 0) 
 			System.out.println (" Finished student subscription import." + new Date ());
	}
}
