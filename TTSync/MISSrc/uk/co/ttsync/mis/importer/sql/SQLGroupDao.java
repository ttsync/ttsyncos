package uk.co.ttsync.mis.importer.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Collection;
import uk.co.ttsync.utils.Properties;

import uk.co.ttsync.entities.Group;
import uk.co.ttsync.mis.importer.GroupDao;

public class SQLGroupDao extends GroupDao {

	private Connection connection;
	
	
	SQLGroupDao (Connection connection, Properties properties) throws SQLException {
		super(properties);
		this.connection = connection;
	}
	
	
	public Collection <Group> readGroups () throws SQLException { 

		if (logLevel > 0) 
 			System.out.println ("Starting teachinggroup import "+new Date ());

		String query = properties.getProperty("mis.group.sql");
		ResultSet resultSet = connection.prepareStatement(query).executeQuery();
		int propCount = resultSet.getMetaData().getColumnCount();
				
		while (resultSet.next()) {
			String id = resultSet.getString("_groupid");
			Group group = new Group ();
			addGroup(id, group);

			for (int i = 1; i <= propCount; i++) {
				String param = resultSet.getMetaData().getColumnName(i);
				String value = resultSet.getString(i);
				group.setProperty(param, value);
			}
			
			if (logLevel > 1) {
				System.out.print (" ...imported MIS group: " + group); 
				if (logLevel > 4)
					System.out.print ("\t" + group.getDetails());
				System.out.println ();
			}
		}
 		if (logLevel > 0) 
 			System.out.println (" Finished " + new Date () + "\n Created " + getGroups().size() + " group entries.");
 		
 		return getGroups();
	}	
}
