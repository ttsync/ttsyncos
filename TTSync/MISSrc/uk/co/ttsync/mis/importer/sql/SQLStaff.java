package uk.co.ttsync.mis.importer.sql;

import uk.co.ttsync.entities.User;

class SQLStaff extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return this.getProperty("title") + " " + this.getProperty("surname") + " (" + this.getProperty("staffcode") + ")";
	}
}