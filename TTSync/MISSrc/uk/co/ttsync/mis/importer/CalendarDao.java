package uk.co.ttsync.mis.importer;

import java.util.Collection;
import java.util.HashMap;
import uk.co.ttsync.utils.Properties;

import uk.co.ttsync.entities.Calendar;

public abstract class CalendarDao {

	protected final Properties properties;
	protected final int logLevel;

	private HashMap<String, Calendar> calendars = new HashMap<String, Calendar>();
	
	public CalendarDao (Properties properties) {
		this.properties = properties;

		logLevel = properties.getLogLevel();
	}

	public abstract Collection <Calendar> readCalendars (GroupDao groupDao) throws Exception;
	public abstract void readCalendarEvents(UserDao teacherDao) throws Exception;
	
	protected void addCalendar (String id, Calendar calendar) {
		calendars.put(id, calendar);
	}
	
	public Calendar getCalendar(String id) {
		return calendars.get(id);
	}
	
	public Collection <Calendar> getCalendars() {
		return calendars.values();
	}
}
