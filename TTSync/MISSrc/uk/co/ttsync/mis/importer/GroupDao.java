package uk.co.ttsync.mis.importer;

import java.util.Collection;
import java.util.HashMap;

import uk.co.ttsync.entities.Group;
import uk.co.ttsync.utils.Properties;

public abstract class GroupDao {

	protected Properties properties;

	protected final int logLevel;

	protected HashMap<String, Group> groups = new HashMap<String, Group>();

	public GroupDao(Properties properties) {
		this.properties = properties;

		logLevel = properties.getLogLevel();
	}

	public abstract Collection <Group> readGroups () throws Exception;
	
	protected void addGroup (String id, Group group) {
		groups.put(id, group);
	}
	public Collection<Group> getGroups() {
		return groups.values();
	}

	public Group getGroup(String id) {
		return groups.get(id);
	}

}
