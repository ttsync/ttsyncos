package uk.co.ttsync.mis.importer;

import java.util.HashSet;

import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.entities.dtos.Dto;
import uk.co.ttsync.utils.Properties;

public abstract class MISImporter {

	protected final Properties properties;
	
	protected GroupDao groupDao;
	protected UserDao studentDao;
	protected UserDao teacherDao;
	protected CalendarDao calendarDao;
	
	public MISImporter(Properties properties) {
		this.properties = properties;
	}
	
	public abstract Dto readEntries () throws Exception;
	
	public Dto getDto () {
		
		Dto result = new Dto ();
		
		HashSet <User> users = new HashSet <User> ();
		users.addAll(teacherDao.getUsers());
		users.addAll(studentDao.getUsers());

		result.users = users;
		result.groups = new HashSet <Group> (groupDao.getGroups());
		result.calendars = new HashSet <Calendar> ();
		if (calendarDao != null)
			result.calendars.addAll(calendarDao.getCalendars ());
		return result;
	}
}
