package uk.co.ttsync.mis.importer.flatfile;

import uk.co.ttsync.entities.dtos.Dto;
import uk.co.ttsync.mis.importer.MISImporter;
import uk.co.ttsync.utils.Properties;

public class FlatFileImporter extends MISImporter {

	public FlatFileImporter(Properties properties) {
		super(properties);
	}

	@Override
	public Dto readEntries() throws Exception {
		groupDao = new GroupDao (properties);
		groupDao.readGroups();
		studentDao = new StudentDao(properties);
		studentDao.readUsers();
		teacherDao = new StaffDao(properties);
		teacherDao.readUsers();

		studentDao.readMembership(groupDao);
		teacherDao.readMembership(groupDao);

		calendarDao = new CalendarDao(properties);
		calendarDao.readCalendars(groupDao);
		calendarDao.readCalendarEvents(teacherDao);

		studentDao.readSubscriptions(calendarDao);
		teacherDao.readSubscriptions(calendarDao);

		return getDto();
	}

}
