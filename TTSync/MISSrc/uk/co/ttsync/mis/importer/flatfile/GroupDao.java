package uk.co.ttsync.mis.importer.flatfile;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import uk.co.ttsync.entities.Group;
import uk.co.ttsync.utils.Properties;

class GroupDao extends uk.co.ttsync.mis.importer.GroupDao {

	public GroupDao(Properties properties) throws IOException {
		super(properties);
	}

	@Override
	public Collection<Group> readGroups() throws Exception {

		FlatFileReader ffr = new FlatFileReader(
				properties.getProperty("mis.flat.group.file"),
				properties.getProperty("mis.flat.group.fields"),
				properties.getProperty("mis.flat.delimiter"));

		if (logLevel > 0)
			System.out.println("Starting teachinggroup import " + new Date());

		while (ffr.hasNext()) {
			String[] fields = ffr.next();
			Group group = new Group();
			for (int i = 0; i < fields.length; i++)
				group.setProperty(ffr.getFieldName(i), fields[i]);

			addGroup(fields[0], group);

			if (logLevel > 1) {
				System.out.print(" ...imported MIS group: " + group);
				if (logLevel > 4)
					System.out.print("\t" + group.getDetails());
				System.out.println();
			}
		}

		if (logLevel > 0)
			System.out.println(" Finished " + new Date() + "\n Created "
					+ getGroups().size() + " group entries.");

		return getGroups();
	}
}