package uk.co.ttsync.mis.importer.flatfile;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.mis.importer.CalendarDao;
import uk.co.ttsync.mis.importer.GroupDao;
import uk.co.ttsync.utils.Properties;

public class StaffDao extends uk.co.ttsync.mis.importer.UserDao {

	public StaffDao(Properties properties) throws IOException {
		super(properties);
	}

	@Override
	public Collection<User> readUsers() throws Exception {

		FlatFileReader ffr = new FlatFileReader(
				properties.getProperty("mis.flat.staff.file"),
				properties.getProperty("mis.flat.staff.fields"),
				properties.getProperty("mis.flat.delimiter"));

		if (logLevel > 0)
			System.out.println("Starting staff import " + new Date());

		while (ffr.hasNext()) {
			String[] fields = ffr.next();
			User user = new User();
			for (int i = 0; i < fields.length; i++)
				user.setProperty(ffr.getFieldName(i), fields[i]);

			addUser(fields[0], user);

			if (logLevel > 1) {
				System.out.print(" ...imported staff: " + user);
				if (logLevel > 4)
					System.out.print("\t" + user.getDetails());
				System.out.println();
			}
		}

		if (logLevel > 0)
			System.out.println(" Finished " + new Date() + "\n Created "
					+ getUsers().size() + " user entries.");

		return getUsers();
	}

	@Override
	public void readMembership(GroupDao groupDao) throws Exception {
		FlatFileReader ffr = new FlatFileReader(
				properties.getProperty("mis.flat.staff.membership.file"),
				properties.getProperty("mis.flat.staff.membership.fields"),
				properties.getProperty("mis.flat.delimiter"));

		if (logLevel > 0)
			System.out.println("Starting staff membership import "
					+ new Date());

		int count = 0;
		while (ffr.hasNext()) {
			String[] fields = ffr.next();

			User user = getUser(fields[0]);
			Group group = groupDao.getGroup(fields[1]);
			if (user != null && group != null) {
				Membership mem = new Membership(user, group, "STAFF");
				count++;
				for (int i = 0; i < fields.length; i++)
					mem.setProperty(ffr.getFieldName(i), fields[i]);
				if (logLevel > 1) {
					System.out.print(" ...imported student membership: " + mem);
					if (logLevel > 4)
						System.out.print("\t" + mem.getDetails());
					System.out.println();
				}
			}

		}

		if (logLevel > 0)
			System.out.println(" Finished " + new Date() + "\n Created "
					+ count + " memberships.");

	}

	@Override
	public void readSubscriptions(CalendarDao calendarDao) throws Exception {
		FlatFileReader ffr = new FlatFileReader(
				properties.getProperty("mis.flat.staff.subscription.file"),
				properties.getProperty("mis.flat.staff.subscription.fields"),
				properties.getProperty("mis.flat.delimiter"));

		if (logLevel > 0)
			System.out.println("Starting student subscription import "
					+ new Date());

		int count = 0;
		
		while (ffr.hasNext()) {
			String[] fields = ffr.next();

			User user = getUser(fields[0]);
			Calendar calendar = calendarDao.getCalendar(fields[1]);
			if (user != null && calendar != null) {
				Subscription subs = new Subscription (user, calendar, "STAFF");
				count++; 
				
				for (int i = 0; i < fields.length; i++)
					subs.setProperty(ffr.getFieldName(i), fields[i]);
				if (logLevel > 1) {
					System.out.print(" ...imported staff subscription: " + subs);
					if (logLevel > 4)
						System.out.print("\t" + subs.getDetails());
					System.out.println();
				}
			}
		}

		if (logLevel > 0)
			System.out.println(" Finished " + new Date() + "\n Created "
					+ count + " subscriptions.");
	}
}
