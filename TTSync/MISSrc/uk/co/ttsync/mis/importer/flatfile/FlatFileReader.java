package uk.co.ttsync.mis.importer.flatfile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

class FlatFileReader implements Iterator<String[]> {

	private String[] fieldNames;
	private BufferedReader br;
	private String delim;

	private String[] nextLine;

	/**
	 * Create a reader for a specific file.
	 * 
	 * @param filename
	 * @param fields
	 * @param delim
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public FlatFileReader(String filename, String fields, String delim)
			throws FileNotFoundException {

		fieldNames = fields.split(",");
		for (int i = 0; i < fieldNames.length; i++)
			fieldNames[i] = fieldNames[i].trim();

		FileReader fr = new FileReader(filename);
		br = new BufferedReader(fr);
		this.delim = delim;

		readLine();
	}

	/**
	 * Process one line from flat file and split it into strings
	 * 
	 * @param line
	 * @return
	 * @throws IOException
	 */
	private void readLine() {
		try {
			String line = null;
			do {
				line = br.readLine();
			} 
			while (line != null && (line.startsWith("#") || line.matches("^\\s*$")));
			
			if (line != null) {
				String[] parts = line.split(delim);
				for (int i = 0; i < parts.length; i++)
					parts[i] = parts[i].trim();
				nextLine = parts;
			} else
				nextLine = null;
		} catch (IOException e) {
			nextLine = null;
		}
	}

	@Override
	public boolean hasNext() {
		return nextLine != null;
	}

	@Override
	public String[] next() {
		String[] currentLine = nextLine;
		readLine();
		return currentLine;
	}

	public String getFieldName(int i) {
		return fieldNames[i];
	}

	@Override
	public void remove() {
		// no		
	}
}
