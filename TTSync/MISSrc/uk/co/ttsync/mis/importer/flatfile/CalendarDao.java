package uk.co.ttsync.mis.importer.flatfile;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.mis.importer.GroupDao;
import uk.co.ttsync.mis.importer.UserDao;
import uk.co.ttsync.utils.Properties;

public class CalendarDao extends uk.co.ttsync.mis.importer.CalendarDao {

	private final DateTimeFormatter dateFormat;
	private final DateTimeFormatter timeFormat;

	public CalendarDao(Properties properties) throws IOException {
		super(properties);
		dateFormat  = DateTimeFormat.forPattern(properties.getProperty("mis.flat.dateformat"));
		timeFormat = DateTimeFormat.forPattern(properties.getProperty("mis.flat.timeformat"));
	}

	@Override
	public Collection<Calendar> readCalendars(GroupDao groupDao) throws Exception {

		FlatFileReader ffr = new FlatFileReader(
				properties.getProperty("mis.flat.calendar.file"),
				properties.getProperty("mis.flat.calendar.fields"),
				properties.getProperty("mis.flat.delimiter"));

		if (logLevel > 0)
			System.out.println("Starting calendar import " + new Date());

		while (ffr.hasNext()) {
			String[] fields = ffr.next();
			Calendar cal = new Calendar();
			for (int i = 0; i < fields.length; i++)
				cal.setProperty(ffr.getFieldName(i), fields[i]);

			addCalendar(fields[0], cal);
			cal.setGroup(groupDao.getGroup(fields[0]));
			
			if (logLevel > 1) {
				System.out.print(" ...imported MIS calendar: " + cal);
				if (logLevel > 4)
					System.out.print("\t" + cal.getDetails());
				System.out.println();
			}
		}

		if (logLevel > 0)
			System.out.println(" Finished " + new Date() + "\n Created "
					+ getCalendars().size() + " calendar entries.");

		return getCalendars();
	}

	@Override
	public void readCalendarEvents(UserDao teacherDao) throws Exception {

		FlatFileReader ffr = new FlatFileReader(
				properties.getProperty("mis.flat.events.file"),
				properties.getProperty("mis.flat.events.fields"),
				properties.getProperty("mis.flat.delimiter"));

		if (logLevel > 0)
			System.out.println("Starting event import " + new Date());

		int count = 0;
		while (ffr.hasNext()) {
			String[] fields = ffr.next();
			Calendar cal = getCalendar (fields[0]);
			Event ev = new Event(cal);
			
			//_date,_start,_finish,_teacherid
			DateTime date = DateTime.parse(fields[1], dateFormat);
			ev.setProperty("date", date);
			DateTime start = DateTime.parse(fields[2], timeFormat);
			ev.setProperty("start", start);
			DateTime finish = DateTime.parse(fields[3], timeFormat);
			ev.setProperty("finish", finish);
			User teacher = teacherDao.getUser(fields[4]);
			ev.setTeacher(teacher);
			
			for (int i = 4; i < fields.length; i++)
				ev.setProperty(ffr.getFieldName(i), fields[i]);

			cal.addEvent(ev);
			count++;
			if (logLevel > 1) {
				System.out.print(" ...imported MIS event: " + cal);
				if (logLevel > 4)
					System.out.print("\t" + cal.getDetails());
				System.out.println();
			}
		}

		if (logLevel > 0)
			System.out.println(" Finished " + new Date() + "\n Created "
					+ count + " event entries.");

		
	}
}