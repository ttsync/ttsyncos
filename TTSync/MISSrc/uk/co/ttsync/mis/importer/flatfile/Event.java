package uk.co.ttsync.mis.importer.flatfile;

import org.joda.time.DateTime;

import uk.co.ttsync.entities.Calendar;

class Event extends uk.co.ttsync.entities.Event {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Event(Calendar calendar) {
		super(calendar);
	}

	@Override
	public void setProperty (String property, Object value) {
		
		switch (property) {
			case "date":
				DateTime date = (DateTime) value;
				start = new DateTime (date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(), start.getHourOfDay(), start.getMinuteOfHour());
				finish = new DateTime (date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(), finish.getHourOfDay(), finish.getMinuteOfHour());
				break;
			case "start":
				DateTime startTime = (DateTime) value;
				start = new DateTime (start.getYear(), start.getMonthOfYear(), start.getDayOfMonth(), startTime.getHourOfDay(), startTime.getMinuteOfHour());
				break;
			case "finish":
				DateTime finishTime = (DateTime) value;
				finish = new DateTime (finish.getYear(), finish.getMonthOfYear(), finish.getDayOfMonth(), finishTime.getHourOfDay(), finishTime.getMinuteOfHour());
				break;
			default:
				super.setProperty(property, value);
		}
	}
	

}
