package uk.co.ttsync;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import uk.co.ttsync.entities.dtos.Dto;
import uk.co.ttsync.entities.serializers.GZipSerializer;
import uk.co.ttsync.entities.serializers.MisDtoSerializer;
import uk.co.ttsync.entities.serializers.Serializer;
import uk.co.ttsync.mis.importer.MISImporter;
import uk.co.ttsync.mis.importer.flatfile.FlatFileImporter;
import uk.co.ttsync.mis.importer.sql.MISDBImporter;
import uk.co.ttsync.utils.Properties;

public class MISAgent {

	private Properties properties;

	public MISAgent(Properties properties) throws Exception {

		this.properties = properties;
		
		try {
			read();
		} catch (Exception e) {
			System.out.println("Caught exception - " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void read() throws Exception {

		String misFile = properties.getProperty("mis.file");

		Serializer<Dto> misSerializer = new GZipSerializer<Dto>(
				new MisDtoSerializer());
//		if (properties.containsKey("mis.key"))
//			misSerializer = new EncryptingSerializer<Dto>(misSerializer,
//					properties.getProperty("mis.key"));

		MISImporter misImporter = null;

		switch (properties.getProperty("mis.importertype")) {
		case "flatfile":
			misImporter = new FlatFileImporter (properties);
			break;
		case "RDBMS":
			misImporter = new MISDBImporter(properties);
			break;
		default:
			throw new Exception("mis.importertype not recognised - "
					+ properties.getProperty("mis.importertype"));
		}

		Dto misDto = misImporter.readEntries();

		FileOutputStream fos = new FileOutputStream(misFile);
		misSerializer.serialize(fos, misDto);
		fos.close();
	}

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out
					.println("Launcher needs an argument of the name of at least one properties file");
			System.exit(0);
		}

		Properties properties = new Properties();

		System.out.println("Loading configuration:");
		for (String fileName : args) {
			try {
				properties.load(new FileReader(fileName));
			} catch (FileNotFoundException e) {
				System.out.println("couldn't find file " + fileName);
				e.printStackTrace();
				System.exit(-1);
			} catch (IOException e) {
				System.out.println("Failed to read file " + fileName);
				e.printStackTrace();
				System.exit(-1);
			}
			System.out.println("..loaded " + fileName);
		}
		
		try {
			new MISAgent (properties);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

// Serializer<Dto> misSerializer = new MisDtoSerializer();
// misSerializer = new GZipSerializer<Dto>(misSerializer);
// misSerializer = new EncryptingSerializer<Dto>(misSerializer,
// properties.getProperty("mail.key"));

