package uk.co.ttsync.filter;

import uk.co.ttsync.utils.Properties;

import uk.co.ttsync.entities.Event;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class EventFilterer  extends TypeFilterer <Event> {
	
	public EventFilterer(Properties properties) throws MalformedRuleException {
		super(properties);
	}

	@Override
	protected String getTypeString() {
		return "event";
	}
}

