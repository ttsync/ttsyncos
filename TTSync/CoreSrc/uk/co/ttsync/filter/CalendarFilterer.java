package uk.co.ttsync.filter;

import uk.co.ttsync.utils.Properties;

import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class CalendarFilterer  extends TypeFilterer <Calendar> {
	
	public CalendarFilterer(Properties properties) throws MalformedRuleException {
		super(properties);
	}

	@Override
	protected String getTypeString() {
		return "calendar";
	}
}

