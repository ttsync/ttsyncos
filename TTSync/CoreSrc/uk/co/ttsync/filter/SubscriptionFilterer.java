package uk.co.ttsync.filter;

import uk.co.ttsync.utils.Properties;

import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class SubscriptionFilterer extends TypeFilterer <Subscription> {

	public SubscriptionFilterer(Properties properties) throws MalformedRuleException {
		super(properties);
	}

	@Override
	protected String getTypeString() {
		return "subscription";
	}
}
