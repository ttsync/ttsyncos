package uk.co.ttsync.filter;

import uk.co.ttsync.utils.Properties;

import uk.co.ttsync.entities.Group;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class GroupFilterer extends TypeFilterer <Group> {
	
	public GroupFilterer(Properties properties) throws MalformedRuleException {
		super(properties);
	}

	@Override
	protected String getTypeString() {
		return "group";
	}
}
