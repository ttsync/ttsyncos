package uk.co.ttsync.filter;

import java.util.List;

import uk.co.ttsync.entities.HasProperties;
import uk.co.ttsync.utils.regex.Filter;

/**
 * An entryfilterer is passed an entry, and it tries it against the filters in order, and returns the first filter action returned that isn't null.
 * 
 * See Filter.Action for description of the 3 possible filter actions.
 * @author jim
 *
 * @param <E>
 */
public class EntryFilterer <E extends HasProperties> {

	private List <Filter> filters;
	
	/**
	 * 
	 * @param filters - a list of filters try try against entries passed via the process method
	 */
	public EntryFilterer(List <Filter> filters) {
		this.filters = filters;
	}
	
	public Filter.Action process(E entry) {
		
		for (Filter filter : filters) {
			Filter.Action action = filter.applyTo(entry);
			if (action != null)
				return action;			
		}
		// if none of the filters apply to this entry, ignore it.
		return Filter.Action.IGNORE;
	}
}