package uk.co.ttsync.filter;

import uk.co.ttsync.entities.User;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class UserFilterer extends TypeFilterer <User> {

	public UserFilterer(Properties properties) throws MalformedRuleException {
		super(properties);
	}

	@Override
	protected String getTypeString() {
		return "user";
	}
}
