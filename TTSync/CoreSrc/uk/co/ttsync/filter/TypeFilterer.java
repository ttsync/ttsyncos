package uk.co.ttsync.filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import uk.co.ttsync.entities.HasProperties;
import uk.co.ttsync.matcher.Matches;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.Filter;
import uk.co.ttsync.utils.regex.MalformedRuleException;

//import static uk.co.ttsync.filter.Filter.Action.*;

/**
 * manages the plan to filter all entries of a specific type
 * 
 * @author jimster
 *
 * @param <E>
 */
public abstract class TypeFilterer <E extends HasProperties> {

	EntryFilterer <E> entryFilterer;
	protected List <Filter> filters = new ArrayList <Filter> ();
	
	
	final int logLevel;
	
	protected String delim;
	
	TypeFilterer (Properties properties) throws MalformedRuleException {
		
		
		logLevel = properties.getLogLevel();
		
		delim = properties.getProperty("ttsync.delimiter.intra");
		
		loadFilters (properties);
	}
	
	protected void loadFilters (Properties properties) throws MalformedRuleException {

		String split = properties.getProperty("ttsync.delimiter.inter");
		String [] filterStrings = properties.getProperty("filter."+getTypeString()).split(split);
		
		for (int i = 0; i < filterStrings.length; i++) {
			filters.add(Filter.getFilter(filterStrings [i], delim));			
		}
		
		entryFilterer = new EntryFilterer <E> (filters);
	}
	
	protected abstract String getTypeString ();
	
	/**
	 * All-in-one method for processAdditions/update/removals:
	 * 
	 * @param matches
	 * @return - a matches object containing a list of entries to add, to update and to delete. 
	 * @throws InapplicableRuleException 
	 */
	public Matches <E> process (Matches <E> matches) {
		
		if (logLevel > 1)
			System.out.println ("Starting " + getTypeString () + " filter - " + 
					" there are " + matches.getUnmatchedMISEntries().size() + " MIS, " + 
					matches.getUnmatchedGroupwareEntries().size() + " groupware and " +
					matches.getMatches().size() + " matched " + getTypeString() + " ("+ new Date() + "):");

		// put results in here.
		Matches <E> result = new Matches <E> ();
		
		Iterator <E> iter;		
		
		/** 
		 * Entries that only appear in MIS.
		 * These can be added to the 'to add' list (mis only in result)
		 * 
		 */
		iter = matches.getUnmatchedMISEntries().iterator();
		
		while (iter.hasNext()) {
			E entry = iter.next();
			Filter.Action action = entryFilterer.process(entry); 
			if (action == Filter.Action.KEEP || action == Filter.Action.MANAGE) {
				result.getUnmatchedMISEntries().add(entry);
				if (logLevel > 2)
					System.out.println (" Entry " + entry.toString() + " - action ADD"); 
			}
		}
		
		/**
		 *  groupware only entries - these may be added to removal (groupware only) list in result
		 */
		iter = matches.getUnmatchedGroupwareEntries().iterator();
		
		while (iter.hasNext()) {
			E entry = iter.next();
			Filter.Action action = entryFilterer.process(entry);
			if (action == Filter.Action.DROP || action == Filter.Action.MANAGE) {
				result.getUnmatchedGroupwareEntries().add(entry);
				if (logLevel > 2)
					System.out.println (" Entry " + entry + " - action DROP");
			}
		}
		
		/**
		 *  matches - rules can apply to MIS or groupware entries, so test them on both.
		 */
		
		// groupware entries iterator:
		iter = matches.getMatches().keySet().iterator();
		
		while (iter.hasNext()) {
			E misEntry = iter.next();
			E gpwareEntry = matches.getMatch(misEntry);
			
			Filter.Action gpwareAction = entryFilterer.process(gpwareEntry);

			if (gpwareAction != Filter.Action.IGNORE) 
				switch (gpwareAction) {
					case MANAGE:
					case KEEP:
						result.addMatch(misEntry, gpwareEntry);
						if (logLevel > 2)
							System.out.println (" Entry " + misEntry + " - action UPDATE"); 
						break;
					case DROP:
						result.getUnmatchedGroupwareEntries().add(gpwareEntry);
						if (logLevel > 2)
							System.out.println (" Entry " + misEntry + " - action DROP"); 
						break;
					default:
						break;
				}
					
			else { // gpware action == null - maybe get a filter hit from mis...
				Filter.Action misAction = entryFilterer.process(misEntry);
				if (misAction != null) {
					switch (misAction) {
						case MANAGE:
						case KEEP:
							result.addMatch(misEntry, gpwareEntry);
							if (logLevel > 2)
								System.out.println (" Entry " + misEntry + " - action UPDATE"); 
							break;
						case DROP:
							result.getUnmatchedGroupwareEntries().add(gpwareEntry);
							if (logLevel > 2)
								System.out.println (" Entry " + misEntry + " - action DROP"); 
							break;
						default:
							break;
					}
				}
			}
		}
		
		System.out.println ("Finished " + getTypeString () + " filter - " + 
				" there are " + result.getUnmatchedMISEntries().size() + " MIS " + getTypeString () + " to add, " + 
				result.getUnmatchedGroupwareEntries().size() + " " + getTypeString() + " to remove and " +
				result.getMatches().size() + " matched " + getTypeString() + " to update ("+ new Date() + "):");

		return result;
	}
}
