package uk.co.ttsync.filter;

import uk.co.ttsync.utils.Properties;

import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class MembershipFilterer extends TypeFilterer <Membership> {

	public MembershipFilterer(Properties properties) throws MalformedRuleException {
		super(properties);
	}

	@Override
	protected String getTypeString() {
		return "membership";
	}
}
