package uk.co.ttsync.actioners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import uk.co.ttsync.matcher.Matches;
import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.HasProperties;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.ChangeDefinition;
import uk.co.ttsync.utils.regex.MalformedRuleException;
import uk.co.ttsync.utils.regex.SedEvaluator;

/**
 * ActionListBuilders take Entries and turn them into Actions to be applied via
 * the Google API. They read 3 collections of Entries - add, update and remove.
 * 
 * For new entries, it creates an ID based on the entries properties parsed
 * through the rules defined in
 * 
 * action.id.<entry type>=
 * 
 * in the properties. rules are of the format:
 * 
 * code/^\\d{2}(\\d{2})[-\\d]*(\\d{3})$/$1$2/
 * 
 * This is / delimited - the parts aare:
 * 
 * code - the property(s) from the Entry to use ^\\d{2}(\\d{2})[-\\d]*(\\d{3})$
 * - the regex that needs to match $1$2 - how to reassemble the capture groups
 * 
 * Forward slashes can be handled using \/. Maybe.
 * 
 * @author jimster
 *
 * @param <E>
 * @param <A>
 */
public abstract class AbstractActionListBuilder<E extends HasProperties, A extends Action<E,?>> {

	protected List<SedEvaluator> idBuilders;
	protected List<ChangeDefinition> changeDefinitions;

	protected final int logLevel;

	protected AbstractActionListBuilder(Properties properties) throws MalformedRuleException {

//		String [] idRules, String [] propertyRules, String split, String delim, int logLevel

		this.logLevel = Integer.parseInt(properties.getProperty ("loglevel"));
		// load rules for setting new entry's IDs

		String split = properties.getProperty("ttsync.delimiter.inter");
		String delim = properties.getProperty("ttsync.delimiter.intra");

		String[] idRules = properties.getProperty("action.id." + getTypeString()).split(split);
		idBuilders = new ArrayList<SedEvaluator>();

		try {
			for (int i = 0; i < idRules.length; i++) {
				String[] parts = idRules[i].trim().split(delim);
				idBuilders.add(new SedEvaluator(parts[0].trim(), parts[1].trim(), parts[2].trim()));
			}
		}
		catch (ArrayIndexOutOfBoundsException e) {
			throw new MalformedRuleException("Can't parse " + idRules);
		}

		// load rules for setting properties

		String[] propertyRules = properties.getProperty("action.fields." + getTypeString()).trim().split(split);
		changeDefinitions = new ArrayList<ChangeDefinition>();

		if (propertyRules.length > 1 || propertyRules[0].length() > 0)
			// bodge as it reads empty string as one entry in an array
			try {
				for (int i = 0; i < propertyRules.length; i++)
					changeDefinitions.add(ChangeDefinition.get(propertyRules[i], delim));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				throw new MalformedRuleException("Can't parse " + propertyRules);
			}
	}

	protected abstract String getTypeString();

	/**
	 * applies SedEvaluators in sequence until one matches, then returns the
	 * name generated by it.
	 * 
	 * @param newUser
	 * @return the generated name, or null if no evaluators match
	 * @throws InapplicableRuleException
	 */
	String buildName(E newEntry) {

		String result = null;
		Iterator<SedEvaluator> iter = idBuilders.iterator();

		while (result == null && iter.hasNext())
			result = iter.next().getResult(newEntry);

		return result;
	}

	/**
	 * provides an Action to create this type of Entry
	 * 
	 * @param newEntry
	 * @return
	 * @throws InapplicableRuleException
	 */
	protected A buildCreateAction(E newEntry) {

		// Strip out illegal characters...
		String id = buildName(newEntry);

		A action = getCreateAction(id, newEntry);

		// set properties;
		for (ChangeDefinition cd : changeDefinitions) {
			String propertyName = cd.getGroupwareField();
			String propertyValue = cd.getResult(newEntry);
			if (propertyValue != null) // conditional CDs return null if not
										// matched, so don't change.
				action.setProperty(propertyName, propertyValue);
		}
		return action;
	}

	protected abstract A getCreateAction(String id, E newEntry);

	/**
	 * provides an Action to update this type of Entry
	 * 
	 * @param newEntry
	 * @return
	 * @throws InapplicableRuleException
	 */
	protected A buildUpdateAction(E misEntry, E gwEntry) {

		A action = getUpdateAction(misEntry, gwEntry);

		for (ChangeDefinition cd : changeDefinitions)
			if (cd.onUpdate()) {
				String propertyName = cd.getGroupwareField();
				String currentValue = gwEntry.getProperty(propertyName);
				String newValue = cd.getResult(misEntry);
				if (newValue != null && 
						(currentValue == null || 
							( cd.isCaseSensitive() ? !currentValue.equals(newValue) : !currentValue.toLowerCase().equals(newValue.toLowerCase()) )
						)
					)
					action.setProperty(propertyName, newValue);
			}
		return action;
	}

	protected abstract A getUpdateAction(E misEntry, E gpwareEntry);

	protected A buildRemoveAction(E entry) {

		return getRemoveAction(entry);
	}

	protected abstract A getRemoveAction(E entity);

	public Collection<A> getCreateList(Matches<E> matches) {

		if (logLevel > 0)
			System.out.println("Starting create " + getTypeString() + " action list " + " (" + new Date() + "):");

		List<A> result = new ArrayList<A>();
		for (E newEntry : matches.getUnmatchedMISEntries()) {
			try {
				A a = buildCreateAction(newEntry);
				result.add(a);
				if (logLevel > 1)
					System.out.println(" Creating " + newEntry + " : " + a);
			}
			catch (NullPointerException e) {
				System.out.println("Failed to create a job for " + newEntry);
			}
		}

		if (logLevel > 0)
			System.out.println("Finished create list " + getTypeString() + " action list " + " (" + new Date() + "):");

		return result;
	}

	public Collection<A> getUpdateList(Matches<E> matches) {

		if (logLevel > 0)
			System.out.println("Starting update " + getTypeString() + " action list " + " (" + new Date() + "):");

		List<A> result = new ArrayList<A>();
		for (E misEntry : matches.getMatches().keySet()) {
			A a = buildUpdateAction(misEntry, matches.getMatch(misEntry));
			if (a != null && a.toString().equals("")) {
				if (logLevel > 1)
					System.out.println(matches.getMatch(misEntry) + " : no updates required - skipping");
			}
			else {
				result.add(a);

				if (logLevel > 0)
					System.out.println(" Updating " + matches.getMatch(misEntry) + " : " + a);
			}
		}

		if (logLevel > 0)
			System.out.println("Finished update list " + getTypeString() + " action list " + " (" + new Date() + "):");

		return result;
	}

	public Collection<A> getRemoveList(Matches<E> matches) {

		if (logLevel > 0)
			System.out.println("Starting remove " + getTypeString() + " action list " + " (" + new Date() + "):");

		List<A> result = new ArrayList<A>();
		for (E entry : matches.getUnmatchedGroupwareEntries()) {
			A a = buildRemoveAction(entry);
			result.add(a);
			if (logLevel > 1)
				System.out.println(" Finished remove list " + entry + " : " + a);
		}

		if (logLevel > 0)
			System.out.println("Finished remove " + getTypeString() + " action list " + " (" + new Date() + "):");

		return result;
	}
}