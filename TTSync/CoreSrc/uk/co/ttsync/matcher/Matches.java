package uk.co.ttsync.matcher;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import uk.co.ttsync.entities.HasProperties;

/**
 * A Matches object is a data transfer object containing 3 collections
 * 
 *  - MIS entries that have not been matched to google Entries
 *  - Google entries that have not been matched to MIS entries
 *  - pairs of matched google/mis entries.
 *  
 * How they are matched is not this object's problem - its just a data transfer object.
 * 
 * @author jim
 *
 * @param <E>
 */

public class Matches <E extends HasProperties> {
	
	private CopyOnWriteArrayList<E> misEntries = new CopyOnWriteArrayList <E> ();
	private CopyOnWriteArrayList<E> groupwareEntries  = new CopyOnWriteArrayList <E> ();
	
	// mis vs groupware entries:
	private ConcurrentHashMap<E, E> matchMap = new ConcurrentHashMap<E, E>();

	Matches(Collection<E> misEntries, Collection<E> groupwareEntries) {
		if (misEntries != null)
			this.misEntries.addAll(misEntries);
		if (groupwareEntries != null)
			this.groupwareEntries.addAll(groupwareEntries);
	}
	
	public Matches () {}

	/**
	 *  on adding a matched pair, they are removed from the mis and groupware collections (if they are in there)
	 * 
	 * @param misEntry
	 * @param groupwareEntry
	 */
	public void addMatch(E misEntry, E gwEntry) {
		matchMap.put(misEntry, gwEntry);
		misEntries.remove(misEntry);
		groupwareEntries.remove(gwEntry);
	}

	/**
	 * returns a collection of matched (mis vs groupware) entries
	 * @return
	 */
	public Map <E, E> getMatches() {
		return matchMap;
	}

	/**
	 * returns the groupware entry associated with a mis entry (if there is one)
	 * @param entry
	 * @return
	 */
	public E getMatch(E misEntry) {
		return matchMap.get(misEntry);
	}

	public Collection<E> getUnmatchedMISEntries() {
		return misEntries;
	}

	public Collection<E> getUnmatchedGroupwareEntries() {
		return groupwareEntries;
	}
}	
