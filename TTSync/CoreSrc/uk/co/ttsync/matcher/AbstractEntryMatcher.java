package uk.co.ttsync.matcher;

import java.util.Collection;
import java.util.Map;

import uk.co.ttsync.entities.HasProperties;

public abstract class AbstractEntryMatcher<E extends HasProperties, M> {

	private String description;

	public AbstractEntryMatcher (String matchRule) {
		this.description = matchRule;
	}
	
	public abstract Map <M, E> loadMISData(Collection<E> misEntries);
	public abstract Map <M, E> loadGWData(Collection<E> gwEntries);
	
	public String getDescription () {
		return description;
	}
	
	@Override
	public String toString () {
		return getDescription ();
	}
	
	protected abstract void match(Matches<E> matches, int logLevel);
}
