package uk.co.ttsync.matcher;

import java.util.ArrayList;
import java.util.Collection;

import uk.co.ttsync.entities.HasProperties;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

/**
 * base class for creating concrete TypeMatchers - UserTypeMatcher etc. Override
 * the method getRule () to set which field of the properties file to read the
 * match strings from (comma delimited, no extra spaces)
 * 
 * @author jim
 * 
 * @param <E>
 */
public abstract class RegexTypeMatcher<E extends HasProperties> extends
		AbstractTypeMatcher<E, String> {

	public RegexTypeMatcher(Collection<E> misEntries, Collection<E> gwEntries,
			Properties properties) throws MalformedRuleException {
		super(misEntries, gwEntries, properties);
	}

	protected Collection<RegexEntryMatcher<E>> getEntryMatchers()
			throws MalformedRuleException {

		ArrayList<RegexEntryMatcher<E>> result = new ArrayList<RegexEntryMatcher<E>>();

		for (int i = 0; i < getRuleStrings().length; i++) {

			RegexEntryMatcher<E> matcher = new RegexEntryMatcher<E>(
					getRuleStrings()[i].trim(), delim);
			result.add(matcher);
		}
		return result;
	}

	
}
