package uk.co.ttsync.matcher;

import java.util.Collection;
import java.util.Date;

import uk.co.ttsync.entities.HasProperties;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

/**
 * TypeMatchers run a series of matching algorithms against 2 sets of data, and finds matches.
 * 
 * @author jim
 *
 * @param <E>
 * @param <M>
 */
public abstract class AbstractTypeMatcher<E extends HasProperties, M extends Object> {

	protected final Properties properties;
	protected final int logLevel;

	// rulestrings should possibly be in subclasses?
	private String[] ruleStrings;

	/**
	 * the delimiter between items in an expression
	 */
	protected String delim;

	protected Matches<E> matches;

	public AbstractTypeMatcher(Collection<E> misEntries,
			Collection<E> gpwareEntries, Properties properties) {

		this.properties = properties;

		String split = properties.getProperty("ttsync.delimiter.inter");
		delim = properties.getProperty("ttsync.delimiter.intra");

		ruleStrings = properties.getProperty(
				"matcher.matchers." + getTypeString()).split(split);

		logLevel = properties.getLogLevel();

		matches = new Matches<E>(misEntries, gpwareEntries);
	}

	public String[] getRuleStrings() {
		return ruleStrings;
	}

	public Matches<E> getMatches() {
		return matches;
	}

	/**
	 * used to find relavant bit in props file + logging.
	 * 
	 * @return
	 */
	protected abstract String getTypeString();

	protected abstract Collection<? extends AbstractEntryMatcher<E,M>> getEntryMatchers()
			throws MalformedRuleException;

	/**
	 * loop through all entryMatchers, applying them in sequence.
	 * 
	 * @throws MalformedRuleException
	 */
	public void run() throws MalformedRuleException {

		if (logLevel > 0)
			System.out.println("Starting " + getTypeString() + " matcher...\n"
					+ "There are " + matches.getUnmatchedMISEntries().size()
					+ " MIS entries and "
					+ matches.getUnmatchedGroupwareEntries().size()
					+ " groupware entries" + " (" + new Date() + "):");

		if (logLevel > 6) {
			System.out.println("MIS entries:");
			for (E entry : matches.getUnmatchedMISEntries())
				System.out.println (entry.getDetails());
			System.out.println ("\n");
			System.out.println("GW entries:");
			for (E entry : matches.getUnmatchedGroupwareEntries())
				System.out.println (entry.getDetails());
			System.out.println ("\n");
		}
		
		// apply each matching rule in sequence...
		for (AbstractEntryMatcher<E,M> matcher : getEntryMatchers()) {

			matcher.match(matches, logLevel);
		}
		
		// now run update on all matches found
		for (E misEntry : matches.getMatches().keySet()) {
			E gwEntry = matches.getMatch(misEntry);
			updateEntries (misEntry, gwEntry);
		}
		
		if (logLevel > 2)
			System.out.println ("Finished Matchers - matched " + matches.getMatches().size() + " entries leaving " + matches.getUnmatchedMISEntries().size() + " MIS entries and " + matches.getUnmatchedGroupwareEntries().size() + " groupware entries.");
	
		if (logLevel > 5) {
			System.out.print("Remaining unmatched MIS entries:\t");
			for (E entry : matches.getUnmatchedMISEntries())
				System.out.print (entry + "; ");
			System.out.println ("\n");
			System.out.print("Remaining unmatched GW entries:\t");
			for (E entry : matches.getUnmatchedGroupwareEntries())
				System.out.print (entry + "; ");
			System.out.println ("\n");
		}
	}

//	protected abstract int match(Map<M, E> gwData, Map<M, E> misData);

	/**
	 * Override this method if you want to make something happen when you find match 2 entries.
	 * 
	 * @param entry
	 * @param match
	 */
	public void updateEntries(E misEntry, E gwEntry) {
		misEntry.setProperty("match", gwEntry);
		gwEntry.setProperty("match", misEntry);
	}

	// private class MatchesImpl implements Matches <E> {
	// }
}
