package uk.co.ttsync.matcher;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.joda.time.DateTime;

import uk.co.ttsync.entities.HasProperties;
import uk.co.ttsync.utils.regex.MalformedRuleException;
import uk.co.ttsync.utils.regex.RegexEvaluator;

/**
 * This class takes a collection of hasProperties objects, and a match
 * expression of the form:
 * 
 * props1 regex1 props2 regex2 modifiers
 * 
 * The collecton of objects are all run through an expressionEvaluator (using
 * props1 and regex1). These results are stored. Then pass the match method a
 * hasProps object - it is evaulated according to an evaluator (props2 regex2),
 * and if the result matches one from the collection, the result is removed from
 * the collection and returned.
 * 
 * @author jim
 * 
 * @param <E>
 * @param <M>
 */
public class RegexEntryMatcher<E extends HasProperties> extends
		AbstractEntryMatcher<E,String> {

	private RegexEvaluator misEvaluator, gwEvaluator;
	private boolean caseSensitive = true;

	public RegexEntryMatcher(String matchRule, String delim)
			throws MalformedRuleException {

		super(matchRule);
		try {
			String[] parts = matchRule.split(delim);
			misEvaluator = new RegexEvaluator(parts[0], parts[1]);
			gwEvaluator = new RegexEvaluator(parts[2], parts[3]);

			if (parts.length > 4 && parts[4].contains("i"))
				caseSensitive = false;

			// if (parts[4].contains("u"))
			// uniqueGlobalMatch = true;

		} catch (ArrayIndexOutOfBoundsException e) {
			throw new MalformedRuleException("Not enough parameters to rule.");
		}
	}

	public Map<String, E> loadMISData(Collection<E> misData) {
		return loadData(misData, misEvaluator);
	}

	public Map<String, E> loadGWData(Collection<E> gwData) {
		return loadData(gwData, gwEvaluator);
	}

	private Map<String, E> loadData(Collection<E> data, RegexEvaluator eval) {

		Map<String, E> result = new HashMap<String, E>();

		Iterator<E> iter = data.iterator();

		while (iter.hasNext()) {
			E entry = iter.next();
			String regex = eval.getResult(entry);
			if (regex != null) {
				if (!caseSensitive)
					regex = regex.toLowerCase();

				if (result.containsKey(regex))
					result.put(regex, null); // at least 2 entries match this
												// pattern, so flag it with a
												// null
				else
					result.put(regex, entry);
			}
		}
		
		// now remove any with nulls
		Iterator <String> regexIter = result.keySet().iterator();
		while (regexIter.hasNext()) 
			if (result.get(regexIter.next()) == null)
				regexIter.remove();
		
		return result;
	}
	
	@Override
	protected void match(Matches<E> matches, int logLevel) {

		int matchCount = 0;

		Map<String, E> misData = loadMISData(matches.getUnmatchedMISEntries());
		Map<String, E> gwData = loadGWData(matches.getUnmatchedGroupwareEntries());
		
		if (logLevel > 1)
			System.out.println(" matching: " + getDescription()	+ ":");
		
		if (logLevel > 4) {
			System.out.println("Mis entries:");
			for (String regex : misData.keySet())
				System.out.println("\t" + regex.toString() + " : "
						+ misData.get(regex).toString());

			System.out.println("GW entries:");
			for (String regex : gwData.keySet())
				System.out.println("\t" + regex.toString() + " : " + gwData.get(regex).getDetails());
		}

		for (String misMatch : misData.keySet())
			if (gwData.containsKey(misMatch)) {
				E mis = misData.get(misMatch);
				E gw = gwData.get(misMatch);

				matches.addMatch(mis, gw);
				matchCount++;

				if (logLevel > 4)
					System.out.println("  Matched " + mis + " with " + gw + " (on regex " + misMatch + ")");
			}

		if (logLevel > 1)
			System.out.println("Finished " 
					+ " matcher " + getDescription() + "\n - matched " + matchCount + " entries leaving "
					+ matches.getUnmatchedMISEntries().size()
					+ " mis entries and "
					+ matches.getUnmatchedGroupwareEntries().size()
					+ " groupware entries. " + "(" + new DateTime() + ")");
	}
}