package uk.co.ttsync.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;

/**
 * A Properties object is based on a String:String hashmap. It also contains a
 * logLevel integer.
 * 
 * It is similar to java.utils.Properties class, in that it stores
 * property:value pairs, but the read () method is different to handle different
 * syntax when populated via the load() method.
 * 
 * Properties file has the following syntax:
 * 
 * property=value(s)
 * 
 * - Lines starting '#' are ignored - use for comments - split lines - if a line
 * ends with '=', the line will continue until the next line containing nothing
 * but white spaces. You can have comment lines within split lines.
 * 
 * It also handles backslashes differently to java.utils.Properties - you don't
 * need to escape backslashes.
 * 
 * @author jim
 *
 */
public class Properties extends HashMap<String, String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * logLevel can be set by calling setProperty
	 */
	private int logLevel = 0;

	public Properties() {
	}

	public void load(Reader r) throws IOException {

		BufferedReader br = new BufferedReader(r);
		while (br.ready()) {
			String line = br.readLine().trim();
			if (!line.startsWith("#")) {
				int eqPos = line.indexOf('=');
				if (eqPos != -1) {
					if (eqPos == line.length() - 1 && br.ready()) {
						// equals at end of line - multi line entry
						String next = null;
						do {
							next = br.readLine().trim();
							if (!next.startsWith("#")) // its a comment
								line += " " + next;
						} while (br.ready() && next.length() != 0);
					}
					String key = line.substring(0, eqPos);
					String value = line.substring(eqPos + 1);
					this.setProperty(key, value);
				}
			}
		}
	}

	public int getLogLevel() {
		return logLevel;
	}

	public String getProperty(String key) {
		if (!this.containsKey(key))
			throw new RuntimeException("The property " + key
					+ " was not found in your configuration file. You will need to fix this to proceed.");
		return super.get(key);
	}

	public void setProperty(String property, String value) {
		if (property.equals("loglevel"))
			logLevel = new Integer(value);
		super.put(property, value);
	}
}
