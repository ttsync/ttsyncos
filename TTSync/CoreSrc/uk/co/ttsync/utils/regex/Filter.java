package uk.co.ttsync.utils.regex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import uk.co.ttsync.entities.HasProperties;

/**
 * produces Filter subclasses given a rule String.
 * 
 * Formats this factory understands (space delimited):
 * 
 * (property|memberOf) [NOT?] <properties> <regex> <what to do with them>
 * 
 * property NOT givenName+familyName ^Fred\\s\\sBloggs$ KEEP (== keep everyone
 * except Fred Bloggs) property givenName+familyName ^Elvis\\s\\sPresley$ IGNORE
 * (== leave Elvis Presley unchanged)
 * 
 * memberOf name ^Management.* IGNORE (== leave anyone in groups with name
 * starting 'Management' unchanged) memberOf NOT name ^Managed\\sUsers$ IGNORE
 * (== leave anyone not in 'Managed Users' group unchanged)
 * 
 * @author jimster
 *
 * @param <E>
 */
public abstract class Filter {

	public enum Action {
		/**
		 * add, update or remove entry as required
		 */
		MANAGE,
		/**
		 * Add or update entry
		 */
		KEEP,
		/**
		 * remove or disable entry
		 */
		DROP,
		/**
		 * Do not add this entry to processing list
		 */
		IGNORE
	}

	private Action action;
	private boolean not;
	private String rule;

	Filter(Action action, boolean not, String rule) {
		this.action = action;
		this.rule = rule;
	}

	public abstract Action applyTo(HasProperties entry);

	public Action getAction() {
		return action;
	}

	public boolean isNot() {
		return not;
	}

	public String toString() {
		return rule;
	}

	/*** Factory methods: ***/

	public static Filter getFilter(String rule, String separator)
			throws MalformedRuleException {

		List <String> parts = Arrays.asList(rule.trim().split(separator));
		return getFilter(new ArrayList <String> (parts), rule);
	}

	public static Filter getFilter(List<String> parts, String rule)
			throws MalformedRuleException {
		Action action = null;
		switch (parts.get(parts.size() - 1).trim()) {
		case "MANAGE":
			action = Action.MANAGE;
			break;
		case "KEEP":
			action = Action.KEEP;
			break;
		case "IGNORE":
			action = Action.IGNORE;
			break;
		case "DROP":
			action = Action.DROP;
			break;
		default:
			throw new MalformedRuleException("Cannot parse "
					+ parts.get(parts.size() - 1).trim() + " in " + rule
					+ " - should be 'MANAGE' or 'IGNORE'.");
		}
		parts.remove(parts.size() - 1);

		boolean not = false;
		try {
//			if (parts.get(parts.size() - 4).trim().equals("NOT"))
			if (parts.get(0).trim().equals("NOT")) {
				not = true;
				parts.remove(0);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new MalformedRuleException("Cannot parse " + rule);
		}

		switch (parts.get(0).trim()) {   
		case "property":
			return new PropertiesFilter(getRegexEvaluator(parts), action, not,
					rule);

//		case "memberof":
//			return new GroupMembershipFilter(getRegexEvaluator(parts), action,
//					not, rule);

		case "dateRange":
			return new DateRangeFilter(Integer.parseInt(parts.get(1).trim()), // days in arrears to accept
					Integer.parseInt(parts.get(3).trim()), // days in future to accept
					action, false, null);
		default:
			throw new MalformedRuleException("Cannot parse "
					+ parts.get(0).trim() + " in " + rule
					+ " - should be 'property' or 'memberOf'.");
		}
	}

	private static RegexEvaluator getRegexEvaluator(List<String> parts)
			throws MalformedRuleException {
		try {
			return new RegexEvaluator(parts.get(parts.size() - 2).trim(), 
					parts.get(parts.size() - 1).trim());
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new MalformedRuleException("Cannot parse regex part of"
					+ parts.toString());
		}
	}
}