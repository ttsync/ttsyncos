package uk.co.ttsync.utils.regex;

public class InapplicableRuleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InapplicableRuleException(String msg) {
		super (msg);
	}

}
