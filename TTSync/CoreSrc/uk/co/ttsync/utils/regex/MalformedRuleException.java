package uk.co.ttsync.utils.regex;

public class MalformedRuleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MalformedRuleException(String msg) {
		super(msg);
	}

}
