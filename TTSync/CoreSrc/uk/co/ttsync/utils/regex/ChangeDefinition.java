package uk.co.ttsync.utils.regex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import uk.co.ttsync.entities.HasProperties;

/**
 * Get method handles the following rule strings:
 * 
 * 		IF/[NOT?]/<misProperties>/<regex>/THEN/<gProp>/<value>   // conditional fixed value rule
 * 		IF/[NOT?]/<misProperties>/<regex>/THEN/<gProp>/<misProps>/<pattern>/<subst>    // conditional Regex property rule
 * 
 * or the unconditional version:
 * 
 * 		<gProp>/<value> 		// unconditional fixed value rule
 * 		<gProp>/<misProps>/<pattern>/<subst>  // unconditional Regex property rule
 * 
 * @author jimster
 *
 */
public abstract class ChangeDefinition {

	private String groupwareField;
	
	// if false, only run on create actions.
	private boolean onUpdate = true;
	
	private boolean caseSensitive = true;
	
	ChangeDefinition (String groupwareField, boolean onUpdate, boolean caseSensitive) {
		this.groupwareField = groupwareField;
		this.onUpdate = onUpdate;
		this.caseSensitive = caseSensitive;
	}
	
	public String getGroupwareField () {
		return groupwareField;
	}
	
	public boolean onUpdate () {
		return onUpdate;
	}
	
	public boolean isCaseSensitive () {
		return caseSensitive;
	}
	
	/**
	 *  Return the resulting value for the groupware field by applying the MIS entry to the change definition.
	 *  
	 * @param misEntry
	 * @return - A value of null indicates no change. A value of anything else is the new value.
	 * to remove a value, set it to "" and (hopefuly) google will take this as removing it.
	 * @throws InapplicableRuleException 
	 */
	public abstract String getResult (HasProperties misEntry);
	
//	public static ChangeDefinition get (String rule) throws MalformedRuleException {
//		return get (rule, "%");
//	}
	
	/**
	 * Entry point for getting a ChangeDefinition given a String and a delimiter
	 * @param rule - the rule to gnerate the ChangeDefinition from
	 * @param split - the delimiter string within the rule to split off the parts
	 * @return
	 * @throws MalformedRuleException
	 */
	public static ChangeDefinition get (String rule, String split) throws MalformedRuleException {
		
		List <String> parts = new ArrayList <String> (Arrays.asList(rule.trim().split(split)));
		
		// trim off whitespaces in properties
		for (int i = 0; i < parts.size(); i++)
			parts.set(i, parts.get(i).trim());

		// is this a create only rule, or should we run it on update too? It will have a '1' as the last param if only on create.
		boolean onUpdate = true;
		boolean caseSensitive = true;
		if (parts.get(parts.size()-1).matches("^tt_.*")) {
			if (parts.get(parts.size()-1).matches(".*once.*"))
				onUpdate = false;
			if (parts.get(parts.size()-1).matches(".*nocase.*"))
				caseSensitive = false;
			parts.remove(parts.size()-1);
		}
			
		if (parts.get(0).equals("IF")) { // conditional
			int iThen = parts.indexOf("THEN");
			PropertiesFilter conditional = ConditionalChangeDefinition.getCondition (parts.subList(1, iThen));
			UnconditionalChangeDefinition ucRule = UnconditionalChangeDefinition.getChangeDefinition (parts.subList(iThen + 1, parts.size()), onUpdate, caseSensitive);
			return new ConditionalChangeDefinition (conditional, ucRule);
		}
		else 		// unconditional
			return UnconditionalChangeDefinition.getChangeDefinition (parts, onUpdate, caseSensitive);
	}
	
	
	
	
}
