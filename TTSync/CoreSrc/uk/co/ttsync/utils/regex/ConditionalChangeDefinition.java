package uk.co.ttsync.utils.regex;

import java.util.ArrayList;
import java.util.List;

import uk.co.ttsync.entities.HasProperties;


class ConditionalChangeDefinition extends ChangeDefinition {

//	private boolean not = false;
	private UnconditionalChangeDefinition ucRule;
	private PropertiesFilter conditional;
	
	ConditionalChangeDefinition(PropertiesFilter conditional, UnconditionalChangeDefinition ucRule) {
		super(ucRule.getGroupwareField(), ucRule.onUpdate(), ucRule.isCaseSensitive());
		this.conditional = conditional;
		this.ucRule = ucRule;
	}

	// necessary??
	public boolean isNot () {
		return conditional.isNot();
	}
	
	static PropertiesFilter getCondition (List <String> parts) throws MalformedRuleException {
		
		parts = new ArrayList <String> (parts);
		parts.add("KEEP");
		Filter condition = Filter.getFilter(parts, parts.toString());
		if (condition instanceof PropertiesFilter)   //WTF???
			return (PropertiesFilter) condition; 
		else
			throw new MalformedRuleException ("Cannot parse condition rule " + parts.toString());
	}

	@Override
	public String getResult(HasProperties misEntry) {
		
		if (conditional.applyTo(misEntry) == Filter.Action.KEEP ^ isNot ())
			return ucRule.getResult(misEntry);
		else// doesn't match condition...
			 return null;
	}
}
