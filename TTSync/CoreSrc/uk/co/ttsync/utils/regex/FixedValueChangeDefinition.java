package uk.co.ttsync.utils.regex;

import uk.co.ttsync.entities.HasProperties;

/**
 * This class handles rules of the form:
 * 
 * 		<googleProperty>/<value>
 * 
 * such as:
 * 	
 * 		agreedToTerms/true	
 * 
 * @author jimster
 *
 */
class FixedValueChangeDefinition extends UnconditionalChangeDefinition {

	private String value;

	FixedValueChangeDefinition(String gField, String value, boolean onUpdate, boolean caseSensitive) {
		super(gField, onUpdate, caseSensitive);
		this.value = value;
	}

	public String getValue () {
		return value;
	}

	@Override
	public String getResult(HasProperties misEntry) {
		return value;
	}
}
