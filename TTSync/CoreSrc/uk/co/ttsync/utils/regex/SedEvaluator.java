package uk.co.ttsync.utils.regex;

import uk.co.ttsync.entities.HasProperties;

/**
 * SedEvaluator builds on RegexEvaluator by adding a third term which defines how to stick the capture groups
 * back together to create a substitution
 * @author jimster
 *
 * @param <E>
 */
public class SedEvaluator {

	private final String subst;
	private final RegexEvaluator regex;
	
	public SedEvaluator(String fields, String pattern, String subst)
			throws MalformedRuleException {
		this.regex = new RegexEvaluator (fields, pattern);
		this.subst = subst.trim();
	}
	
	public String getSubstitution () {
		if (regex.matcher == null || !regex.matcher.matches())
			return null;
		
		return regex.matcher.replaceAll(subst);
	}
	
	public String getResult (HasProperties hasProperties) {

		regex.getResult(hasProperties);
		
		if (regex.matcher == null || !regex.matcher.matches())
			return null;
		else
			try {
				return regex.matcher.replaceAll(subst);
			}
			catch (StringIndexOutOfBoundsException e) {
				System.out.println("Failed to process " + this.regex.toString() + " " + this.subst + " for " +hasProperties.toString() + "(" + hasProperties.getDetails() + ")");
				return null;
			}
	}
	
	public String toString () {
		return regex.toString() + " " + subst;
	}
}
