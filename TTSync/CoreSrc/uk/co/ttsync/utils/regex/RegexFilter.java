package uk.co.ttsync.utils.regex;


abstract class RegexFilter extends Filter {

	final RegexEvaluator regex;
	
	RegexFilter(RegexEvaluator regex, Filter.Action action, 
			boolean not, String rule) {
		super(action, not, rule);
		this.regex = regex;
	}
}
