package uk.co.ttsync.utils.regex;

import uk.co.ttsync.entities.HasProperties;

public class PropertiesFilter extends RegexFilter {

	protected PropertiesFilter(RegexEvaluator regex, Action action, boolean not, String rule) {
		super(regex, action, not, rule);
	}

	@Override
	public Filter.Action applyTo(HasProperties entry) {

		String result = regex.getResult(entry);
		if (result != null ^ isNot())
			return getAction();
		else
			return null;
	}
}