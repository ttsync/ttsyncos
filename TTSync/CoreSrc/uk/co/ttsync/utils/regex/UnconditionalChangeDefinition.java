package uk.co.ttsync.utils.regex;

import java.util.List;

abstract class UnconditionalChangeDefinition extends ChangeDefinition {

	UnconditionalChangeDefinition(String gField, boolean onUpdate, boolean caseSensitive) {
		super(gField, onUpdate, caseSensitive);
	}
	
	static UnconditionalChangeDefinition getChangeDefinition (List <String> parts, boolean onUpdate, boolean caseSensitive) throws MalformedRuleException {
		
		if (parts.size() == 2) // hopefully a ValueRule...
			return new FixedValueChangeDefinition (parts.get(0), parts.get(1), onUpdate, caseSensitive);
		
		if (parts.size() == 4) // hopefully a RegexPropertyRule
			return new RegexChangeDefinition (parts.get(0), new SedEvaluator (parts.get(1), parts.get(2), parts.get(3)), onUpdate, caseSensitive);
		
		// no idea how to parse this so throw exception
		throw new MalformedRuleException ("Cannot parse unconditional rule " + parts.toString());
	}
}
