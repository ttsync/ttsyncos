package uk.co.ttsync.utils.regex;

import uk.co.ttsync.entities.HasProperties;


/**
 * This rule handles rules of the form:
 * 
 * 		<google property>/<MISProperties>/<pattern>/<subst>
 * 
 * for instance:
 * 
 * 		fullName/givenName+familyName/^([a-zA-Z]*)\\s{2}([a-zA-Z]*)$/$1 $2
 * 
 * 		combine terms 2,3 and 4 into a SedEvaluator first.
 * @author jimster
 *
 */
public class RegexChangeDefinition extends UnconditionalChangeDefinition {

	private SedEvaluator sedEval;
	
	RegexChangeDefinition(String gField, SedEvaluator sedEval, boolean onUpdate, boolean caseSensitive) {
		super(gField, onUpdate, caseSensitive);
		this.sedEval = sedEval;
	}

	@Override
	public String getResult(HasProperties misEntry){
		
		return sedEval.getResult(misEntry);
	}

}
