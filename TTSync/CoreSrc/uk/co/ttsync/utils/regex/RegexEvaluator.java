package uk.co.ttsync.utils.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.co.ttsync.entities.HasProperties;

/**
 * A regexEvaluator object apply a fixed regular expression to a selection of property values of a HasProperties object.
 * If the selection of values matches the regular expression, the result of the capture groups is returned.
 * 
 * For instance:	forename+surname ^(\\w)\\w*\\s{2}([\\w]*)$
 * 
 * will concatenate the property values of 'forename' and 'surname' (with 2 spaces inbetween) and apply the regex ^(\\w)\\w*\\s{2}([\\w]*)$
 * to the result.
 * If the pattern matches the concatenates values, then the concatenated capture groups are returned.
 * 
 * @author jim
 *
 * @param <E>
 */
public class RegexEvaluator {

	/**
	 * a list of the fields to concatenate.
	 */
	private final String[] fields;
	/**
	 * the pattern to match
	 */
	private final Pattern pattern;
	
	Matcher matcher;

	/**
	 * Constructor - creates a new processor for a specific set of fields and expression. 
	 * @param fields - the list of fields (1 or more) joined by a '+' (no spaces)
	 * @param pattern - the regex to match with optional capture groups
	 * @throws MalformedRuleException
	 */
	public RegexEvaluator(String fields, String pattern) throws MalformedRuleException {
	
		if (fields == null || fields.equals(""))
			throw new MalformedRuleException("Fields not set.");
		
		if (fields.matches("^\\+.*") || fields.matches(".*\\+$"))
			throw new MalformedRuleException("Syntex error in fields: "	+ fields);
		
//		if (!pattern.matches(".*\\(.*\\).*"))
//			throw new MalformedRuleException("Syntex error in fields - no capture groups defined: " + pattern);

		this.fields = fields.trim().split("\\+");
		
		try {
			this.pattern = Pattern.compile(pattern.trim());
		} catch (Exception e) {
			throw new MalformedRuleException("Failed to create regex from "	+ pattern);
		}
	}

//	public void reset () {
//		matcher.reset();
//	}
	
	/**
	 * Reads the required property values from the hasProperties object, then applies the regular expression to the result. 
	 * @param hasProperties - he object whose properties are to be compared
	 * @return - a String of the concatenated capture groups from the expression or null if no match is found.
	 * @throws InapplicableRuleException thrown if the hasProperties object doesn't have the required fields defined. 
	 */
	public String getResult(HasProperties hasProperties) {
		
		// reset matcher so we don't remember old data...
		matcher = null;
		String [] captureGroups = null;
		
		captureGroups = getMatchData(buildFieldData(hasProperties));
		
		if (captureGroups == null)
			return null;
		else {
			String result = "";
			for (int i = 0; i < captureGroups.length; i++)
				result += captureGroups [i];
		
			return result;
		}
	}

	/**
	 * applies the pattern and returns captured groups. If the pattern does not match, null is returned.
	 * 
	 * @param fieldData
	 * @return
	 */
	String [] getMatchData(String fieldData) {

		if (fieldData == null)
			return null;
		
		matcher = pattern.matcher(fieldData);
		String [] result = new String [matcher.groupCount()];

		if (matcher.matches()) {
			int count = matcher.groupCount();
			for (int i = 0; i < count; i++)
				result [i] = matcher.group(i+1);
			
			return result;
		}
		else
			return null;
	}

	/**
	 * concatenates the values from the specified fields of the hasProperties object.
	 * If a filed is not found, return null.
	 * 
	 * @param entry
	 * @param fields
	 * @return
	 * @throws InapplicableRuleException
	 */
	private String buildFieldData(HasProperties entry) {
		String result = "";

		//Aaargh big nasty bodge. Need to readdress evaulators...
		if (fields [0].equals(""))
			return "fixed";
		for (int i = 0; i < fields.length; i++) {
			if (result.length() != 0)
				result += "  ";
			String value = entry.getProperty(fields[i].trim());
			if (value == null)
				return null;
			else
				result += value.trim();
		}
		return result;
	}
	
	public String toString () {
		String result = "";
		for (int i = 0; i < fields.length; i++)
			result += fields [i] + (i != fields.length -1 ? "+" : "");
		return result + " " + pattern.toString();
	}
}