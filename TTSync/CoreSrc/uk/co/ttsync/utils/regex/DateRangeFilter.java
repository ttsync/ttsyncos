package uk.co.ttsync.utils.regex;

import org.joda.time.DateTime;

import uk.co.ttsync.entities.Event;
import uk.co.ttsync.entities.HasProperties;


class DateRangeFilter extends Filter {

	DateTime minDate = new DateTime();
	DateTime maxDate = new DateTime();

	DateRangeFilter(int daysPast, int daysFuture, Action action, boolean not, String rule) {
		super(action, not, rule);

		minDate = minDate.minusDays(daysPast);
		maxDate = maxDate.plusDays(daysFuture);
	}

	@Override
	public Action applyTo(HasProperties entry) {
		
		Event event = (Event) entry;
		DateTime date = event.getStart();
				
		if (date.isAfter(minDate) && date.isBefore(maxDate))
			return getAction ();
		else
			return null;
	}
}
