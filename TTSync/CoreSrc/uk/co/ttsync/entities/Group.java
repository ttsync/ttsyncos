package uk.co.ttsync.entities;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Group extends HasProperties {

	private static final long serialVersionUID = 1L;

	Calendar calendar;
	transient Set <Membership> membership = new HashSet <Membership> ();

	public Group () {}
	
	/**
	 * For deserialization - there are loops in the data structure which cause deserialization to fail.
	 * 
	 * To get round this, a group's associations are transient, so collection properties need to be reinitialised.
	 * Values are put into this object by the Membership object on deserialization.
	 * 
	 * @param stream
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
		membership = new HashSet <Membership> ();
	}

	public String getName() {
		return getProperty ("name");
	}

	public Collection<Membership> getMembers() {
		return membership;
	}

	/**
	 * Get associated calendar (if there is one)
	 * @return
	 */
	public Calendar getCalendar() {
		return calendar;
	}
	
	public void setCalendar (Calendar calendar) {
		this.calendar = calendar;
		calendar.associatedGroup = this;
	}
	
	@Override
	public String toString() {
		return getName();
	}
	
//	@Override
//	public boolean equals (Object o) {
//		if (o == this)
//			return true;
//		return false;
//	}
}
