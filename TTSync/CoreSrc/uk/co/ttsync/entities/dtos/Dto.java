package uk.co.ttsync.entities.dtos;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Event;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.entities.User;

/**
 * A simple data transfer object containing collections objects of all the data managed by the application.
 * 
 * @author jimster
 *
 */
public class Dto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Collection <User> users;
	public Collection <Group> groups;
	public Collection <Calendar> calendars;

	public Collection <Membership> getMemberships () {
		Collection <Membership> memberships = new HashSet <Membership> ();
		if (groups != null)
			for (Group group : groups)
				memberships.addAll(group.getMembers());
		
		return memberships;
	}
	
	public Collection <Subscription> getSubscriptions () {
		Collection <Subscription> subscriptions = new HashSet <Subscription> ();
		
		if (users != null)
			for (User user : users)
				subscriptions.addAll(user.getSubscriptions());
		
		return subscriptions;
	}
	
	public Collection <Event> getEvents () {
		Collection <Event> events = new HashSet <Event> ();
		
		for (Calendar cal : calendars)
			events.addAll(cal.getEvents());
		
		return events;
	}
}
