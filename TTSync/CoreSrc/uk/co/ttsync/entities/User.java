package uk.co.ttsync.entities;

import java.util.Collection;
import java.util.HashSet;

public class User extends HasProperties {

	private static final long serialVersionUID = 1L;

	Collection <Membership> membership;
	Collection <Subscription> subscriptions;
	
	public User() {
		membership = new HashSet<Membership>();
		subscriptions = new HashSet <Subscription> ();
	}
	
	public Collection<Membership> getMembership() {
		return membership;
	}

	public Collection<Subscription> getSubscriptions() {
		return subscriptions;
	}
	
	@Override
	public String toString() {
		if (getProperty("fullname") != null)
			return getProperty("fullname");
		else
			return getProperty ("forename") + " " + getProperty ("surname");
	}
	
//	@Override
//	public boolean equals (Object o) {
//		if (o == this)
//			return true;
//		return false;
//	}
}