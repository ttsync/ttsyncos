package uk.co.ttsync.entities;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Calendar extends HasProperties {

	private static final long serialVersionUID = 1L;

	Group associatedGroup;
	
	private SortedSet <Event> events = new TreeSet <Event> ();
	transient Set <Subscription> subscribers = new HashSet <Subscription> ();

	public Calendar() {}

	private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
		subscribers = new HashSet <Subscription> ();
	}
	
	public Collection<Event> getEvents() {
		return events;
	}
	
	public void addEvent (Event event) {
		events.add(event);
		event.calendar = this;
	}
	
	public Group getGroup() {
		return associatedGroup;
	}
	
	// mutual group/calendar setting - could this go wrong??
	public void setGroup(Group group) {
		this.associatedGroup = group;
		if (group != null && group.getCalendar() != this)
			group.calendar = this;
	}

	@Override
	public String getProperty (String property) {
		if (property.equals("subject"))
			if (super.getProperty ("subject") != null)
				return super.getProperty ("subject");
			else if (associatedGroup != null)
					return associatedGroup.getProperty("subject");
		if (property.matches("^group.*"))
			return getGroup().getProperty(property.substring(6));
		return super.getProperty(property);
	}
	
	public String toString () {
		return getProperty ("name");
	}

	public Collection<Subscription> getSubscribers() {
		return subscribers;
	}
	
	@Override
	public boolean equals (Object obj) {
		return super.equals(obj) 
			|| 
				(obj instanceof Calendar &&	
				((Calendar)obj).getProperty("name") != null &&
				this.getProperty("name") != null &&
				((Calendar)obj).getProperty("name").equals(this.getProperty("name")));
	}
}