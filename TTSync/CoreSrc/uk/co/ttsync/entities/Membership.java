package uk.co.ttsync.entities;

public class Membership extends HasProperties  {

	private static final long serialVersionUID = 1L;

	private User user;
	private Group group;
	private String role;

	public Membership (User user, Group group, String role) {

		this.user = user;
		this.group = group;
		this.role = role;
	
		this.user.membership.add(this);
		this.group.membership.add(this);
	}

	public User getUser() {
		return user;
	}

	public Group getGroup() {
		return group;
	}

	public String getRole() {
		return role;
	}

	@Override
	public String getProperty (String property) {
		if (property.equals("user"))
			return getUser().toString();
		if (property.equals("group"))
			return getGroup().toString();
		if (property.matches("^user.*"))
			return getUser().getProperty (property.substring(5));
		if (property.matches("^group.*"))
			return getGroup().getProperty (property.substring(6));
		if (property.equals("role"))
			return getRole().toString();
		return super.getProperty(property); 
	}
	
	@Override
	public void setProperty (String property, Object value) {
		if (property.equals("role"))
			this.role = value.toString();
		else
			super.setProperty(property, value);
	}
	
	@Override
	public String toString() {
		return getUser() + " is a " + getRole() + " in " + getGroup();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Membership other = (Membership) obj;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}	
	
	@Override
	public String getDetails () {
		return toString();
	}
}
