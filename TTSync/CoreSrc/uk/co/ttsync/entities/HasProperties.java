package uk.co.ttsync.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;

/**
 * HasProperties is the base class for all entities from either the MIS or groupware data set.
 * 
 * A HasProperties object contains a map of property:value pairs, containing the details of the entity,
 * such as their name, id etc. There are no contraints on what can be stored in the map, except that
 * any property named 'match'.
 * 
 * Each HasProperties object may have a match - that is, another HasProperties object. The point here is to
 * be able to reference the corresponding entitiy in another data set. For instance, a user or group in the MIS 
 * dataset would correspond to a matching user or group in the groupware data set.
 *  
 * A match is set by calling hasProps1.setProperty("match", hasProps2). This will in turn call setProperty ("match")
 * on hasProps 1 so both objects point at each other.
 * 
 * There is currently no check for a match changing (it shouldn't happen and would have unpredicatable effects.
 * 
 * Matches are transient - they aren't remembered between executions of the code (ie streamed to file and back)
 * 
 * You can call getMatch () to retrieve the matched object, or getProperty ("match") to retrieve the match as a String.
 * You can call getProperty ("match.prop1") on a matched hasProps to retrieve the value of property 'prop1' from the 
 * matched object.  
 * @author jim
 *
 */
public abstract class HasProperties implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	/**
	 * the map where all properties are stored
	 */
	private HashMap <String, Object> properties;
	
	/**
	 * a matching HasProperties object. Transient as matches will be re-worked out on each execution of code.
	 */
	private transient HasProperties match;
	 
	HasProperties () {
		properties = new HashMap <String, Object> ();
	}
	
	/**
	 * getProperty returns a String representing the value of the stored property.
	 * If the property does not exist, null is returned.
	 * If the property is 'match' it will return the matched object as a String.
	 * If the property is 'match.xxx' it will return the property 'xxx' of the matched object as a String.
	 * 
	 * Override this method in subclasses to add specific behaviours as required.
	 * 
	 * @param property
	 * @return the object associated with the property, toString called on it.
	 */
	public String getProperty (String property) {
		if (property.matches("^match\\..+")) 
			if (match == null)
				return null;
			else
				return match.getProperty (property.substring(6));
		if (property.equals ("match"))
			if (match == null)
				return null;
			else
				return match.toString();
		
		Object result = properties.get(property);
		if (result != null)
			return result.toString();
		else
			return null;
	}
	
	/**
	 * returns a list of all available property names.
	 * The list will not contain 'match'. Call getMatch() to see if there is an associated hasProperties object
	 * 
	 * @return
	 */
	public Collection <String> getProperties () {
		return properties.keySet();
	}
	
	/**
	 * Sets a property.
	 * All values will be stored in the properties map except 'match' which is stored separately.
	 * 
	 * If you call setProperty ("match.xx") it will set a property 'match.xxx'
	 * If you call setProperty ("match", obj) you will get a classcastexception if obj is not a HasProperties
	 * if you call setProperties ("match", null) it will set a proeprty to null, not remove it.
	 * if you call setProperty("match", obj) on an already associated object you are likely to break something.
	 * 
	 * Override this method in subclasses to add specific handling for particular properties.
	 * @param property
	 * @param value
	 */
	public void setProperty (String property, Object value) {

		if (property.equals("match")) {
			if (value != null) { // && value instanceof HasProperties) {  // best here to throw a classcastexception if match isn't an hasprops as this really shouldn't happen... 
				this.match = (HasProperties) value;
				if (((HasProperties)value).getMatch() != this)
					((HasProperties)value).setProperty("match", this);
			}
		}
		else 
			if (value == null)
				properties.remove(property);
			else
				properties.put(property, value);
	}
	
	/**
	 * Merge copies all the properties of one hasprops object into another, overwriting existing properties.
	 * 
	 * @param props
	 */
	public HasProperties merge (HasProperties props) {
		
		if (props != null)
			for (String property : props.getProperties())
				setProperty (property, props.getProperty(property));
			
		return this;
	}
	
	/**
	 * returns the associated match hasProperties if it exists, null if it doesn't
	 * @return
	 */
	public HasProperties getMatch () {
		return match;
	}
	
	/**
	 * returns all property:value pairs, concatenated
	 * @return
	 */
	public String getDetails () {
		return properties.toString();
	}
	
	/** equals - rely on Object default method - its the same object...*/
	/** toString - define later...*/
}
