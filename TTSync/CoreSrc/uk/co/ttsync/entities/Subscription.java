package uk.co.ttsync.entities;

public class Subscription extends HasProperties {

	private static final long serialVersionUID = 1L;
	
	private User user;
	private Calendar calendar;
	private String role;

	public Subscription (User user, Calendar calendar, String role) {

		super();
		this.user = user;
		this.calendar = calendar;
		this.role = role;
		
		this.user.subscriptions.add(this);
		this.calendar.subscribers.add(this);
	}
	
	public User getUser() {
		return user;
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public String getRole() {
		return role;
	}		
	
	@Override
	public String getProperty (String property) {
		if (property.equals("user"))
			return getUser().toString();
		if (property.equals("calendar"))
			return getCalendar().toString();
		if (property.matches("^user.*"))
			return getUser().getProperty (property.substring(5));
		if (property.matches("^calendar.*"))
			return getCalendar().getProperty (property.substring(9));
		if (property.equals("role"))
			return getRole().toString();
		return super.getProperty(property); 
	}
	
	@Override
	public void setProperty (String property, Object value) {
		if (property.equals("role"))
			this.role = value.toString();
		else
			super.setProperty(property, value);
	}
	
	@Override
	public String toString () {
		return getUser () + " is subscribed to " + getCalendar () + " as " + getRole ();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((calendar == null) ? 0 : calendar.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subscription other = (Subscription) obj;
		if (calendar == null) {
			if (other.calendar != null)
				return false;
		} else if (!calendar.equals(other.calendar))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String getDetails () {
		return toString();
	}
}