package uk.co.ttsync.entities;

import org.joda.time.DateTime;

/**
 * Basic class to represent events. Override it with oddities of your specific event source.
 * 
 * @author jim
 *
 */

public class Event extends HasProperties implements Comparable <Event> {

	private static final long serialVersionUID = 1L;
	
	protected Calendar calendar;
	protected DateTime start, finish;

	protected User teacher;
	
	public Event (Calendar calendar) {
		this.calendar = calendar;
		start = new DateTime();
		finish = new DateTime();
	}

	public Calendar getCalendar () {
		return calendar;
	}
	
	public DateTime getStart () {
		return start;
	}
	
	public DateTime getFinish () {
		return finish;
	}
	
	public User getTeacher () {
		return teacher;
	}
	
	public void setTeacher (User teacher) {
		this.teacher = teacher;
	}
	
	@Override
	public String getProperty (String property) {
		switch (property) {
			case "date" :
				return getStart().toString("yyyy-MM-dd");
			case "start" :
				return getStart().toString("HH:mm:ss");
			case "finish" :
				return getFinish().toString("HH:mm:ss");
			case "subject" :
				if (calendar != null)
					return calendar.getProperty("subject");
		}
		
		if (property.matches("^teacher.*"))
			return getTeacher().getProperty (property.substring(8));

		if (property.matches("^calendar.*"))
			return getCalendar().getProperty(property.substring(9));
		return super.getProperty(property);
		
	}
	
	@Override
	public int compareTo(Event event) {
		if (event.getCalendar() == null || ! event.getCalendar().equals(this.getCalendar()))
			return -1;
		else
			return this.start.compareTo(event.start);
	}
	
	@Override
	public boolean equals (Object obj) {
		if (obj instanceof Event) {
			Event compare = (Event) obj;
			if (this.getCalendar() == null)
				return false;
			return (this.getCalendar().equals(compare.getCalendar()))
				&& this.getStart().equals(compare.getStart());
		}
		else
			return false;
	}

	@Override
	public String toString () {
		return 	getCalendar () + ": " + 
				getProperty("date") + 
				" from " + getProperty ("start") + 
				" - " + getProperty ("finish") + 
				" - " + getProperty ("subject") + " - " +
				(teacher != null ? " with " + teacher : "") +
 				(getProperty ("location") != null ? " in " + getProperty ("location") : "");
	}
	
	@Override 
	public String getDetails() {
		return toString ();
	}
}