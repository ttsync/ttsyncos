package uk.co.ttsync.entities.serializers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;

import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.entities.dtos.Dto;

public class MisDtoSerializer extends BasicSerializer <Dto> {

	@Override
	public void serialize(OutputStream output, Dto obj) throws IOException {
		super.serialize(output, obj);
	}

	@Override
	public Dto deserialize(InputStream input) throws IOException,
			ClassNotFoundException {
		
		Dto dto = super.deserialize(input);
		
		for (User user : dto.users) { 
			for (Membership m : new HashSet<Membership> (user.getMembership()))
//				new Membership (user, m.getGroup(), m.getRole());
				m.getGroup().getMembers().add(m);

			
			for (Subscription s : new HashSet <Subscription>(user.getSubscriptions()))
//				new Subscription (user, s.getCalendar(), s.getRole());
				s.getCalendar().getSubscribers().add(s);
		}
		
		return dto;
	}
}
