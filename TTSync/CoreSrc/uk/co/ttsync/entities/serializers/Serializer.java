package uk.co.ttsync.entities.serializers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

public interface Serializer <T extends Serializable> {

	void serialize (OutputStream output, T obj) throws IOException;
	T deserialize (InputStream input) throws IOException, ClassNotFoundException;
}
