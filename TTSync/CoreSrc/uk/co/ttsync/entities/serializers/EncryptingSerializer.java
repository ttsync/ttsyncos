package uk.co.ttsync.entities.serializers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Wrapper for Serializer objects that provides encryption.
 * @author jim
 *
 */
public class EncryptingSerializer <T extends Serializable> implements Serializer <T>{
	
	private Serializer <T> serializer;
	private Cipher cipher;
	private SecretKeySpec k;
	
	public EncryptingSerializer(Serializer <T> serializer, String key) {
		this.serializer = serializer;
		try {
			this.cipher = Cipher.getInstance("AES");
			k = new SecretKeySpec(key.getBytes(), "AES");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void serialize(OutputStream output, T obj)
			throws IOException {
		try {
			cipher.init(Cipher.ENCRYPT_MODE, k);
			serializer.serialize(new CipherOutputStream(output, cipher), obj);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}		
	}

	@Override
	public T deserialize(InputStream input) throws IOException,
			ClassNotFoundException {

		try {
			cipher.init(Cipher.DECRYPT_MODE, k);
			return serializer.deserialize(new CipherInputStream(input, cipher));
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			return null;			
		}
	}
}

/*
byte[] key = //... secret sequence of bytes
byte[] dataToSend = ...

Cipher c = Cipher.getInstance("AES");
SecretKeySpec k =
  new SecretKeySpec(key, "AES");
c.init(Cipher.ENCRYPT_MODE, k);
byte[] encryptedData = c.doFinal(dataToSend);

// now send encryptedData to Bob...

	

byte[] key = //... we know the secret!
byte[] encryptedData = //... received from Alice

Cipher c = Cipher.getInstance("AES");
SecretKeySpec k =
  new SecretKeySpec(key, "AES");
c.init(Cipher.DECRYPT_MODE, k);
byte[] data = c.doFinal(encryptedData);

// do something with data
*/