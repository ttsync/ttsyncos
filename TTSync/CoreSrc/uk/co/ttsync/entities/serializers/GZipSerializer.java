package uk.co.ttsync.entities.serializers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZipSerializer <T extends Serializable> implements Serializer <T> {

	private Serializer<T> serializer;

	public GZipSerializer(Serializer <T> serializer) {
		this.serializer = serializer;
	}

	@Override
	public void serialize(OutputStream output, T obj) throws IOException {
		serializer.serialize (new BufferedOutputStream (new GZIPOutputStream (output)), obj);
	}

	@Override
	public T deserialize(InputStream input) throws IOException,	ClassNotFoundException {
		return serializer.deserialize(new BufferedInputStream (new GZIPInputStream (input)));
	}

}
