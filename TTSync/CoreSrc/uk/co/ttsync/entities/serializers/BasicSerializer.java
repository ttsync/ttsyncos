package uk.co.ttsync.entities.serializers;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;

public class BasicSerializer <T extends Serializable> implements Serializer <T> {

	public BasicSerializer () {}
	
	public void serialize (OutputStream output, T obj) throws IOException {

		ObjectOutputStream oos = new ObjectOutputStream (output);
				oos.writeObject (obj);
		oos.close ();
	}
	
	@SuppressWarnings("unchecked")
	public T deserialize (InputStream input) throws IOException, ClassNotFoundException {
		
		ObjectInputStream ois = new ObjectInputStream (input);
		return (T) ois.readObject();
	}
}
