package uk.co.ttsync.actions;

import java.util.ArrayList;
import java.util.List;

import uk.co.ttsync.process.ServiceProvider;

/**
 * This is the base class for Action classes.
 * Action classes implement the command pattern.
 * 
 * @author jim
 *
 * @param <S> - type type of Service Provider that action will need to run
 */
public abstract class Action <E, S extends ServiceProvider> {

	public static enum Type {ADD, UPDATE, REMOVE}
	
	private E entry;
	private List <String> changeDetails = new ArrayList <String> ();
	
	public Action (E entry) {
		this.entry = entry;
	}
		
	public abstract E run (S svc) throws Exception;
	public abstract Action.Type getActionType ();
	public abstract void setProperty (String propertyName, Object propertyValue);
	
	public E getEntry () {
		return entry;
	}
	
	protected void addChange (String change) {
		changeDetails.add(change);
	}
	
	@Override
	public String toString () {
		
		String result = "";
		
		for (String change : changeDetails)
			result += change + "; ";
		
		return result;
	}
}