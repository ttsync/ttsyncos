package uk.co.ttsync.process;

import java.util.Collection;
import java.util.HashSet;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.HasProperties;
import uk.co.ttsync.filter.TypeFilterer;
import uk.co.ttsync.matcher.AbstractTypeMatcher;
import uk.co.ttsync.matcher.Matches;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

/**
 * A ProcessManager provides methods for each step of the integration: 
 * 
 * - loading the current state 
 * 
 * - finding matches 
 * 
 * - applying filters 
 * 
 * - creating actions 
 * 
 * - commiting changes.
 * 
 * Each of these steps is done in turn, and labelled below.
 * 
 * @author jimster
 *
 * @param <E>
 */
public abstract class AbstractProcessManager<E extends HasProperties, A extends Action<E, S>, S extends ServiceProvider> {

	final protected Properties properties;

	final protected S svc;
	
	private final static DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
	private DateTime expiry;
	
	protected AbstractProcessManager(S svc, Properties properties) {
		
		this.properties = properties;
		this.svc = svc;
		
		// disable if after a certain date:
		expiry = DateTime.parse("2016-08-01", dateFormatter);
		if (properties.containsKey("licence.expiry"))
			expiry = DateTime.parse(properties.get("lience.expiry"), dateFormatter);
			
		if (expiry.isBeforeNow())
			for (String property : properties.keySet()) 
				if (property.matches ("^commit\\..*\\.actions$"))
					properties.setProperty(property, "");
	}

	/**
	 * This is the main run process for each type.
	 * Match -> filter -> create actions -> do stuff
	 * If anything goes wrong, catch exception and move on to next task... 
	 * @param misEntries
	 * @param groupwareEntries
	 * @throws Exception
	 */
	public void run (Collection<E> misEntries, Collection<E> groupwareEntries) {
		try {
//			if (properties.containsKey("matcher.matchers." + getTypeString())) {
			findMatches(misEntries, groupwareEntries);
//				if (properties.containsKey("filter." + getTypeString ())) {
			applyFilters();
//				if (properties.containsKey("action.id." + getTypeString()) || properties.containsKey("action.field." + getTypeString())) {
			createActions();
			commitChanges();
//				}
//			}
		}
		catch (Exception e) {
			System.out.println ("Aborted job for " + getTypeString() + " (This is not necessarily a bad thing). Details: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	// the type for logging: returns eg 'calendar for calendars
	protected abstract String getTypeString();

	/**
	 * This is a reference to the initial collection of google entries of this type loaded from file. 
	 * The collection itself will be altered by add and remove actions, and the items in it changed by 
	 * update actions.
	 */
	private Collection <E> gpwareEntries; 
	

	
	/**************** MATCHES ***********************/

	protected Matches<E> matches;
	private AbstractTypeMatcher<E,?> typeMatcher;  // used in the commitChanges method when a new entry is created
	
	
	protected abstract AbstractTypeMatcher<E,?> getTypeMatcher(Collection<E> misEntries, Collection<E> groupwareEntries)
			throws MalformedRuleException;
	
	public void findMatches(Collection<E> misEntries, Collection<E> groupwareEntries) 
			throws MalformedRuleException {
		
		this.gpwareEntries = groupwareEntries;
		
		typeMatcher = getTypeMatcher(misEntries,
				groupwareEntries);
		typeMatcher.run();
		matches = typeMatcher.getMatches();
	}

	
	/***************** FILTERING *****************************/

	protected Matches<E> filteredMatches;

	public Matches<E> getFilteredMatches() {
		return filteredMatches;
	}

	protected abstract TypeFilterer<E> getFilterer()
			throws MalformedRuleException;

	public Matches<E> applyFilters() throws MalformedRuleException {
		TypeFilterer<E> filterer = getFilterer();
		filteredMatches = filterer.process(matches);
		return filteredMatches;
	}

	// create actions:

	protected Collection<A> actions;

	protected abstract AbstractActionListBuilder<E, A> getActionListBuilder()
			throws MalformedRuleException;

	public Collection<A> createActions() throws MalformedRuleException {

		actions = new HashSet<A>();

		actions.addAll(getActionListBuilder().getCreateList(filteredMatches));
		actions.addAll(getActionListBuilder().getUpdateList(filteredMatches));
		actions.addAll(getActionListBuilder().getRemoveList(filteredMatches));

		return actions;
	}

	protected void add (E misEntry, E gwEntry) {
		matches.addMatch(misEntry, gwEntry);
		filteredMatches.addMatch(misEntry, gwEntry);
		gpwareEntries.add(gwEntry);		// add to main groupware entry list so when we write it back to file its included.
		typeMatcher.updateEntries(misEntry, gwEntry); // copy any necessary MIS<->GW details across 
	}
	
	protected void remove (E gwEntry) {
		matches.getUnmatchedGroupwareEntries().remove(gwEntry);
		filteredMatches.getUnmatchedGroupwareEntries().remove(gwEntry);
		gpwareEntries.remove(gwEntry);
	}
	
	public void commitChanges() throws Exception {

		String permittedActions = properties.getProperty("commit."
				+ getTypeString() + ".actions").toLowerCase();

		if (properties.getLogLevel() > 0)
			System.out.println ("Starting commit:");
			
		for (A action : actions) {
			boolean run = false;
			if (permittedActions.contains(action.getActionType().toString().toLowerCase()))
				run = true;

			System.out.println (action.getEntry().toString() + " - " + action.toString() + "...");
			if (run) {
				try {
					System.out.print ("...running...");
					E change = action.run(svc);
					System.out.println (".. success");
					switch (action.getActionType()) {
					case ADD:
						// change is a GROUPWARE entry!!! check this
						add (action.getEntry(), change);
						break;
					case REMOVE:
						remove (action.getEntry());
						break;
					case UPDATE:
					default:
						break;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				if (expiry.isBeforeNow())
					System.out.println("...not running (licence expired)");
				else
					System.out.println("...not flagged to run");
			}
		}
	}
}