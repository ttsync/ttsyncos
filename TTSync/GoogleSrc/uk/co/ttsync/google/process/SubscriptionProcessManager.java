package uk.co.ttsync.google.process;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.filter.SubscriptionFilterer;
import uk.co.ttsync.filter.TypeFilterer;
import uk.co.ttsync.google.actioners.SubscriptionActionListBuilder;
import uk.co.ttsync.google.actions.json.SubscriptionAction;
import uk.co.ttsync.google.matcher.SubscriptionTypeMatcher;
import uk.co.ttsync.matcher.AbstractTypeMatcher;
import uk.co.ttsync.matcher.Matches;
import uk.co.ttsync.process.AbstractProcessManager;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class SubscriptionProcessManager extends AbstractProcessManager<Subscription, SubscriptionAction, GoogleServiceProvider> {

	public SubscriptionProcessManager(GoogleServiceProvider svc, Properties properties) {
		super(svc, properties);
	}

	@Override
	protected String getTypeString() {
		return "subscription";
	}

	/**
	 * 
	 * @param misEntries - everyone who should be subscribed
	 * @param gwEntries - everyone who is subscribed
	 * @param userMatches - user map
	 * @throws Exception
	 */
	public void run(Collection<Subscription> misEntries, Collection<Subscription> gwEntries, Matches<User> userMatches) throws Exception {

		Map<User, User> filteredUsers = new HashMap<User, User>(userMatches.getMatches());

		Collection<Subscription> filteredMisEntries = new HashSet<Subscription>();
		for (Subscription subscription : misEntries) {
			User user = subscription.getUser();
//			Group group = membership.getGroup();
			if (filteredUsers.keySet().contains(user))
				filteredMisEntries.add(subscription);
		}

		Collection<Subscription> filteredGwEntries = new HashSet<Subscription>();
		for (Subscription subscription : gwEntries) {
			User user = subscription.getUser();
//			Group group = membership.getGroup();
			if (filteredUsers.values().contains(user))
				filteredGwEntries.add(subscription);
		}

		run(filteredMisEntries, filteredGwEntries);
	}
	
	/**
	 * we only want to deal with membership entries that relate to users AND groups we have not filtered out. remove them here
	 * @param misEntries
	 * @param gwEntries
	 * @param userMatches
	 * @param groupMatches
	 * @throws InapplicableRuleException 
	 * @throws MalformedRuleException 
	 */
	public void findMatches (Collection<Subscription> misEntries, Collection<Subscription> gwEntries, Matches<User> userMatches, Matches<Calendar> calendarMatches) throws MalformedRuleException {
		Collection<Subscription> filteredMisEntries = new HashSet <Subscription>();
		
		Map <User, User> filteredUsers = new HashMap <User, User> (userMatches.getMatches());
		Map <Calendar, Calendar> filteredCalendars = new HashMap <Calendar, Calendar> (calendarMatches.getMatches());
		
		for (Subscription subscription : misEntries) {
			User user = subscription.getUser();
			Calendar calendar = subscription.getCalendar();
			if (filteredUsers.values().contains(user))
				if (filteredCalendars.values().contains(calendar))
					filteredMisEntries.add(subscription);
		}
		
		Collection<Subscription> filteredGwEntries = new HashSet <Subscription>();
		for (Subscription subscription : gwEntries){
			User user = subscription.getUser();
			Calendar calendar = subscription.getCalendar();
			if (filteredUsers.keySet().contains(user))
				if (filteredCalendars.keySet().contains(calendar))
					filteredGwEntries.add(subscription);
		}
		
		findMatches (filteredMisEntries, filteredGwEntries);
	}
	
	@Override
	protected AbstractTypeMatcher<Subscription,?> getTypeMatcher(
			Collection<Subscription> misEntries,
			Collection<Subscription> groupwareEntries)
			throws MalformedRuleException {
		return new SubscriptionTypeMatcher (misEntries, groupwareEntries, properties);
	}

	@Override
	protected TypeFilterer<Subscription> getFilterer()
			throws MalformedRuleException {
		return new SubscriptionFilterer (properties);
	}

	protected AbstractActionListBuilder<Subscription, SubscriptionAction> getActionListBuilder()
			throws MalformedRuleException {
		return new SubscriptionActionListBuilder (properties);

	}
	
	@Override
	protected void remove (Subscription sub) {
		// remove sub from user
		sub.getUser().getSubscriptions().remove(sub);
		
		// remove sub from calendar
		sub.getCalendar().getSubscribers().remove(sub);
		
		super.remove (sub);
	}
}
