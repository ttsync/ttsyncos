package uk.co.ttsync.google.process;

import java.util.Collection;

import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.filter.GroupFilterer;
import uk.co.ttsync.filter.TypeFilterer;
import uk.co.ttsync.google.actioners.GroupActionListBuilder;
import uk.co.ttsync.google.actions.json.GroupAction;
import uk.co.ttsync.google.matcher.GroupTypeMatcher;
import uk.co.ttsync.matcher.AbstractTypeMatcher;
import uk.co.ttsync.process.AbstractProcessManager;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class GroupProcessManager extends AbstractProcessManager <Group, GroupAction, GoogleServiceProvider> {

	public GroupProcessManager (GoogleServiceProvider svc, Properties properties) {
		super(svc, properties);
	}
		
	@Override
	protected String getTypeString() {return "group";}

	@Override
	protected AbstractTypeMatcher<Group,?> getTypeMatcher(Collection <Group> misEntries,
			Collection <Group> groupwareEntries) throws MalformedRuleException {
		return new GroupTypeMatcher (misEntries, groupwareEntries, properties);
	}
	
	@Override
	protected TypeFilterer<Group> getFilterer() throws MalformedRuleException {
		return new GroupFilterer (properties);
	}
	
	@Override 
	protected AbstractActionListBuilder <Group, GroupAction> getActionListBuilder () throws MalformedRuleException {
		return new GroupActionListBuilder (properties);

	}
	
	@Override
	protected void remove (Group group) {
		// remove group's memberships...
		for (Membership mem : group.getMembers())
			mem.getUser().getMembership().remove(mem);
		
		// remove group's associated calendar
		group.getCalendar().setGroup(null);
		
		super.remove(group);
	}
}
