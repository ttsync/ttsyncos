package uk.co.ttsync.google.process;

import java.util.Collection;
import java.util.HashSet;

import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Event;
import uk.co.ttsync.filter.EventFilterer;
import uk.co.ttsync.filter.TypeFilterer;
import uk.co.ttsync.google.actioners.EventActionListBuilder;
import uk.co.ttsync.google.actions.json.EventAction;
import uk.co.ttsync.google.matcher.EventTypeMatcher;
import uk.co.ttsync.matcher.AbstractTypeMatcher;
import uk.co.ttsync.matcher.Matches;
import uk.co.ttsync.process.AbstractProcessManager;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class EventProcessManager extends AbstractProcessManager <Event, EventAction, GoogleServiceProvider> {

	public EventProcessManager(GoogleServiceProvider svc, Properties properties) {
		super(svc, properties);
	}

	@Override
	protected String getTypeString() {
		return "event";
	}


//	private Matches <Calendar> calendars;
	

	
	/**
	 * remove all events that aren't in our filtered calendars...
	 * 
	 * @param misEvents
	 * @param gwEvents
	 * @param filteredCalendars
	 * @throws MalformedRuleException
	 * @throws InapplicableRuleException
	 */
	public void findMatches(Collection<Event> misEvents, Collection<Event> gwEvents, Matches<Calendar> filteredCalendars) throws MalformedRuleException {
	
//		this.calendars = filteredCalendars;
		
		Collection <Event> filteredGWEvents = new HashSet <Event> ();
		for (Calendar calendar : filteredCalendars.getMatches().keySet())
			for (Event event : gwEvents)
				if (calendar.getEvents().contains(event))
					filteredGWEvents.add(event);
							
		Collection <Event> filteredMISEvents = new HashSet <Event> ();
		for (Calendar calendar : filteredCalendars.getMatches().values())  // go through filtered MIS calendars
			for (Event event : misEvents) 
				if (calendar.getEvents().contains(event))
					filteredMISEvents.add(event);	
		
		findMatches (filteredMISEvents, filteredGWEvents);
	}
	
	@Override
	protected AbstractTypeMatcher<Event,?> getTypeMatcher(
			Collection<Event> misEntries, Collection<Event> groupwareEntries)
			throws MalformedRuleException {
		return new EventTypeMatcher (misEntries, groupwareEntries, properties);
	}

	@Override
	protected TypeFilterer<Event> getFilterer() throws MalformedRuleException {
		return new EventFilterer (properties);
	}

	@Override
	protected AbstractActionListBuilder<Event, EventAction> getActionListBuilder()
			throws MalformedRuleException {
		return new EventActionListBuilder (properties);

	}
	
	@Override
	protected void remove (Event event) {
		// remove reference to this event in calendar...
		event.getCalendar().getEvents().remove(event);
		
		super.remove (event);
	}
}
