package uk.co.ttsync.google.process;

import java.util.Collection;

import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.filter.CalendarFilterer;
import uk.co.ttsync.filter.TypeFilterer;
import uk.co.ttsync.google.actioners.CalendarActionListBuilder;
import uk.co.ttsync.google.matcher.CalendarTypeMatcher;
import uk.co.ttsync.matcher.AbstractTypeMatcher;
import uk.co.ttsync.process.AbstractProcessManager;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class CalendarProcessManager extends AbstractProcessManager <Calendar, Action <Calendar, GoogleServiceProvider>, GoogleServiceProvider> {

	public CalendarProcessManager (GoogleServiceProvider svc, Properties properties) {
		super(svc, properties);
	}
		
	@Override
	protected String getTypeString() {return "calendar";}

	@Override
	protected AbstractTypeMatcher<Calendar,?> getTypeMatcher(Collection <Calendar> misEntries,
			Collection <Calendar> groupwareEntries) throws MalformedRuleException {
		return new CalendarTypeMatcher (misEntries, groupwareEntries, properties);
	}
	
	@Override
	protected TypeFilterer<Calendar> getFilterer() throws MalformedRuleException {
		return new CalendarFilterer (properties);
	}

	protected AbstractActionListBuilder<Calendar, Action<Calendar, GoogleServiceProvider>> getActionListBuilder () throws MalformedRuleException {
		return new CalendarActionListBuilder (properties);
	}
	
	@Override
	protected void remove (Calendar cal) {
		// remove association with group...
		cal.getGroup().setCalendar(null);
		
		// remove subscriptions...
		for (Subscription sub : cal.getSubscribers())
			sub.getUser().getSubscriptions().remove(sub);
		
		super.remove(cal);
	}
}

