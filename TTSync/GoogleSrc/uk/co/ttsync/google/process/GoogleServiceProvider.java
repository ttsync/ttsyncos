package uk.co.ttsync.google.process;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyFactory;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Map;

import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential.Builder;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.Base64;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.groupssettings.Groupssettings;
//import com.google.gdata.client.appsforyourdomain.AppsPropertyService;

import uk.co.ttsync.process.ServiceProvider;
import uk.co.ttsync.utils.Properties;

public class GoogleServiceProvider implements ServiceProvider {

	private final HttpTransport httpTransport;
	private final JacksonFactory jsonFactory = new JacksonFactory();

	private Builder builder;

	private GoogleCredential cred;
	private GoogleCredential.Builder serviceAccountCredentialBuilder;

	private Directory dir;
//	private AppsPropertyService appsSvc;
	private Calendar calSvc;
	private Groupssettings groupSvc;

//	private Map<Class<?>, Object> services = new HashMap<Class<?>, Object>();

	public GoogleServiceProvider(Properties properties) {

		if (properties.getLogLevel() > 2)
			System.out.println("Initiating Google Service provider");

		Proxy proxy = null;
		if (properties.containsKey("proxy.host"))
			try {
				if (properties.getLogLevel() > 4)
					System.out.println("Setting proxy...");

				String proxyHost = properties.getProperty("proxy.host");
				int proxyPort = Integer.parseInt(properties
						.getProperty("proxy.port"));
				proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
						proxyHost, proxyPort));
				if (properties.getLogLevel() > 4)
					System.out.println("... " + proxyHost + ":" + proxyPort);
			} catch (Exception e) {}
		
		
		httpTransport = new NetHttpTransport.Builder().setProxy(proxy).build();

		GoogleClientSecrets secrets = new GoogleClientSecrets();
		GoogleClientSecrets.Details details = new GoogleClientSecrets.Details();

		details.set("auth_uri", "https://accounts.google.com/o/oauth2/auth");
		details.set("client_id", properties.getProperty("google.clientid"));
		details.set("client_secret",
				properties.getProperty("google.clientsecret"));
		details.set("token_uri", "https://accounts.google.com/o/oauth2/token");
		details.set("redirect_uris",
				Arrays.asList("urn:ietf:wg:oauth:2.0:oob", "oob"));
		details.set("auth_provider_x509_cert_url", // ?? not understood?
				"https://www.googleapis.com/oauth2/v1/certs");

		secrets.setInstalled(details);

		builder = new GoogleCredential.Builder().setTransport(httpTransport)
				.setJsonFactory(jsonFactory);

		cred = builder.setClientSecrets(secrets).build();
		cred.setRefreshToken(properties.getProperty("google.refreshtoken"));

		
		serviceAccountCredentialBuilder = builder
				.setServiceAccountId(properties
						.getProperty("google.serviceaccount.id"));
		serviceAccountCredentialBuilder.setServiceAccountScopes(Arrays
				.asList("https://www.googleapis.com/auth/calendar"));
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			String pk = properties.getProperty("google.serviceaccount.pk");
			pk = pk.replaceAll("\\\\n", "\n");
			pk = pk.replaceAll("\\\\u003d", "=");
			byte[] decodedKey = Base64.decodeBase64(pk.getBytes());
			EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decodedKey);
			serviceAccountCredentialBuilder
					.setServiceAccountPrivateKey(keyFactory
							.generatePrivate(keySpec));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		dir = new Directory.Builder(cred.getTransport(), cred.getJsonFactory(),
				null).setHttpRequestInitializer(cred)
				.setApplicationName("ttsync").build();

//		appsSvc = new AppsPropertyService("CalSvc");
//		appsSvc.setOAuth2Credentials(cred);

		calSvc = new Calendar.Builder(cred.getTransport(),
				cred.getJsonFactory(), null).setHttpRequestInitializer(cred)
				.setApplicationName("CalSync").build();

		groupSvc = new Groupssettings.Builder(cred.getTransport(),
				cred.getJsonFactory(), null).setHttpRequestInitializer(cred)
				.setApplicationName("ttsync").build();

//		services.put(dir.getClass(), dir);
//		services.put(appsSvc.getClass(), appsSvc);
//		services.put(calSvc.getClass(), calSvc);
//		services.put(groupSvc.getClass(), groupSvc);
	}

	public GoogleCredential getCredential() {
		return cred;
	}

	// public Object getServiceProvider (Class <?> clazz) {
	// return services.get(clazz);
	// }

	public Directory getDir() {
		return dir;
	}

	public Calendar getCalSvcAsUser(String id) {
		GoogleCredential cred = serviceAccountCredentialBuilder
				.setServiceAccountUser(id).build();

		Calendar calSvc = new Calendar.Builder(cred.getTransport(),
				cred.getJsonFactory(), null).setHttpRequestInitializer(cred)
				.setApplicationName("ttsync").build();

		return calSvc;
	}

//	public AppsPropertyService getAppsSvc() {
//		return appsSvc;
//	}

	public Calendar getCalSvc() {
		return calSvc;
	}

	public Groupssettings getGroupSvc() {
		return groupSvc;
	}
}
