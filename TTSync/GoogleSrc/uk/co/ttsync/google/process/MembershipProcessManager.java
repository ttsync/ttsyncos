package uk.co.ttsync.google.process;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.filter.MembershipFilterer;
import uk.co.ttsync.filter.TypeFilterer;
import uk.co.ttsync.google.actioners.MembershipActionListBuilder;
import uk.co.ttsync.google.actions.json.MembershipAction;
import uk.co.ttsync.google.matcher.MembershipTypeMatcher;
import uk.co.ttsync.matcher.AbstractTypeMatcher;
import uk.co.ttsync.matcher.Matches;
import uk.co.ttsync.process.AbstractProcessManager;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class MembershipProcessManager extends AbstractProcessManager<Membership, MembershipAction, GoogleServiceProvider> {

	public MembershipProcessManager(GoogleServiceProvider svc, Properties properties) {
		super(svc, properties);
	}

	@Override
	protected String getTypeString() {
		return "membership";
	}

	@Override
	protected AbstractTypeMatcher<Membership,?> getTypeMatcher(Collection<Membership> misEntries, Collection<Membership> groupwareEntries)
			throws MalformedRuleException {
		return new MembershipTypeMatcher(misEntries, groupwareEntries, properties);
	}

	@Override
	protected TypeFilterer<Membership> getFilterer() throws MalformedRuleException {
		return new MembershipFilterer(properties);
	}

	protected AbstractActionListBuilder<Membership, MembershipAction> getActionListBuilder() throws MalformedRuleException {
		return new MembershipActionListBuilder(properties);

	}

	private Map<User, User> filteredUsers;
//	private Map<Group, Group> filteredGroups;
	
	public void run(Collection<Membership> misEntries, Collection<Membership> gwEntries, 
			Matches<User> userMatches, Matches <Group> groupMatches) throws Exception {

		filteredUsers = new HashMap<User, User>(userMatches.getMatches());
//		filteredGroups = new HashMap<Group, Group>(groupMatches.getMatches());		

		Collection<Membership> filteredMisEntries = new HashSet<Membership>();
		for (Membership membership : misEntries) {
			User user = membership.getUser();
//			Group group = membership.getGroup();
			if (filteredUsers.keySet().contains(user))
				filteredMisEntries.add(membership);
		}

		Collection<Membership> filteredGwEntries = new HashSet<Membership>();
		for (Membership membership : gwEntries) {
			User user = membership.getUser();
//			Group group = membership.getGroup();
			if (filteredUsers.values().contains(user))
				filteredGwEntries.add(membership);
		}

		run(filteredMisEntries, filteredGwEntries);
	}

//	@Override
//	protected void add(Membership misEntry, Membership gwEntry) {
//
//		super.add(misEntry, gwEntry);
//	}
	
	@Override
	protected void remove (Membership mem) {
		// remove membership from user
		mem.getUser().getMembership().remove(mem);
		
		// remove membership from group
		mem.getGroup().getMembers().remove(mem);
		
		super.remove(mem);
	}
}
