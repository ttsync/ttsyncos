package uk.co.ttsync.google.process;

import java.util.Collection;

import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.filter.TypeFilterer;
import uk.co.ttsync.google.filter.UserFilterer;
import uk.co.ttsync.google.actioners.UserActionListBuilder;
import uk.co.ttsync.google.actions.json.UserAction;
import uk.co.ttsync.google.matcher.UserTypeMatcher;
import uk.co.ttsync.matcher.AbstractTypeMatcher;
import uk.co.ttsync.process.AbstractProcessManager;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class UserProcessManager extends AbstractProcessManager <User, UserAction, GoogleServiceProvider> {

	public UserProcessManager (GoogleServiceProvider svc, Properties properties) {
		super(svc, properties);
	}
		
	@Override
	protected String getTypeString() {return "user";}

	@Override
	protected AbstractTypeMatcher<User,?> getTypeMatcher(Collection <User> misEntries,
			Collection <User> groupwareEntries) throws MalformedRuleException {
		return new UserTypeMatcher (misEntries, groupwareEntries, properties);
	}

	@Override
	protected TypeFilterer<User> getFilterer() throws MalformedRuleException {
		return new UserFilterer (properties);
	}

	protected AbstractActionListBuilder <User, UserAction> getActionListBuilder () throws MalformedRuleException {
		return new UserActionListBuilder (properties);
	}
	
	@Override
	protected void remove (User user) {
		// remove user's memberships - only need to unlink group as user is being ditched anyway, so assoc'd memberships with no group will go too.
		for (Membership mem : user.getMembership())
			mem.getGroup().getMembers().remove(mem);
		
		// remove user's subscriptions...
		for (Subscription sub : user.getSubscriptions())
			sub.getCalendar().getSubscribers().remove(sub);
		
		super.remove(user);
	}
}
//String [] idRules, String [] propertyRules, String split, String delim, int logLevel