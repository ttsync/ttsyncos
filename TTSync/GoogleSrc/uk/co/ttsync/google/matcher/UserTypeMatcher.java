package uk.co.ttsync.google.matcher;

import java.util.Collection;

import uk.co.ttsync.entities.User;
import uk.co.ttsync.matcher.RegexTypeMatcher;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class UserTypeMatcher extends RegexTypeMatcher<User> {
	
	public UserTypeMatcher(Collection<User> misEntries,
			Collection<User> groupwareEntries, Properties properties) throws MalformedRuleException {
		super(misEntries, groupwareEntries, properties);
	}

	@Override
	protected String getTypeString() {
		return "user";
	}
/*	
	public void updateEntries(User misEntry, User gwEntry) {
//		misEntry.setProperty("_googleId", gwEntry.getProperty("primaryEmail"));
		misEntry.setProperty("match", gwEntry);
		gwEntry.setProperty("match", misEntry);
	}
*/	
}
