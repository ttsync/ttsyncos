package uk.co.ttsync.google.matcher;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.joda.time.DateTime;

import uk.co.ttsync.entities.Event;
import uk.co.ttsync.matcher.AbstractEntryMatcher;
import uk.co.ttsync.matcher.Matches;
import uk.co.ttsync.mis.importer.MISEvent;

class EventEntryMatcher extends AbstractEntryMatcher <Event, UnequalDateTime> {

	private int tolerance;

	public EventEntryMatcher(String tolerance) {
		super("Match events in the same calendar that start within " + tolerance + " seconds of each other");
		this.tolerance = Integer.parseInt(tolerance.trim()) * 1000;  // milliseconds
	}

	@Override
	public Map<UnequalDateTime, Event> loadMISData(Collection<Event> misEntries) {
		return loadData (misEntries);
	}

	@Override
	public Map<UnequalDateTime, Event> loadGWData(Collection<Event> gwEntries) {
		return loadData(gwEntries);
	}

	private Map <UnequalDateTime, Event> loadData (Collection <Event> events) {
		
		Map <UnequalDateTime, Event> result = new HashMap <UnequalDateTime, Event> ();
		
		for (Event e : events) 
			if (isOurEvent (e))
				result.put(new UnequalDateTime(e.getStart()), e);
			
		return result;
	}
	
	static EventEntryMatcher getEntryMatcher (String rule, String delim) {
		String[] ruleParts = rule.split(delim);
		if (ruleParts[0].trim().equals("tolerance"))
			return new EventEntryMatcher (ruleParts[1]);
		else
			return null; //new RegexEntryMatcher<E>(rule, delim); // super.getEntryMatcher (ruleString);
	}

	/**
	 * Filter out events not created by us...call this at some point.
	 */
	public boolean isOurEvent(Event entry) {
		if (entry instanceof MISEvent)
			return true;
		return (entry.getProperty("extended.shared.createdBy") != null && entry.getProperty("extended.shared.createdBy").equals("ttsync"));
	}

	@Override
	protected void match(Matches<Event> matches, int logLevel) {
		
		int matchCount = 0;
		
		Map<UnequalDateTime, Event> misData = loadMISData(matches.getUnmatchedMISEntries());
		Map<UnequalDateTime, Event> gwData = loadGWData(matches.getUnmatchedGroupwareEntries());
		
		if (logLevel > 1)
			System.out.println(" matching: " + getDescription()	+ ":");
		
		if (logLevel > 4) {
			System.out.println("Mis entries:");
			for (UnequalDateTime regex : misData.keySet())
				System.out.println("\t" + regex.toString() + " : "
						+ misData.get(regex).getDetails());

			System.out.println("GW entries:");
			for (UnequalDateTime regex : gwData.keySet())
				System.out.println("\t" + regex.toString() + " : " + gwData.get(regex).getDetails());
		}
		
		// for each mis entry:
		for (UnequalDateTime misEntry : misData.keySet()) {
			// Iterate through all gwMatches, match first one we meet.
			Event gwEntry = null;
			
			Iterator <UnequalDateTime> iter = gwData.keySet().iterator();
			
			while (iter.hasNext() && gwEntry == null) {
				UnequalDateTime gw = iter.next(); 
				if (Math.abs(gw.getMilis() - misEntry.getMilis()) < tolerance) {
					gwEntry = gwData.get(gw);
					matches.addMatch(misData.get(misEntry), gwEntry);
				}
			}
			
			

			if (logLevel > 4)
				System.out.println("  Matched " + misData.get(misEntry) + " with " + gwEntry + " (" + misEntry + ")");
			
			matchCount++;
		}
		
		if (logLevel > 1)
			System.out.println("Finished " 
					+ " matcher " + getDescription() + "\n - matched " + matchCount + " entries leaving "
					+ matches.getUnmatchedMISEntries().size()
					+ " mis entries and "
					+ matches.getUnmatchedGroupwareEntries().size()
					+ " groupware entries. " + "(" + new DateTime() + ")");
	}
}

/** 
 * 
 * @author jim
 *
 */
class UnequalDateTime  {
	
	private DateTime time;
	
	UnequalDateTime (DateTime time) {
		this.time = time;
	}
	
	long getMilis () {
		return time.getMillis();
	}
	
	@Override
	public String toString () {
		return time.toString();
	}
}