package uk.co.ttsync.google.matcher;

import java.util.ArrayList;
import java.util.Collection;

import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.entities.Event;
import uk.co.ttsync.matcher.AbstractEntryMatcher;
import uk.co.ttsync.matcher.AbstractTypeMatcher;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class EventTypeMatcher extends AbstractTypeMatcher <Event, UnequalDateTime> {

	public EventTypeMatcher(Collection<Event> misEntries,
			Collection<Event> groupwareEntries, Properties properties)
			throws MalformedRuleException {
		super(misEntries, groupwareEntries, properties);
	}

	@Override
	protected String getTypeString() {
		return "event";
	}

	@Override
	protected Collection<? extends AbstractEntryMatcher<Event, UnequalDateTime>> getEntryMatchers()
			throws MalformedRuleException {
		
		ArrayList <EventEntryMatcher> result = new ArrayList <EventEntryMatcher> ();
		for (int i = 0; i < getRuleStrings().length; i++) {
			
			EventEntryMatcher matcher = EventEntryMatcher.getEntryMatcher(getRuleStrings()[i].trim(), delim);
			result.add(matcher);
		}
		return result;
	}
/*	
	@Override
	public void updateEntries(Event misEvent, Event gpwareEvent) {
		misEvent.setProperty("_googleid", gpwareEvent.getProperty("id"));
	}
*/
}
