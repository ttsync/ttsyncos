package uk.co.ttsync.google.matcher;

import java.util.Collection;

import uk.co.ttsync.matcher.RegexTypeMatcher;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class MembershipTypeMatcher  extends RegexTypeMatcher <Membership> {

	public MembershipTypeMatcher(Collection<Membership> misEntries,
			Collection<Membership> gpEntries, Properties properties)
			throws MalformedRuleException {
		super(misEntries, gpEntries, properties);
	}

	@Override
	protected String getTypeString() {
		return "membership";
	}

//	@Override 
//	protected void updateEntries(Membership gwEntry, Membership misMatch) {
//		misMatch.setProperty("_googleId", gwEntry.getProperty("id"));
//	}
}
