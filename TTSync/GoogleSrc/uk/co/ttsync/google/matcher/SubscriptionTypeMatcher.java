package uk.co.ttsync.google.matcher;

import java.util.Collection;

import uk.co.ttsync.matcher.RegexTypeMatcher;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class SubscriptionTypeMatcher extends RegexTypeMatcher <Subscription> {

	public SubscriptionTypeMatcher(Collection<Subscription> misEntries,
			Collection<Subscription> gwEntries, Properties properties)
			throws MalformedRuleException {
		super(misEntries, gwEntries, properties);
	}
	
	@Override
	protected String getTypeString() {
		return "subscription";
	}
/*	
	@Override
	public void updateEntries(Subscription gwEntry, Subscription misMatch) {	
		misMatch.setProperty("_googleId", gwEntry.getUser().getProperty("_googleId"));
	}
*/
}
