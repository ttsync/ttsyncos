package uk.co.ttsync.google.matcher;

import java.util.Collection;

import uk.co.ttsync.matcher.RegexTypeMatcher;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class GroupTypeMatcher extends RegexTypeMatcher <Group> {

	public GroupTypeMatcher(Collection<Group> misEntries,
			Collection<Group> groupwareEntries, Properties properties)
			throws MalformedRuleException {
		super(misEntries, groupwareEntries, properties);
	}

	@Override
	protected String getTypeString() {
		return "group";
	}
/*
	@Override
	public void updateEntries(Group misEntry, Group gwEntry) {
		misEntry.setProperty("_googleId", gwEntry.getProperty("id"));
	}
*/
}
