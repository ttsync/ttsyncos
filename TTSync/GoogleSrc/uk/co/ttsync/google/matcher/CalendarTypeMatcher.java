package uk.co.ttsync.google.matcher;

import java.util.Collection;

import uk.co.ttsync.matcher.RegexTypeMatcher;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class CalendarTypeMatcher extends RegexTypeMatcher <Calendar> {

	public CalendarTypeMatcher(Collection<Calendar> misEntries,
			Collection<Calendar> groupwareEntries, Properties properties)
			throws MalformedRuleException {
		super(misEntries, groupwareEntries, properties);
	}

	@Override
	protected String getTypeString() {
		return "calendar";
	}

/*
	@Override
	public void updateEntries(Calendar misEntry, Calendar gwEntry) {
		String googleId = gwEntry.getProperty("resourceEmail");
		// issue that resourceId doesnt always have domain on it...
//		if (! googleId.matches(".*@.*")) {
//			googleId = googleId + "@" + properties.getProperty("google.domain");
//			gwEntry.setProperty("resourceId", googleId);
//		}	
//		misEntry.setProperty("_googleId", googleId);
		super.updateEntries(misEntry, gwEntry);
	}
*/
}
