package uk.co.ttsync.google.entities;

import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import uk.co.ttsync.entities.Calendar;

public class GoogleEvent extends uk.co.ttsync.entities.Event {

	private static final long serialVersionUID = 1L;
	
	private static final DateTimeFormatter dtFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	private static final DateTimeFormatter dFormat = DateTimeFormat.forPattern("yyyy-MM-dd");
	
	public GoogleEvent(Calendar calendar, com.google.api.services.calendar.model.Event gEvent) {
		super (calendar);
		
		this.setProperty("_googleId", gEvent.getId());
		
		for (String key : gEvent.keySet()) {
			String value = gEvent.get(key).toString();
			if (! key.equals("extendedProperties"))
				setProperty(key, value);
		}

		if (gEvent.getExtendedProperties() != null) {
			Map <String, String> privProperties = gEvent.getExtendedProperties().getPrivate();
			if (privProperties != null)
				for (String property : privProperties.keySet())
					setProperty ("extended.private." + property, privProperties.get(property));
		
			Map <String, String> sharedProperties = gEvent.getExtendedProperties().getShared();
			if (sharedProperties != null)
				for (String property : sharedProperties.keySet())
					setProperty ("extended.shared." + property, sharedProperties.get(property));
		}			
		
		String gStart,gFinish;
		if (gEvent.getStart().getDateTime() != null) {
			gStart = gEvent.getStart().getDateTime().toString();
			gFinish = gEvent.getEnd().getDateTime().toString();
			this.start = DateTime.parse(gStart, dtFormat);
			this.finish = DateTime.parse(gFinish, dtFormat);
		}
		else { // all day event
			gStart = gEvent.getStart().getDate().toString();
			gFinish = gEvent.getEnd().getDate().toString();
			this.start = DateTime.parse(gStart, dFormat);
			this.finish = DateTime.parse(gFinish, dFormat);			
		}
				
	}

	@Override
	public String toString () {
		return 	getCalendar () + ": " + 
				getProperty("date") + 
				" from " + getProperty ("start") + 
				" - " + getProperty ("finish") + 
				" - " + getProperty ("summary") + " - " +
				(teacher != null ? " with " + teacher : "") +
 				(getProperty ("location") != null ? " in " + getProperty ("location") : "");
	}
}