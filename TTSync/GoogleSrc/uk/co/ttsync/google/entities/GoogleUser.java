package uk.co.ttsync.google.entities;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

import com.google.api.client.util.ArrayMap;
import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.UserName;

/**
 * GoogleUser stores user values from json user object with some refinements:
 * - givenName, fullName, familyName NOT in UserName JSON, but in their individual properties
 * - externalIDs stored as ext.<name> properties, where property maps to predefinded name <eg account, network etc) or custom extId
 *  
 * @author jim
 *
 */
public class GoogleUser extends uk.co.ttsync.entities.User {

	private static final long serialVersionUID = 1L;

	/**
	 * constructed by being passed a Google User object...
	 * 
	 * @param user
	 */
	public GoogleUser(User user) {

		
		for (String key : user.keySet()) {
			Object value = user.get(key);
			setProperty(key, value);
		}
	}

	@Override
	public void setProperty(String property, Object value) {

		if (property.equals("externalIds")) {
			@SuppressWarnings("unchecked")
			ArrayList <ArrayMap <String, String>> ids = (ArrayList <ArrayMap <String, String>>)value;
			for (ArrayMap <String, String> extId : ids) {
				String type = extId.get("type");
				if (type.equals("custom"))
					type = extId.get("customType");
				super.setProperty("ext."+new String(type), new String(extId.get("value")));  // otherwise it tries to serialise ArrayMap..?
			}
		}
		else if (property.equals("name")) {
			UserName username = (UserName) value;
			super.setProperty("fullname", username.getFullName());
			super.setProperty("forename", username.getGivenName());
			super.setProperty("surname", username.getFamilyName());
		}
		else if (property.equals("primaryEmail")) {
			super.setProperty("primaryEmail", value);
			super.setProperty("username", ((String)value).split("@")[0]);
		}
		else if (property.equals("emails")) {
			super.setProperty(property, value.toString());
		}
		else
			if (value != null)
				super.setProperty(property, value);
	}
	
	@Override
	public String toString() {
		return getProperty ("forename")+" "+getProperty ("surname") + " ("+getProperty ("primaryEmail")+")";
	}
}