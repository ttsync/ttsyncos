package uk.co.ttsync.google.entities;

import java.io.IOException;
import java.net.SocketTimeoutException;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.util.DateTime;
import com.google.api.services.admin.directory.model.CalendarResource;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

public class GoogleCalendar extends uk.co.ttsync.entities.Calendar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	private List <Calendar.Acl> acls

	// set in GoogleGroupDao...
	public static int logLevel;
		
	/**
	 * JSON constructor...
	 * @param entry
	 * @throws IOException
	 */
	public GoogleCalendar(CalendarResource entry) throws IOException {
		setProperty("_googleId", entry.getResourceId());
		
		for (String property : entry.keySet())
			setProperty (property, entry.get(property).toString());
		
		setProperty ("name",entry.getResourceDescription());
	}
	
	/**
	 * Reads events from specified calendar. Only read days from now - daysPast to now + daysFuture
	 * @param calSvc
	 * @param readPast
	 * @param readFuture
	 * @throws IOException
	 */
	public void readEvents (Calendar calSvc, int daysPast, int daysFuture) throws IOException {
	
		if (logLevel > 0)
			System.out.println (" Reading events for calendar " + getProperty ("name") + "(" + getProperty("_googleId") + ")");
		
		org.joda.time.DateTime minDate = new org.joda.time.DateTime();
		minDate = minDate.minusDays(daysPast);
		
		org.joda.time.DateTime maxDate = new org.joda.time.DateTime();
		maxDate = maxDate.plusDays(daysFuture);
		
//		System.out.println (minDate.toString()+" <-> " +DateTime.parseRfc3339(minDate.toString()));
//		minDate.getMillis();
		
		int count = 0;
		Calendar.Events.List eventList = calSvc.events().list(getProperty("resourceEmail"))
				.setTimeMin(DateTime.parseRfc3339(minDate.toString()))
				.setTimeMax(DateTime.parseRfc3339(maxDate.toString()));
				
		
		
		String pageToken = "";
		do {
			try {
				Events events = eventList.execute();
				for (Event event : events.getItems()) {
					GoogleEvent gEvent = new GoogleEvent (this, event);
					this.addEvent(gEvent);
					if (logLevel > 1)
						System.out.println ("... importing GW event " + gEvent);

					count++;
					System.out.print(".");
					if (count % 50 == 0)
						System.out.println(" " + count);
				}
				pageToken = events.getNextPageToken();
				eventList.setPageToken(pageToken);
			}
			catch (GoogleJsonResponseException | SocketTimeoutException e) {
				System.out.println ("Failed to read events - retrying... (" + e.getMessage() + ")");
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					Thread.currentThread().interrupt();
				}
			}
		} while (pageToken != null);
		

		if (logLevel > 0)
			System.out.println (" ..read " + this.getEvents().size() + " events - " + new org.joda.time.DateTime());
	}
/*	
	public void readAcls (Calendar calSvc) throws IOException {
	
		if (logSummary)
			System.out.println (" Reading ACLs for calendar " + getProperty ("name") + "(" + getId() + ")");
		
		Calendar.Acl.List aclList = calSvc.acl().list(getId());
		String pageToken = "";
		do {
			try {
				Acl acls = aclList.execute();
				for (AclRule acl : acls.getItems()) {
			
					Scope scope = acl.getScope();
					String role = acl.getRole();
					if (logEvents)
						System.out.println ("... added acl " );
				}
				pageToken = acls.getNextPageToken();
				aclList.setPageToken(pageToken);
			}
			catch (GoogleJsonResponseException | SocketTimeoutException e) {
				System.out.println ("Failed to read events - retrying...");
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					Thread.currentThread().interrupt();
				}
			}
		} while (pageToken != null);
		
		if (logSummary)
			System.out.println ("   finished reading ACLs - " + " ACLs found - " + new Date());
		
	}
*/
	@Override
	public String toString () {
		return this.getProperty("name");
	}
}
