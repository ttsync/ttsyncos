package uk.co.ttsync.google.entities;

import com.google.api.services.admin.directory.model.Group;

public class GoogleGroup extends uk.co.ttsync.entities.Group {

	private static final long serialVersionUID = 1L;
	
	/**
	 * constructed by being passed a google group object...
	 * @param group
	 */
	public GoogleGroup (Group group) {
	// pretty sure this can go...
		this.setProperty("_googleId", group.getId ());
		
		for (String key : group.keySet()) {
			String value = group.get(key).toString();
			setProperty(key, value);
		}
	}
	
	@Override
	public String toString () {
		return super.toString () + " (gw)";
	}
}