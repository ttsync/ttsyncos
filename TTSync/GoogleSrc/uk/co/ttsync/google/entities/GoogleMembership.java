package uk.co.ttsync.google.entities;

import com.google.api.services.admin.directory.model.Member;
import uk.co.ttsync.entities.Membership;

public class GoogleMembership extends Membership {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static enum Role {
		OWNER("Owner"),
		MANAGER ("Manager"),
		MEMBER ("Member");
		
		private String roleName;
		
		Role (String roleName) {
			this.roleName = roleName;
		}
		
		public String toString () {
			return roleName;
		}
		
		public static Role getRole (String roleName) {
			switch (roleName.toLowerCase()) {
				case "owner" :
					return OWNER;
				case "manager" :
					return MANAGER;
				case "member" :
					return MEMBER;
				default :
					return null;
			}
		}
	}
	
	public GoogleMembership (GoogleUser user, GoogleGroup group, Member member) {
		super(user, group, member.getRole());
		
		this.setProperty("_googleId", member.getId());
		for (String key : member.keySet()) {
			String value = member.get(key).toString();
			setProperty(key, value);
		}
	}
	
	public GoogleUser getUser() {
		return (GoogleUser) super.getUser();
	}

	public GoogleGroup getGroup() {
		return (GoogleGroup) super.getGroup ();
	}
}