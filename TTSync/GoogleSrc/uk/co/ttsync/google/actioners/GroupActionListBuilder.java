package uk.co.ttsync.google.actioners;

import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.google.actions.json.CreateGroupAction;
import uk.co.ttsync.google.actions.json.GroupAction;
import uk.co.ttsync.google.actions.json.RemoveGroupAction;
import uk.co.ttsync.google.actions.json.UpdateGroupAction;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class GroupActionListBuilder extends AbstractActionListBuilder <Group, GroupAction> {

	private String domain;

	public GroupActionListBuilder (Properties properties) throws MalformedRuleException {
		super(properties);
		domain = properties.getProperty("google.domain");
	}

	@Override
	protected String getTypeString() {
		return "group";
	}

	@Override
	protected GroupAction getCreateAction(String id, Group group) {
//		return new CreateGroupAction (id.replaceAll("[@/\\s]", ""), domain, group);
		return new CreateGroupAction (id, domain, group);
	}

	@Override
	protected GroupAction getUpdateAction(Group misEntry, Group gwEntry) {
		return new UpdateGroupAction (misEntry, gwEntry);
	}

	@Override
	protected GroupAction getRemoveAction(Group group) {
		return new RemoveGroupAction (group);
	}

}
