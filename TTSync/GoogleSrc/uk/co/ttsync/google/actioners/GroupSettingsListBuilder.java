package uk.co.ttsync.google.actioners;

import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.google.actions.json.UpdateGroupSettingsAction;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class GroupSettingsListBuilder extends AbstractActionListBuilder <Group, UpdateGroupSettingsAction> {

	public GroupSettingsListBuilder (Properties properties) throws MalformedRuleException {
		super(properties);
	}

	@Override
	protected String getTypeString() {
		return "groupSettings";
	}

	@Override
	protected UpdateGroupSettingsAction getCreateAction(String id,
			Group newEntry) {
		return null;
	}

	@Override
	protected UpdateGroupSettingsAction getUpdateAction(Group misEntry, Group gpwareEntry) {
		return new UpdateGroupSettingsAction (misEntry.getProperty("_googleId"), misEntry);
	}

	@Override
	protected UpdateGroupSettingsAction getRemoveAction(Group entity) {
		return null;
	}
}
