package uk.co.ttsync.google.actioners;

import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.google.actions.json.CreateMembershipAction;
import uk.co.ttsync.google.actions.json.MembershipAction;
import uk.co.ttsync.google.actions.json.RemoveMembershipAction;
import uk.co.ttsync.google.actions.json.UpdateMembershipAction;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class MembershipActionListBuilder extends
		AbstractActionListBuilder<Membership, MembershipAction> {
	public MembershipActionListBuilder (Properties properties) throws MalformedRuleException {
		super(properties);
	}

	@Override
	protected String getTypeString() {
		return "membership";
	}

	@Override
	protected CreateMembershipAction getCreateAction(String id, Membership membership) {
		return new CreateMembershipAction (membership);
	}

	@Override
	protected MembershipAction getUpdateAction(Membership misMembership, Membership gpwareMembership) {
		return new UpdateMembershipAction (gpwareMembership);
	}

	@Override
	protected MembershipAction getRemoveAction(Membership membership) {
		return new RemoveMembershipAction (membership);
	}

}
