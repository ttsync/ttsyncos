package uk.co.ttsync.google.actioners;

import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.google.actions.json.CreateSubscriptionAction;
import uk.co.ttsync.google.actions.json.RemoveSubscriptionAction;
import uk.co.ttsync.google.actions.json.SubscriptionAction;
import uk.co.ttsync.google.actions.json.UpdateSubscriptionAction;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class SubscriptionActionListBuilder extends AbstractActionListBuilder <Subscription, SubscriptionAction> {

	public SubscriptionActionListBuilder (Properties properties) throws MalformedRuleException {
		super(properties);
	}

	@Override
	protected String getTypeString() {
		return "subscription";
	}

	@Override
	protected SubscriptionAction getCreateAction(String id, Subscription newEntry) {
		return new CreateSubscriptionAction (id, newEntry);
	}

	@Override
	protected SubscriptionAction getUpdateAction(Subscription misSubscription, Subscription gpwareSubscription) {
		return new UpdateSubscriptionAction (misSubscription);
	}

	@Override
	protected SubscriptionAction getRemoveAction(Subscription entity) {
		return new RemoveSubscriptionAction (entity);
	}

}
