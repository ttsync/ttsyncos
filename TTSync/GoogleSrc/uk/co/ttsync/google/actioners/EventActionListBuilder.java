package uk.co.ttsync.google.actioners;

import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.entities.Event;
import uk.co.ttsync.google.actions.json.CreateEventAction;
import uk.co.ttsync.google.actions.json.EventAction;
import uk.co.ttsync.google.actions.json.RemoveEventAction;
import uk.co.ttsync.google.actions.json.UpdateEventAction;
import uk.co.ttsync.google.entities.GoogleEvent;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class EventActionListBuilder extends AbstractActionListBuilder <Event, EventAction> {

	public EventActionListBuilder (Properties properties) throws MalformedRuleException {
		super(properties);
	}

	@Override
	protected String getTypeString() {
		return "event";
	}

	@Override
	protected EventAction getCreateAction(String id, Event newEntry) {
		CreateEventAction action = new CreateEventAction (id, newEntry);
		action.setProperty("extended.shared.createdBy", "ttsync");
		return action;
	}

	@Override
	protected EventAction getUpdateAction(Event misEvent, Event gpwareEvent) {
		return new UpdateEventAction ((GoogleEvent)gpwareEvent);
	}

	@Override
	protected EventAction getRemoveAction(Event entity) {
		return new RemoveEventAction ((GoogleEvent)entity);
	}
}
