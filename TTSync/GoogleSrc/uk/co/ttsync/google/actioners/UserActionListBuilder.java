package uk.co.ttsync.google.actioners;

import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.google.actions.json.CreateUserAction;
import uk.co.ttsync.google.actions.json.RemoveUserAction;
import uk.co.ttsync.google.actions.json.UpdateUserAction;
import uk.co.ttsync.google.actions.json.UserAction;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class UserActionListBuilder extends AbstractActionListBuilder <User, UserAction> {

	private String domain;
	
	public UserActionListBuilder (Properties properties) throws MalformedRuleException {
		super(properties);
		domain = properties.getProperty("google.domain");
	}

	@Override
	protected String getTypeString () {
		return "user";
	}

	@Override
	protected CreateUserAction getCreateAction(String id, User user) {
		return new CreateUserAction (id.replaceAll("[@/\\s]", ""), domain, user);
	}

	@Override
	protected UserAction getUpdateAction(User misUser, User gpwareUser) {
		return new UpdateUserAction (misUser, gpwareUser);
	}

	@Override
	protected UserAction getRemoveAction(User user) {
		return new RemoveUserAction (user);
	}
}
