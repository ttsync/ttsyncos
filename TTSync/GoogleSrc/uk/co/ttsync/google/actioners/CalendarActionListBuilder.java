package uk.co.ttsync.google.actioners;

import uk.co.ttsync.actioners.AbstractActionListBuilder;
import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Calendar;
//import uk.co.ttsync.google.actions.gdata.CreateCalendarAction;
import uk.co.ttsync.google.actions.json.CreateCalendarAction;
import uk.co.ttsync.google.actions.json.RemoveCalendarAction;
import uk.co.ttsync.google.actions.json.UpdateCalendarAction;
import uk.co.ttsync.google.entities.GoogleCalendar;
import uk.co.ttsync.google.process.GoogleServiceProvider;
import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class CalendarActionListBuilder extends AbstractActionListBuilder <Calendar, Action <Calendar, GoogleServiceProvider>> {

//	private String domain;

	public CalendarActionListBuilder (Properties properties) throws MalformedRuleException {
		super(properties);
//		this.domain = properties.getProperty("google.domain");
	}

	@Override
	protected String getTypeString () {
		return "calendar";
	}

	@Override
	protected CreateCalendarAction getCreateAction(String id, Calendar calendar) {
//		return new CreateCalendarAction (id+"@"+domain, calendar);
		return new CreateCalendarAction (id, calendar);
	}

	@Override
	protected UpdateCalendarAction getUpdateAction(Calendar misCalendar, Calendar gpwareCalendar) {
		return new UpdateCalendarAction ((GoogleCalendar)gpwareCalendar);
	}

	@Override
	protected RemoveCalendarAction getRemoveAction(Calendar calendar) {
		return new RemoveCalendarAction ((GoogleCalendar)calendar);
	}
}
