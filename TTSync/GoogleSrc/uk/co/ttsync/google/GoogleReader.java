package uk.co.ttsync.google;

import java.io.FileOutputStream;
import java.io.IOException;

import uk.co.ttsync.entities.dtos.Dto;
import uk.co.ttsync.entities.serializers.GZipSerializer;
import uk.co.ttsync.entities.serializers.Serializer;
import uk.co.ttsync.google.importer.GoogleDtoSerializer;
import uk.co.ttsync.google.importer.GoogleImporter;
import uk.co.ttsync.utils.Properties;

public class GoogleReader {

	public GoogleReader(Properties properties) throws IOException {

		GoogleImporter gwImporter = new GoogleImporter (properties);
		gwImporter.readUsers();
		gwImporter.readGroups();
		gwImporter.readMembership();
		gwImporter.readCalendars();
		gwImporter.readSubscriptions();
		gwImporter.readEvents();

		System.out.println ("Finished reading data");
		Dto googleDto;
		googleDto = gwImporter.getDto();

		Serializer <Dto> serializer = new GZipSerializer <Dto> (new GoogleDtoSerializer ());

		// write it to file:
		String googleFile = properties.getProperty("google.file");
		FileOutputStream fos = new FileOutputStream (googleFile);
		serializer.serialize(fos, googleDto);
		fos.close ();
		System.out.println ("Written Google data to file " + googleFile + ".");
	}
}
