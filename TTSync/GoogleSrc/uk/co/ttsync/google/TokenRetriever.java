
package uk.co.ttsync.google;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Arrays;
import java.util.Collection;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.admin.directory.DirectoryScopes;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.groupssettings.GroupssettingsScopes;
import uk.co.ttsync.utils.Properties;

public class TokenRetriever {

	private static Collection <String> clientScopes = Arrays.asList(
		 "https://apps-apis.google.com/a/feeds/calendar/resource/",
		 DirectoryScopes.ADMIN_DIRECTORY_USER, 		//  https://www.googleapis.com/auth/admin.directory.user
		DirectoryScopes.ADMIN_DIRECTORY_GROUP,		//  https://www.googleapis.com/auth/admin.directory.group
		DirectoryScopes.ADMIN_DIRECTORY_RESOURCE_CALENDAR,
		CalendarScopes.CALENDAR,					// https://www.googleapis.com/auth/calendar
		GroupssettingsScopes.APPS_GROUPS_SETTINGS	// https://www.googleapis.com/groups/v1/groups/
		
	);

	public TokenRetriever (Properties properties) throws IOException {
		
		Proxy proxy = null;
		if (properties.containsKey("proxy.host"))
			try {
				if (properties.getLogLevel() > 4)
					System.out.println("Setting proxy...");

				String proxyHost = properties.getProperty("proxy.host");
				int proxyPort = Integer.parseInt(properties
						.getProperty("proxy.port"));
				proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
						proxyHost, proxyPort));
				if (properties.getLogLevel() > 4)
					System.out.println("... " + proxyHost + ":" + proxyPort);
			} catch (Exception e) {}
		
		
		HttpTransport httpTransport = new NetHttpTransport.Builder().setProxy(proxy).build();
		JacksonFactory jsonFactory = new JacksonFactory();

			
		GoogleClientSecrets secrets = new GoogleClientSecrets();
		GoogleClientSecrets.Details details = new GoogleClientSecrets.Details();
			details.set("auth_uri", "https://accounts.google.com/o/oauth2/auth");
			details.set("token_uri", "https://accounts.google.com/o/oauth2/token");
			details.set("redirect_uris", Arrays.asList("urn:ietf:wg:oauth:2.0:oob", "oob"));
			details.set("auth_provider_x509_cert_url","https://www.googleapis.com/oauth2/v1/certs");

			details.set("client_id",properties.getProperty("google.clientid"));
			details.set("client_secret", properties.getProperty("google.clientsecret"));
		
		secrets.setInstalled(details);
		
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
	            httpTransport, jsonFactory, secrets, clientScopes).
	        build();
	    Credential cred = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("");
	    
	    System.out.println ("Refresh token: "+cred.getRefreshToken());
	}
}
