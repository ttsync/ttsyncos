package uk.co.ttsync.google.filter;

import uk.co.ttsync.utils.Properties;
import uk.co.ttsync.utils.regex.MalformedRuleException;

public class UserFilterer extends uk.co.ttsync.filter.UserFilterer {

	public UserFilterer(Properties properties) throws MalformedRuleException {
		super(properties);

		this.filters.add(0, new DefaultUserFilter ());
	}

}
