package uk.co.ttsync.google.filter;

import uk.co.ttsync.utils.regex.MalformedRuleException;
import uk.co.ttsync.utils.regex.PropertiesFilter;
import uk.co.ttsync.utils.regex.RegexEvaluator;

/**
 * Filter that is run first - stops anyone deletting admins...
 * @author jim
 *
 * @param <E>
 */
class DefaultUserFilter extends PropertiesFilter {
	
	DefaultUserFilter() throws MalformedRuleException {	
		super(new RegexEvaluator ("isAdmin", "true"), Action.IGNORE, true, "If property isAdmin == true IGNORE");
	}
}
