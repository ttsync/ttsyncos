package uk.co.ttsync.google;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import uk.co.ttsync.MISAgent;
import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.entities.dtos.Dto;
import uk.co.ttsync.entities.serializers.EncryptingSerializer;
import uk.co.ttsync.entities.serializers.GZipSerializer;
import uk.co.ttsync.entities.serializers.MisDtoSerializer;
import uk.co.ttsync.entities.serializers.Serializer;
import uk.co.ttsync.google.GoogleReader;
import uk.co.ttsync.google.TokenRetriever;
import uk.co.ttsync.google.importer.GoogleDtoSerializer;
import uk.co.ttsync.google.process.CalendarProcessManager;
import uk.co.ttsync.google.process.EventProcessManager;
import uk.co.ttsync.google.process.GoogleServiceProvider;
import uk.co.ttsync.google.process.GroupProcessManager;
import uk.co.ttsync.google.process.MembershipProcessManager;
import uk.co.ttsync.google.process.SubscriptionProcessManager;
import uk.co.ttsync.google.process.UserProcessManager;
import uk.co.ttsync.utils.Properties;

public class GoogleSync {

	public GoogleSync(Properties properties) {

		try {
			File mFile = new File(properties.getProperty("mis.file"));
			if (!mFile.exists())
				new MISAgent(properties);

			if (!properties.containsKey("google.refreshtoken")) {
				new TokenRetriever(properties);
				return;
			}

			File gFile = new File(properties.getProperty("google.file"));
			if (!gFile.exists())
				new GoogleReader(properties);

			sync(properties);
		} catch (Exception e) {
			System.out.println("Caught exception - " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void sync(Properties properties) throws Exception {

		// import google from file:
		Serializer<Dto> googleSerializer = new GZipSerializer<Dto>(new GoogleDtoSerializer());
		String googleFile = properties.getProperty("google.file");
		FileInputStream fis = new FileInputStream(googleFile);
		Dto googleDto = googleSerializer.deserialize(fis);
		fis.close();

		Serializer<Dto> misSerializer = new GZipSerializer<Dto>(new MisDtoSerializer());
		if (properties.containsKey("mailer.key")) 
			misSerializer = new EncryptingSerializer<Dto>(misSerializer,
					properties.getProperty("mailer.key"));
		fis = new FileInputStream(properties.getProperty("mis.file"));
		Dto misDto = misSerializer.deserialize(fis);
		fis.close();

		GoogleServiceProvider svc = new GoogleServiceProvider(properties);

		try {
			UserProcessManager userProcessManager = new UserProcessManager(svc, properties);
			userProcessManager.run(misDto.users, googleDto.users);

			GroupProcessManager groupProcessManager = new GroupProcessManager(svc, properties);
			groupProcessManager.run(misDto.groups, googleDto.groups);

			if (properties.containsKey("filter.user") && properties.containsKey("filter.group")) {
				MembershipProcessManager membershipProcessManager;

				// this is Gw -> MIS map.
				Map<Group, Group> groups = groupProcessManager.getFilteredMatches().getMatches();
				for (Group misGroup : groups.keySet()) {
					if (properties.getLogLevel() > 1)
						System.out.println("Doing membership for " + misGroup);

					membershipProcessManager = new MembershipProcessManager(svc, properties);
					membershipProcessManager.run(misGroup.getMembers(), groups.get(misGroup).getMembers(),
							userProcessManager.getFilteredMatches(), groupProcessManager.getFilteredMatches());
				}
			}

			CalendarProcessManager calendarProcessManager = new CalendarProcessManager(svc, properties);
			calendarProcessManager.run(misDto.calendars, googleDto.calendars);

			SubscriptionProcessManager subscriptionProcessManager;

			Map<Calendar, Calendar> calendars = calendarProcessManager.getFilteredMatches().getMatches();
			if (properties.containsKey("filter.user") && properties.containsKey("filter.subscription")) {
				for (Calendar misCal : calendars.keySet()) {
					if (properties.getLogLevel() > 1)
						System.out.println("Doing subscriptions for " + misCal);
					subscriptionProcessManager = new SubscriptionProcessManager(svc, properties);
					subscriptionProcessManager.run(misCal.getSubscribers(), calendars.get(misCal).getSubscribers(),
							userProcessManager.getFilteredMatches());
				}
			}

			if (properties.containsKey("filter.calendar")) {
				EventProcessManager eventProcessManager;
				for (Calendar misCal : calendars.keySet()) {
					if (properties.getLogLevel() > 1)
						System.out.println("Doing events for " + misCal);
					eventProcessManager = new EventProcessManager(svc, properties);
					eventProcessManager.run(misCal.getEvents(), calendars.get(misCal).getEvents());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Updating google data file...");
		FileOutputStream fos = new FileOutputStream(googleFile);
		googleSerializer.serialize(fos, googleDto);
		fos.close();

		System.out.println("the end...");
	}

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("Launcher needs an argument of the name of at least one properties file");
			System.exit(0);
		}

		Properties properties = new Properties();

		System.out.println("Loading configuration:");
		for (String fileName : args) {
			try {
				properties.load(new FileReader(fileName));
			} catch (IOException e) {
				System.out.println("Failed to read file " + fileName);
			}
			System.out.println("..loaded " + fileName);
		}
		new GoogleSync(properties);

	}
}
