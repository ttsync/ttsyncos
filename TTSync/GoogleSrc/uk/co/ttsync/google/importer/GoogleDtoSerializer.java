package uk.co.ttsync.google.importer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.HashSet;

import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.entities.dtos.Dto;
import uk.co.ttsync.entities.serializers.BasicSerializer;

public class GoogleDtoSerializer extends BasicSerializer <Dto> {

	@Override
	public void serialize(OutputStream output, Dto obj) throws IOException {
		super.serialize(output, obj);
	}

	@Override
	public Dto deserialize(InputStream input) throws IOException,
			ClassNotFoundException {
		
		Dto dto = super.deserialize(input);
		
		for (User user : dto.users) {
			for (Membership m : new HashSet<Membership> (user.getMembership()))
				m.getGroup().getMembers().add(m);

			for (Subscription s : new HashSet <Subscription>(user.getSubscriptions()))
				s.getCalendar().getSubscribers().add(s);
		}	
		
		HashMap <String, Group> groups = new HashMap <String, Group> ();
		for (Group group : dto.groups)
			groups.put(group.getName(), group);
		
		for (Calendar calendar : dto.calendars) {
			Group group = groups.get(calendar.getProperty("resourceName")); 
			calendar.setGroup(group);
		}
		return dto;
	}
}

