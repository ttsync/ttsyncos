package uk.co.ttsync.google.importer;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import uk.co.ttsync.utils.Properties;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.model.CalendarResource;
import com.google.api.services.admin.directory.model.CalendarResources;
import com.google.api.services.calendar.Calendar;

import uk.co.ttsync.google.entities.GoogleCalendar;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class CalendarDao {

	private final int logLevel;
	boolean add,update,remove;
	
	/**
	 * a map of the calendars name (NOT id in this case) vs the calendar itself.
	 */
	HashMap<String, uk.co.ttsync.entities.Calendar> calendars = new HashMap<String, uk.co.ttsync.entities.Calendar>();

//	private Properties properties;
	Directory dir;
	Calendar calSvc;
	
	public CalendarDao(GoogleServiceProvider svc, Properties properties) {
//		this.properties = properties;
		
		// Set up loggers:
		// reading entries in:
		logLevel = properties.getLogLevel();
		GoogleCalendar.logLevel = logLevel;
		
		add = properties.containsKey("commit.calendar.actions")
				&& properties.getProperty("commit.calendar.actions").contains("add");
		update = properties.containsKey("commit.calendar.actions") 
				&& properties.getProperty("commit.calendar.actions").contains("update");
		remove = properties.containsKey("commit.calendar.actions") 
				&& properties.getProperty("commit.calendar.actions").contains("remove");

//		domain = properties.getProperty("google.domain");	
			
//		svc = cred.getAppsSvc();
		dir = svc.getDir();
		calSvc = svc.getCalSvc();
	}
	
	public Collection <uk.co.ttsync.entities.Calendar> readCalendars () throws IOException{
		if (logLevel > 0)
			System.out.println ("Starting Google Calendar import:" + new Date ());
		
		Directory.Resources.Calendars.List calList = dir.resources().calendars().list("my_customer");
		
		int count = 0;
		String pageToken = "";
		do {
			try {
				CalendarResources cals = calList.execute();
				for (CalendarResource cal : cals.getItems()) {
					GoogleCalendar gCal = new GoogleCalendar (cal);
					this.calendars.put(cal.getResourceEmail(), gCal);
					count++;
					if (logLevel > 1)
						System.out.print(" ...imported google calendar: "
							+ gCal + " ("
							+ gCal.getProperty ("resourceEmail") + ")");
					if (logLevel > 4)
						System.out.print (" - "  + gCal.getDetails());
					System.out.print("\n.");
					
					if (count % 50 == 0)
						System.out.println(" " + count);
				}
				pageToken = cals.getNextPageToken();
				calList.setPageToken(pageToken);
			} 
			catch (GoogleJsonResponseException | SocketTimeoutException | SocketException e) {
				System.out.println ("Failed to read calendars - retrying (" + e.getClass() + ")");
				e.printStackTrace();
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					Thread.currentThread().interrupt();
				}
			}
		} while (pageToken != null);
		
		if (logLevel > 0)
			System.out.println ("Finished Google Calendar import - " + count + " entries found. "+ new Date ());

		return calendars.values();
	}
	
	public Collection <uk.co.ttsync.entities.Calendar> getCalendars () {
		return calendars.values();
	}
}
