package uk.co.ttsync.google.importer;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.Users;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.Calendar.CalendarList.List;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;

import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.google.entities.GoogleUser;
import uk.co.ttsync.google.process.GoogleServiceProvider;
import uk.co.ttsync.utils.Properties;

public class UserDao {

	final int logLevel;
	
	boolean add,update,remove;
	
	HashMap<String, uk.co.ttsync.entities.User> users = new HashMap<String, uk.co.ttsync.entities.User>();

	private GoogleServiceProvider svc;
	private Properties properties;
	
	public UserDao(GoogleServiceProvider svc, Properties properties) throws IOException {

		this.properties = properties;
		
		logLevel = properties.getLogLevel();
		
		add = properties.containsKey("commit.user.actions")
				&& properties.getProperty("commit.user.actions").contains("add");
		update = properties.containsKey("commit.user.actions")
				&& properties.getProperty("commit.user.actions").contains("update");
		remove = properties.containsKey("commit.user.actions")
				&& properties.getProperty("commit.user.actions").contains("remove");
		
		this.svc = svc;
	}
	
	/**
	 * 
	 * @throws IOException
	 */
	Collection<uk.co.ttsync.entities.User> readUsers () throws IOException {

		if (logLevel > 0)
			System.out.println("Starting Google user import " + new Date());

		Directory.Users.List userList = svc.getDir().users().list();
		
		userList.setDomain(properties.getProperty("google.domain"));

		int count = 0;
		String pageToken = "";
		do {
			try {
				Users users = userList.execute();
				for (User user : users.getUsers()) {
					GoogleUser gUser = new GoogleUser (user);
					this.users.put(user.getId(), gUser);
					count++;
					if (logLevel > 1)
						System.out.print(" ...imported google user: "
							+ gUser + " ("
							+ gUser.getProperty ("primaryEmail") + ")");
					if (logLevel > 4)
						System.out.print (" - "  + gUser.getDetails());
					System.out.print("\n.");
					
					if (count % 50 == 0)
						System.out.println(" " + count);
				}
				pageToken = users.getNextPageToken();
				userList.setPageToken(pageToken);
			} 
			catch (GoogleJsonResponseException | SocketTimeoutException | SocketException e) {
				System.out.println ("Failed to read users - retrying (" + e.getClass() + ")");
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					Thread.currentThread().interrupt();
				}
			}
		} while (pageToken != null);

		if (logLevel > 0)
			System.out.println("Finished Google user import - " + count + " users found. " + new Date());
		
		return this.users.values();
	}

	public Collection<uk.co.ttsync.entities.User> getUsers() {
		return users.values();
	}

	Collection<Subscription> readSubscriptions (uk.co.ttsync.google.entities.GoogleUser user, CalendarDao calendarDao) throws IOException {

		if (logLevel > 0)
			System.out.println("...importing Google subscriptions for " + user + ":");
		
		Calendar calSvc = svc.getCalSvcAsUser(user.getProperty("primaryEmail"));
		List calendarList = calSvc.calendarList().list();

		Collection<Subscription> result = new HashSet <Subscription> ();
		
		int count = 0;
		
		String pageToken = "";
		do {
			try {
				CalendarList list = calendarList.execute();
	
				for (CalendarListEntry calendar : list.getItems()) {
					uk.co.ttsync.entities.Calendar cal = calendarDao.calendars.get(calendar.get("id"));
					if (user != null && cal != null) {
						Subscription subscription = new Subscription (user, cal, calendar.getAccessRole());
					
						for (String property : calendar.keySet())
							subscription.setProperty (property, calendar.get(property).toString());
						
						result.add(subscription);
				
						count++;
						if (logLevel > 1)
							System.out.println(" ...imported Google subscription: " + calendar.getSummary() + " as " + calendar.getAccessRole());
						
						System.out.print(".");
						if (count % 50 == 0)
							System.out.println(" " + count);
						
					}
				}
				pageToken = list.getNextPageToken();
				calendarList.setPageToken(pageToken);
			}
			catch (GoogleJsonResponseException | SocketTimeoutException | SocketException e) {
				System.out.println ("Failed to read subscriptions - retrying... (" + e.getMessage() + ", " + e.getClass() + ")");
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					Thread.currentThread().interrupt();
				}
			}
		} while (pageToken != null);
		
		if (logLevel > 0)
			System.out.println("...finished - " + count + " subscriptions found for " + user + ":");
		
		return result;
	}
}
