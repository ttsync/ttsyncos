package uk.co.ttsync.google.importer;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.entities.dtos.Dto;
import uk.co.ttsync.google.entities.GoogleCalendar;
import uk.co.ttsync.google.entities.GoogleUser;
import uk.co.ttsync.google.process.GoogleServiceProvider;
import uk.co.ttsync.utils.Properties;

public class GoogleImporter {

	private GroupDao groupDao;
	private UserDao userDao;
	private CalendarDao calendarDao;

	private GoogleServiceProvider cred;
	
	private Properties properties;

	public GoogleImporter(Properties properties) throws IOException {
		
		cred = new GoogleServiceProvider (properties); // .getCredential();
		
		userDao = new UserDao(cred, properties);
		groupDao = new GroupDao(cred, properties);
		calendarDao = new CalendarDao(cred, properties);
		
		this.properties = properties;
	}

	public Collection <User> readUsers () throws IOException {

		return userDao.readUsers();
	}	
	
	public Collection <Group> readGroups () throws IOException {
		
		return groupDao.readGroups();
	}	
	
	public Collection <Membership> readMembership () throws IOException {
		
		return groupDao.readMembership(userDao);
	}
	
	public Collection <Calendar> readCalendars () throws IOException {

		return calendarDao.readCalendars();
	}
	
	public Collection <Subscription> readSubscriptions () throws IOException {
		
		Collection <Subscription> subscriptions = new HashSet <Subscription> ();
		for (User user : userDao.getUsers())
			if (user.getProperty("suspended").equals("false"))
				subscriptions.addAll(userDao.readSubscriptions ((GoogleUser)user, calendarDao));
		
		return subscriptions;
	}
	
	public Collection <Calendar> readEvents() throws IOException {

		for (Calendar cal : calendarDao.calendars.values()) {
			((GoogleCalendar) cal).readEvents(cred.getCalSvc(), 
					Integer.parseInt(properties.getProperty("google.event.readpast")),
					Integer.parseInt(properties.getProperty("google.event.readfuture")
			));
		}
				
		return calendarDao.getCalendars();
		
//		groupDao.matchCalendars(calDao);
	}

	public Dto getDto() {
		Dto dto = new Dto ();
		dto.users = new HashSet<User>(userDao.getUsers());
		dto.groups = new HashSet<Group>(groupDao.getGroups());
		dto.calendars = new HashSet <Calendar> (calendarDao.getCalendars());
		
		return dto;
	}
}