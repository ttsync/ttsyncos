package uk.co.ttsync.google.importer;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.model.Group;
import com.google.api.services.admin.directory.model.Groups;
import com.google.api.services.admin.directory.model.Member;
import com.google.api.services.admin.directory.model.Members;

import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.google.entities.GoogleCalendar;
import uk.co.ttsync.google.entities.GoogleGroup;
import uk.co.ttsync.google.entities.GoogleMembership;
import uk.co.ttsync.google.entities.GoogleUser;
import uk.co.ttsync.google.process.GoogleServiceProvider;
import uk.co.ttsync.utils.Properties;

public class GroupDao {

	private final int logLevel;

	boolean add, update, remove;

	private Directory dir;
	private com.google.api.services.groupssettings.Groupssettings.Groups groupSvc;
	private String domain;

	HashMap<String, uk.co.ttsync.entities.Group> groups = new HashMap<String, uk.co.ttsync.entities.Group>();

	public GroupDao(GoogleServiceProvider cred, Properties properties)
			throws IOException {

		logLevel = properties.getLogLevel();
		
		add = properties.containsKey("commit.group.actions")
				&& properties.getProperty("commit.group.actions").contains(
						"add");
		update = properties.containsKey("commit.group.actions")
				&& properties.getProperty("commit.group.actions").contains(
						"update");
		remove = properties.containsKey("commit.group.actions")
				&& properties.getProperty("commit.group.actions").contains(
						"remove");

		domain = properties.getProperty("google.domain");

		dir = cred.getDir();

		groupSvc = cred.getGroupSvc().groups();
	}

	Collection<uk.co.ttsync.entities.Group> readGroups() throws IOException {

		if (logLevel > 0)
			System.out.println("Starting Google group import " + new Date());

		Directory.Groups.List groupList = dir.groups().list().setDomain(domain);
		int count = 0;
		String pageToken = "";
		do {
			try {
				Groups groups = groupList.execute();
				for (Group group : groups.getGroups()) {
					GoogleGroup gGroup = new GoogleGroup(group);

					readGroupSettings(gGroup);
					this.groups.put(group.getId(), gGroup);
					count++;
					if (logLevel > 1)
						System.out.print(" ...imported GW group " + gGroup.getName()
								+ "(" + group.getId() + ")");
					if (logLevel > 4)
						System.out.print(" - " + gGroup.getDetails());

					System.out.print("\n.");
				}
				pageToken = groups.getNextPageToken();
				groupList.setPageToken(pageToken);
			} catch (IOException e) {
				System.out.println ("Failed to read groups list ("+e.getMessage() + " - retrying..."); 
				// give up at some point???
				try { 
					Thread.sleep(10000); 
				} catch (InterruptedException e1) { 
					Thread.currentThread().interrupt(); 
				} 
			}
		} while (pageToken != null);

		if (logLevel > 1)
			System.out.println("Finished Google group import - " + count
					+ " groups found - " + new Date());

		return this.groups.values();
	}

	void readGroupSettings(GoogleGroup group) { // throws IOException {

		for (int i = 0; i < 5 ; i++) {
			try {
				com.google.api.services.groupssettings.model.Groups groups = groupSvc
						.get(group.getProperty("email")).execute();
				for (Entry<String, Object> prop : groups.entrySet())
					group.setProperty("settings." + prop.getKey(), prop.getValue()
							.toString());
				return;
			}
			catch (IOException e) {
				System.out.println ("Failed to read group setting for " + group.getName() + " (" + e.getClass() + ") - retrying..."); 
				try { 
					Thread.sleep(10000); 
				} catch (InterruptedException e1) { 
					Thread.currentThread().interrupt(); 
				} 
			}
		}
		System.err.println ("Giving up reading groups settings for " + group.getName());
	}

	Collection<Membership> readMembership(UserDao userDao) throws IOException {

		Collection<Membership> memberships = new HashSet<Membership>();

		if (logLevel > 0)
			System.out.println("Starting Google group membership import "
					+ new Date());

		for (uk.co.ttsync.entities.Group group : groups.values()) {

			Directory.Members.List memberList = dir.members().list(
					group.getProperty("_googleId"));

			int count = 0;
			String pageToken = "";
			do {
				try {
					Members members = memberList.execute();
					if (members.getMembers() != null)
						for (Member member : members.getMembers()) {

							User user = userDao.users.get(member.getId());
							// Seem to get some memberships where user or group
							// is null - filter them out here:
							if (user != null && group != null) {
								GoogleMembership m = new GoogleMembership(
										(GoogleUser) user, (GoogleGroup) group,
										member);

								count++;
								if (logLevel > 1)
									System.out.println(" ...imported GW membership "
											+ m.toString());

								System.out.print(".");
								if (count % 50 == 0)
									System.out.println(" " + count);
							}
						}

					pageToken = members.getNextPageToken();
					memberList.setPageToken(pageToken);
				} catch (GoogleJsonResponseException | SocketTimeoutException e) {
					System.out
							.println("Failed to read membership - retrying...  ("
									+ e.getMessage() + ")");
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e1) {
						Thread.currentThread().interrupt();
					}
				}
			} while (pageToken != null);

			if (logLevel > 0)
				System.out.println("...read " + count
						+ " memberships for group " + group.getName() + " : "
						+ new Date());

		}

		if (logLevel > 0)
			System.out.println("Finished Google group membership import "
					+ new Date());

		return memberships;
	}

	void matchCalendars(CalendarDao calDao) {

		for (uk.co.ttsync.entities.Group group : groups.values()) {
			String groupName = group.getName();
			GoogleCalendar cal = (GoogleCalendar) calDao.calendars
					.get(groupName);
			if (cal != null) {
				group.setCalendar(cal);
			}
		}
	}

	public Collection<uk.co.ttsync.entities.Group> getGroups() {
		return groups.values();
	}
}
