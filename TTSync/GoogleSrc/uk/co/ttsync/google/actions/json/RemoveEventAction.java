package uk.co.ttsync.google.actions.json;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Event;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class RemoveEventAction extends EventAction {

	public RemoveEventAction(Event event) {
		super(event.getCalendar().getProperty("_googleId"), event);
		addChange ("Removing event " + event.toString());
	}

	@Override
	public Event run(GoogleServiceProvider svc) throws Exception {
		svc.getCalSvc().events().delete(getEntry().getProperty("_googleId"), getEntry().getProperty("email")).execute();
		return null;
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.REMOVE;
	}
}
