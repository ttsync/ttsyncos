package uk.co.ttsync.google.actions.json;

import java.io.IOException;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.google.entities.GoogleCalendar;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class UpdateCalendarAction extends CalendarAction {

	public UpdateCalendarAction(GoogleCalendar calendar) {
		super(calendar);
//		addChange ("updating calendar " + calendar.getId());
	}

	@Override
	public Calendar run(GoogleServiceProvider svc) throws IOException {
		if (getContent().size() > 0) {
//			GoogleCalendar changes = new GoogleCalendar(svc.getCalSvc().calendars().patch(getEntry().getProperty("email"), getContent()).execute());
			GoogleCalendar changes = new GoogleCalendar(svc.getDir().resources().calendars().patch("my_customer", getEntry().getProperty("resourceId"), getContent()).execute());
			getEntry().merge(changes);
		}
		return getEntry();
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.UPDATE;
	}
}
