package uk.co.ttsync.google.actions.json;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Event;
import uk.co.ttsync.google.entities.GoogleCalendar;
import uk.co.ttsync.google.entities.GoogleEvent;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class UpdateEventAction extends EventAction {

	public UpdateEventAction(GoogleEvent event) {
//		super(((GoogleCalendar) event.getCalendar()).getId(), event);
		super(event.getCalendar().getProperty("resourceEmail"), event);
//		addChange ("updating event " + getId());
	}

	@Override
	public Event run(GoogleServiceProvider svc) throws Exception {
		if (getContent().size() > 2) {  // must have start and finish, so ignore these...
			com.google.api.services.calendar.model.Event event = svc.getCalSvc().events().update(calendarId, getEntry().getProperty("id"), getContent()).execute();
			GoogleEvent result = new GoogleEvent ((GoogleCalendar) getEntry().getCalendar(), event);
			getEntry().merge(result);
		}
		return getEntry();
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.UPDATE;
	}
}
