package uk.co.ttsync.google.actions.json;

import com.google.api.services.admin.directory.model.Member;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.google.entities.GoogleMembership.Role;

public abstract class MembershipAction extends
		JsonAction<Member, Membership> {

	MembershipAction(Membership membership) {
		super(membership, new Member ());
		getContent().setEmail(membership.getUser().getProperty("_googleId"));
	}

	public void setProperty(String property, Object value) {
		if (property.equals("role")) {
			if (value instanceof Role)
				getContent().setRole( ((Role)value).toString() );
			else if (value instanceof String)
				getContent().setRole ((String)value);
			
			addChange("role set to " + value);
			
//			switch (getContent().getRole()) {
//				case "teacher" :
//					getContent().setRole("MANAGER");
//					break;
//				default:
//					getContent().setRole("MEMBER");
//			}
		}
		else
			super.setProperty(property, value);
	}
	
//	@Override
//	public String getId () {
//		return getEntry().getGroup().getProperty("_googleId");		
//	}
}
