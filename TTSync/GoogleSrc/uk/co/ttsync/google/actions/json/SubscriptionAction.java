package uk.co.ttsync.google.actions.json;

import com.google.api.services.calendar.model.CalendarListEntry;
import uk.co.ttsync.entities.Subscription;

public abstract class SubscriptionAction extends JsonAction <CalendarListEntry, Subscription> {

	SubscriptionAction(Subscription subscription) {
		super(subscription, new CalendarListEntry());
		getContent().setId(subscription.getCalendar().getProperty("_googleId"));
	}
	
	@Override
	public void setProperty(String property, Object value) {
		if (property.equals("role"))
			getContent().setAccessRole(value.toString());	
		else
			super.setProperty(property, value);
	}
}
