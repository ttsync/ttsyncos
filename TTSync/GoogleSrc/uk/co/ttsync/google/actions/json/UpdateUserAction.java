package uk.co.ttsync.google.actions.json;

import java.io.IOException;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.google.entities.GoogleUser;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class UpdateUserAction extends UserAction {

	public UpdateUserAction(uk.co.ttsync.entities.User misUser, User gpwareUser) {
		super(gpwareUser, gpwareUser);
	}

	@Override
	public uk.co.ttsync.entities.User run(GoogleServiceProvider svc) throws IOException {
		if (getContent().size() > 0) {
			GoogleUser changes = new GoogleUser (svc.getDir().users().update(getEntry().getProperty("primaryEmail"), getContent()).execute());
			gpwareUser.merge(changes);
		}
		return gpwareUser;
	}

	@Override
	public Action.Type getActionType() {
		return Action.Type.UPDATE;
	}
}
