package uk.co.ttsync.google.actions.json;

import java.io.IOException;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.google.entities.GoogleGroup;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class RemoveGroupAction extends GroupAction {
	
	public RemoveGroupAction(Group group) {
		super(group, new com.google.api.services.admin.directory.model.Group());
		addChange("Deleting group");
	}

	@Override
	public GoogleGroup run(GoogleServiceProvider svc) throws IOException {
		svc.getDir().groups().delete(getEntry().getProperty("email")).execute();
		return null;
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.REMOVE;
	}

}
