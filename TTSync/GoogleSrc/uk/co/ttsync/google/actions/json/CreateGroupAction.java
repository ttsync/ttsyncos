package uk.co.ttsync.google.actions.json;

import java.io.IOException;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.google.entities.GoogleGroup;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class CreateGroupAction extends GroupAction {

	public CreateGroupAction(String id, String domain, Group group) {
		super(group, new com.google.api.services.admin.directory.model.Group ());
		getContent().setEmail(id+"@"+domain);
		addChange ("Create group " + id);
	}

	@Override
	public GoogleGroup run(GoogleServiceProvider svc) throws IOException {
		GoogleGroup newGroup = new GoogleGroup (svc.getDir().groups().insert(getContent()).execute());
		settingsAction.id = newGroup.getProperty("email");
		Group settings = settingsAction.run(svc);
		if (settings != null)
			newGroup.merge(settings);
		return newGroup;
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.ADD;
	}
}
