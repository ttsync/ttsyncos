package uk.co.ttsync.google.actions.json;

import java.util.ArrayList;

import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.UserExternalId;
import com.google.api.services.admin.directory.model.UserName;

public abstract class UserAction extends
		JsonAction<User, uk.co.ttsync.entities.User> {

	protected uk.co.ttsync.entities.User gpwareUser;

	UserAction(uk.co.ttsync.entities.User user, uk.co.ttsync.entities.User gpwareUser) {
		super(user, new User());
		this.gpwareUser = gpwareUser;
	}

	/**
	 * Override default setProperty to allow for specific handling of User's
	 * name...
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void setProperty(String property, Object value) {

		if (value instanceof String
				&& (value.equals("true") || value.equals("false")))
			value = Boolean.parseBoolean((String) value);

		if (property.matches("^ext\\..*")) {

			String type = property.substring(4);

			ArrayList<UserExternalId> extIds;
			if (getContent().getExternalIds() instanceof ArrayList<?>)
				extIds = (ArrayList<UserExternalId>) getContent().getExternalIds();
			else
				extIds = new ArrayList<UserExternalId>();
			
			UserExternalId extId = new UserExternalId();
			if (type.equals("account") || type.equals("customer") ||type.equals("network") ||type.equals("organization"))
				extId.setType(type);
			else {
				extId.setType("custom");
				extId.setCustomType(type);
			}
			extId.setValue((String)value);
			extIds.add(extId);

			getContent().setExternalIds(extIds.toArray());
			
		} else
			switch (property) {
			case "foreame":

			case "surname":
			case "fullname":
				UserName name = getContent().getName();
				if (name == null) {
					name = new UserName();
					if (gpwareUser != null) {
						name.setFamilyName(gpwareUser.getProperty("surname"));
						name.setGivenName(gpwareUser.getProperty("forename"));
						name.setFullName(gpwareUser.getProperty("fullname"));
					}
				}
				
				if (property.equals("givenName"))
					name.setGivenName((String) value);
				if (property.equals("familyName"))
					name.setFamilyName((String) value);
				if (property.equals("fullName"))
					name.setFullName((String) value);
					
				getContent().setName(name);
				break;
			default:
				getContent().set(property, value);
			}

		addChange(property + " set to " + value);
	}
}