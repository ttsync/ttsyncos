package uk.co.ttsync.google.actions.json;

import com.google.api.services.admin.directory.model.Member;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.google.entities.GoogleGroup;
import uk.co.ttsync.google.entities.GoogleMembership;
import uk.co.ttsync.google.entities.GoogleUser;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class UpdateMembershipAction extends MembershipAction {

	public UpdateMembershipAction(Membership membership) {
		super(membership);
//		addChange ("Updating membership " + membership);
	}

	@Override
	public Membership run(GoogleServiceProvider svc) throws Exception {
		if (getContent().size() > 0) {
			Member result = svc.getDir().members().update(getEntry().getGroup().getProperty("id"), getEntry().getUser().getProperty("primaryEmail"), getContent()).execute();
			GoogleMembership changes = new GoogleMembership ((GoogleUser)getEntry().getUser(), (GoogleGroup)getEntry().getGroup (), result);
			getEntry().merge(changes);
		}
		return getEntry();
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.UPDATE;
	}
}
