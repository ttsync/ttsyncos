package uk.co.ttsync.google.actions.json;

import java.util.HashMap;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Event.ExtendedProperties;

public abstract class EventAction extends JsonAction <Event, uk.co.ttsync.entities.Event> {
	
	protected final String calendarId;

	EventAction (String calendarId, uk.co.ttsync.entities.Event event) {
		super (event, new Event());
		
		this.calendarId = calendarId;
		
		DateTime start = DateTime.parseRfc3339(event.getStart().toString());
		getContent().setStart(new EventDateTime().setDateTime(start));
		
		DateTime finish = DateTime.parseRfc3339(event.getFinish().toString());
		getContent().setEnd(new EventDateTime().setDateTime(finish));
	}

	@Override
	public void setProperty(String property, Object value) {
//		if (property.equals("role"))
//				content.setAccessRole(value.toString());	
//		else DO SOMETHING ABOUT TIMES ETC...


		if (property.matches("^extended\\..*")) {
			ExtendedProperties extProps = getContent().getExtendedProperties();
			if (extProps == null)
				extProps = new ExtendedProperties();
			
			if (property.matches("^extended\\.shared\\..*")) {
				if (extProps.getShared() == null)
					extProps.setShared(new HashMap <String, String> ());
				extProps.getShared().put(property.substring(16), (String) value);
			}
			if (property.matches("^extended\\.private\\..*")) {
				if (extProps.getShared() == null)
					extProps.setShared(new HashMap <String, String> ());
				extProps.getPrivate().put(property.substring(17), (String) value);
			}
			
			getContent().setExtendedProperties(extProps);
		}
		else
			super.setProperty(property, value);
	}
}
