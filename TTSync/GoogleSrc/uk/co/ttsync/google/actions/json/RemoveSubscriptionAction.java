package uk.co.ttsync.google.actions.json;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class RemoveSubscriptionAction extends SubscriptionAction {

	public RemoveSubscriptionAction(Subscription subscription) {
		super(subscription);
		addChange ("remove subscription");
	}

	@Override
	public Subscription run(GoogleServiceProvider svc) throws Exception {
		svc.getCalSvcAsUser(getEntry().getProperty("primaryEmail")).calendarList().delete(getEntry().getCalendar().getProperty("resourceEmail")).execute();
		return null;
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.REMOVE;
	}

	
	
}
