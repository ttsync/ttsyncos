package uk.co.ttsync.google.actions.json;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.google.entities.GoogleCalendar;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class CreateCalendarAction extends CalendarAction {

	public CreateCalendarAction(String id, Calendar calendar) {
		super(calendar);
		this.getContent().setResourceId(id);
		addChange("Create calendar " + id);
//		this.getContent().setResourceName(calendar.getProperty("name"));
	}

	@Override
	public Calendar run(GoogleServiceProvider svc) throws Exception {
		return new GoogleCalendar(
				svc.getDir().resources().calendars().insert("my_customer", getContent()).execute());
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.ADD;
	}

}
