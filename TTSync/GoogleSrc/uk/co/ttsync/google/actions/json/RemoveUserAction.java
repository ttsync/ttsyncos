package uk.co.ttsync.google.actions.json;

import java.io.IOException;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.google.entities.GoogleUser;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class RemoveUserAction extends UserAction {
	
	boolean delete = false;

	public RemoveUserAction(uk.co.ttsync.entities.User gpwareUser) {
		super(gpwareUser, gpwareUser);
		addChange ("delete user " + gpwareUser.toString());
	}

	@Override
	public GoogleUser run(GoogleServiceProvider svc) throws IOException {

//		if (delete) {
//			svc.getDir().users().delete(getId()).execute();
//			return null;
//		}	
//		else {
			getContent().setSuspended(true);
			return new GoogleUser (svc.getDir().users().update(getEntry().getProperty("primaryEmail"), getContent()).execute());
//		}
	}
	
	/**
	 * If false, user will be suspended.
	 * @return
	 */
	public boolean getDelete () {
		return delete;
	}
	
	public void setDelete (boolean delete) {
		this.delete = delete;
	}
	
	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.REMOVE;
	}
}
