package uk.co.ttsync.google.actions.json;

import java.io.IOException;

import com.google.api.services.admin.directory.model.Member;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.google.entities.GoogleGroup;
import uk.co.ttsync.google.entities.GoogleUser;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class CreateMembershipAction extends MembershipAction {

	public CreateMembershipAction(Membership membership) {
		super(membership);
		addChange("Create membership : " + membership.getUser() + " in " + membership.getGroup() + " as a " + membership.getRole());
	}

	@Override
	public Membership run(GoogleServiceProvider svc) throws IOException {
		GoogleUser gUser = (GoogleUser) getEntry().getUser().getMatch();
		GoogleGroup gGroup = (GoogleGroup) getEntry().getGroup().getMatch();
		
		Member member = svc.getDir().members().insert(gGroup.getProperty("email"), getContent().setId(gUser.getProperty("id"))).execute();
		Membership membership = new Membership (gUser, gGroup, member.getRole());
		// set ID of membership... 
		membership.setProperty("id", member.getId());
		return membership;
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.ADD;
	}
}
