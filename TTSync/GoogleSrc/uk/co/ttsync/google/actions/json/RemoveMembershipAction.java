package uk.co.ttsync.google.actions.json;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Membership;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class RemoveMembershipAction extends MembershipAction {

	public RemoveMembershipAction(Membership membership) {
		super(membership);
	}

	@Override
	public Membership run(GoogleServiceProvider svc) throws Exception {
		svc.getDir().members().delete(
				getEntry().getGroup().getProperty("email"), 
				getEntry().getUser().getProperty("primaryEmail")
				//getContent().getId()
			).execute();
		return null;
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.REMOVE;
	}
}
