package uk.co.ttsync.google.actions.json;

import com.google.api.services.admin.directory.model.CalendarResource;

//import com.google.api.services.calendar.model.Calendar;



public abstract class CalendarAction extends JsonAction <CalendarResource, uk.co.ttsync.entities.Calendar> {

	CalendarAction(uk.co.ttsync.entities.Calendar calendar) {
		super(calendar, new CalendarResource());
	}

/*
	@Override
	public void setProperty(String property, Object value) {
 		if (property.equals ("name")) {
			getContent().setResourceName(value.toString());
			addChange(property + " set to " + value);
		}	
		else if (property.equals ("type")) {
			getContent().setResourceType(value.toString());
			addChange(property + " set to " + value);			
		}
		else if (property.equals("description")) {
			getContent().setResourceDescription(value.toString());
			addChange(property + " set to " + value);
		}
		else
			super.setProperty(property, value);
	}
*/
}
