package uk.co.ttsync.google.actions.json;

import java.io.IOException;

import com.google.api.services.calendar.model.CalendarListEntry;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Calendar;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class CreateSubscriptionAction extends SubscriptionAction {

	private String userEmail;

	public CreateSubscriptionAction(String id, Subscription subscription) {
		super(subscription);
		this.userEmail = subscription.getUser().getMatch().getProperty("primaryEmail");
		String calendarListId = subscription.getCalendar().getMatch().getProperty("resourceEmail");
		getContent ().setId(calendarListId);
		addChange ("create subscription for " + subscription.getUser() + " to " + subscription.getCalendar());
	}
	
	@Override
	public Subscription run (GoogleServiceProvider svc) throws IOException {
		CalendarListEntry result = svc.getCalSvcAsUser(userEmail).calendarList().insert(getContent()).execute();
		return new Subscription ((User) getEntry().getUser().getMatch(), (Calendar) getEntry().getCalendar().getMatch(), result.getAccessRole());
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.ADD;
	}
}
