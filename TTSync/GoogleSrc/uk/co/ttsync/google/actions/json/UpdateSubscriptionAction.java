package uk.co.ttsync.google.actions.json;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Subscription;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class UpdateSubscriptionAction extends SubscriptionAction {

	public UpdateSubscriptionAction(Subscription subscription) {
		super(subscription);
//		addChange("Updating subscription " + getEntry());
	}

	@Override
	public Subscription run(GoogleServiceProvider svc) throws Exception {
		svc.getCalSvcAsUser(getEntry().getUser().getProperty("id")).calendarList().update(getEntry().getUser().getProperty("id"), getContent()).execute();
//		Subscription changes = new Subscription (...
//		merge...
		return getEntry();
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		// TODO Auto-generated method stub
		return Action.Type.UPDATE;
	}
}
