package uk.co.ttsync.google.actions.json;

import java.io.IOException;
import java.util.Random;

import com.google.api.services.admin.directory.model.UserName;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.User;
import uk.co.ttsync.google.entities.GoogleUser;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class CreateUserAction extends UserAction {

	/**
	 * To create a user in Google you need to specify email, name (UserName object) (and password??), so you need to set 
	 * these properties before executing. 
	 * @param email
	 * @throws Exception 
	 */
	public CreateUserAction(String id, String domain, User misUser) {
		super(misUser, null);
		getContent().setPrimaryEmail(id+"@"+domain);
		UserName username = new UserName();
		username.setGivenName(misUser.getProperty("forename"));
		username.setFamilyName(misUser.getProperty("surname"));
		username.setFullName(misUser.getProperty("forename")+" "+misUser.getProperty("surname"));
		
		getContent().setName(username);
		getContent().setPassword(getPassword ());
		addChange("Create user " + id);
	}
	
	@Override
	public uk.co.ttsync.entities.User run(GoogleServiceProvider svc) throws IOException {
		return new GoogleUser(svc.getDir().users().insert(getContent()).execute());
	}

	@Override
	public Action.Type getActionType() {
		return Action.Type.ADD;
	}
	
	private String getPassword () {
		String result = "";
		Random r = new Random ();
		do {
			int c = (int) (r.nextFloat() * 76) + 47;
			if ((c > 47 && c < 58) || (c > 64 && c < 91) || (c > 96 && c < 123))
				result += (char) c;
		} while (result.length() < 8);
		
		return result;
	}
}
