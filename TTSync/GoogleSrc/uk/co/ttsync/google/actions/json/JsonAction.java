package uk.co.ttsync.google.actions.json;

import com.google.api.client.json.GenericJson;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.HasProperties;
import uk.co.ttsync.google.process.GoogleServiceProvider;

/**
 * This is an action on a genericJson object...
 * 
 * @author jimster
 *
 * @param <H>
 */
public abstract class JsonAction <T extends GenericJson, E extends HasProperties> extends Action <E, GoogleServiceProvider> {

	private T content;

	public JsonAction(E entry, T content) {
		super(entry);
		this.content = content;
	}

	/**
	 * Override this if ID is something else...
	 * @return
	 */
//	public String getId () {
//		return getEntry().getProperty("_googleId");
//	}
		
	protected T getContent () {
		return content;
	}
	
	public void setProperty(String property, Object value) {
		
//		if (value.toString().equals ("false"))
//			value = new Boolean (false);
//		if (value.toString().equals ("true"))
//			value = new Boolean (true);
		
		content.set(property, value);
		addChange(property + " set to " + value);
	}
}
