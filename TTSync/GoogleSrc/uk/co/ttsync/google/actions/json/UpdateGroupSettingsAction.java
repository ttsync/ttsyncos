package uk.co.ttsync.google.actions.json;

import java.io.IOException;

import com.google.api.services.groupssettings.model.Groups;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class UpdateGroupSettingsAction extends JsonAction <Groups, Group> {

	String id;

	public UpdateGroupSettingsAction(String id, Group entry) {
		super(entry, new Groups ());
		this.id = id;
	}
	
	@Override
	public Group run(GoogleServiceProvider svc) throws IOException {
		if (getContent().size() > 0) {
			Groups result = svc.getGroupSvc().groups().update(id, getContent()).execute();
			
			Group group = new Group ();
			for (String property : result.keySet()) 
				group.setProperty("settings." + property, result.get(property));
			return group;
		}
		return null;
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.UPDATE;
	}
}
