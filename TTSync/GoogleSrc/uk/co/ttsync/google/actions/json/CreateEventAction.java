package uk.co.ttsync.google.actions.json;

import com.google.api.services.calendar.model.Event;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.google.entities.GoogleCalendar;
import uk.co.ttsync.google.entities.GoogleEvent;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class CreateEventAction extends EventAction {


	public CreateEventAction(String calendarId, uk.co.ttsync.entities.Event event) {
		super(calendarId, event);
		addChange ("create event " + event.toString());
		
//		User teacher = event.getTeacher();
// stash teacher's email here...
//		setProperty ("extended.private.teacher", teacherEmail);
	}

	@Override
	public uk.co.ttsync.entities.Event run(GoogleServiceProvider svc) throws Exception {
		String calendarId = getEntry().getCalendar().getMatch().getProperty("resourceEmail");
		Event gEvent = svc.getCalSvc().events().insert(calendarId, getContent()).execute();

		GoogleCalendar cal = (GoogleCalendar) getEntry().getCalendar().getMatch();
		return new GoogleEvent (cal, gEvent);
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.ADD;
	}
}
