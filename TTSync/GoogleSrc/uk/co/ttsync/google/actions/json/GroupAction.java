package uk.co.ttsync.google.actions.json;

import com.google.api.services.admin.directory.model.Group;

import uk.co.ttsync.google.process.GoogleServiceProvider;

public abstract class GroupAction extends JsonAction <Group, uk.co.ttsync.entities.Group> {
	
	protected UpdateGroupSettingsAction settingsAction;
	
	GroupAction(uk.co.ttsync.entities.Group group, Group content) {
		super(group, content);
		settingsAction = new UpdateGroupSettingsAction (group.getProperty("email"), group);
	}
	
	uk.co.ttsync.entities.Group runSettings (GoogleServiceProvider svc) throws Exception {
		if (getContent().size() > 0)
			return settingsAction.run(svc);
		else
			return null;
	}
	
	@Override
	public void setProperty(String property, Object value) {
		if (property.startsWith("settings.")) {
			settingsAction.setProperty(property.substring(9), value);
			addChange(property + " set to " + value);
		}
		else 
			super.setProperty(property, value);
	}
}