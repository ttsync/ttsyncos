package uk.co.ttsync.google.actions.json;

import java.io.IOException;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.entities.Group;
import uk.co.ttsync.google.entities.GoogleGroup;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class UpdateGroupAction extends GroupAction {

	private Group gwGroup;

	public UpdateGroupAction(Group group, Group gwEntry) {
		super(group, new com.google.api.services.admin.directory.model.Group ());
		this.gwGroup = gwEntry;
	}
	
	@Override
	public Group run(GoogleServiceProvider svc) throws IOException {
		if (getContent().size() > 0) {
			Group changes = new GoogleGroup (svc.getDir().groups().update (gwGroup.getProperty ("id"), getContent()).execute());
			gwGroup.merge (changes);
		}
	
		settingsAction.id = gwGroup.getProperty("settings.email");
		Group settings = settingsAction.run (svc);
		if (settings != null)
			gwGroup.merge(settings);
		return gwGroup;
	}

	@Override
	public Action.Type getActionType() {
		return Action.Type.UPDATE;
	}
}
