package uk.co.ttsync.google.actions.json;

import java.io.IOException;

import uk.co.ttsync.actions.Action;
import uk.co.ttsync.google.entities.GoogleCalendar;
import uk.co.ttsync.google.process.GoogleServiceProvider;

public class RemoveCalendarAction extends CalendarAction {

	public RemoveCalendarAction(GoogleCalendar calendar) {
		super(calendar);
		this.addChange("Deleting calendar " + calendar.toString());
	}

	@Override
	public GoogleCalendar run(GoogleServiceProvider svc) throws IOException {
		svc.getCalSvc().calendars().delete(getEntry().getProperty("resourceEmail")).execute();
		return null;
	}

	@Override
	public uk.co.ttsync.actions.Action.Type getActionType() {
		return Action.Type.REMOVE;
	}

}
